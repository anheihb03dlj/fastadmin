<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\library\Token;

use think\Loader;
use think\Request;

// 初始化rpc
use ThriftRpc\RpcSystem;
use ThriftRpc\RpcClient;

class Index extends Frontend
{
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
        // 必须加载类,否则报错!!!
        Loader::import('ThriftRpc\RpcSystem', EXTEND_PATH);
        Loader::import('ThriftRpc\RpcClient', EXTEND_PATH);
        Loader::import('vno\VnoService', EXTEND_PATH);
        RpcSystem::init();
    }

    function readGraphContent($request) {
        if($request->has('graph_file_url','post')) {
            $url = $request->param('graph_file_url');
            // 上传的文件完整地址
            $graph_file = ROOT_PATH . "public" . DS . "uploads" . '\\' . $url;
            $file_type = pathinfo($graph_file, PATHINFO_EXTENSION); // 文件扩展名
            $file_content = file_get_contents($graph_file);//将整个文件内容读入到一个字符串中
            // 是否需要发送到rpc服务器进行布局计算
            $need_layout = intval($request->param("need_layout"));
            return array(
                "file_type" => $file_type, 
                "file_content" => $file_content,
                "need_layout" => $need_layout
            );
        }
        else if($request->has('graph','post')) {
            return array(
                "file_type" => "json",
                "file_content" => $request->param("graph"),
                "need_layout" => 1
            );
        }
        else {
            return null;
        }
    }
    
    function readGraphParam($request) {
        $kwargs = array();
        $kwargs["nodesep"] = floatval($request->param('nodesep'));
        $kwargs["ranksep"] = floatval($request->param('ranksep'));
        $kwargs["rankdir"] = $request->param('rankdir');
        $kwargs["splines"] = $request->param('splines');
        $kwargs["node_width"] = floatval($request->param('node_width'));
        $kwargs["node_height"] = floatval($request->param('node_height'));        
        $kwargs["fontsize"] = floatval($request->param('fontsize'));
        return $kwargs;
    }

    function testRequest($request) {
        // 测试
        $result = array();
        $result['method'] = $request->method();
        $result['type'] = $request->type();
        $result['is_ajax']=$request->isAjax();
        return json($result);
    }

    public function ajaxVng()
    {
        try {
            $service_client = new RpcClient("vno", HOST, PORT2);
            $client = $service_client->getClient();
            // 把JSON字符串转成PHP数组(true表示为数组,false表示json对象)
            $result = json_decode($client->TestVng(), true);
            return json($result);
        }
        catch (TException $tx) {
            $result = array(
                "code" => 1,
                "msg" => $tx->getMessage(),
                "data" => array(),
            );
            return json($result);
        }
    }

    public function ajaxVngFromGraph()
    {
        // 请求对象(post和get)
        $request = Request::instance();
        // 读取文件的扩展名以及文件内容
        $graph = $this->readGraphContent($request);
        if(is_null($graph)) {
            $result = array(
                "code" => 1,
                "msg" => "数据错误!",
                "data" => array()
            );
            return json($result);
        }

        // 需要发送rpc服务器进行布局计算
        if($graph["need_layout"] == 1) {
            // 读取网络图布局的参数
            $kwargs = $this->readGraphParam($request);
            try {
                $service_client = new RpcClient("vno", HOST, PORT2);
                $client = $service_client->getClient();
                // 把JSON字符串转成PHP数组(true表示为数组,false表示json对象)
                $result = json_decode($client->RunVngFromFile($graph["file_type"], $graph["file_content"], json_encode($kwargs)), true);
                // $result["code"] = 0;
                $result["need_layout"] = $graph["need_layout"];
                return json($result);
            }
            catch (TException $tx) {
                $result = array(
                    "code" => 1,
                    "msg" => $tx->getMessage(),
                    "data" => array()
                );
                return json($result);
            }
        }
        else if($graph["file_type"] == "json") {
            // 不需要计算,直接返回json字符串
            $result = array(
                "code" => 0,
                "msg" => '不需要计算,直接返回json字符串',
                "data" => array(),
                "graph" => $graph["file_content"],
                "need_layout" => $graph["need_layout"]
            );
            return json($result);
        }
        // 测试request
        // return $this->testRequest($request);
        // return json($graph);            
    }

    public function ajaxUploadGraph()
    {
        // 必须返回如下的json格式(code、msg、data必须有,data里的内容可以自定义)
        // https://www.kancloud.cn/zhiqiang/helper/248534
        // http://fly.layui.com/jie/17489/
        // $result = array(
        //     "code" => 1,
        //     "msg" => "",
        //     "data" => array()
        // );
        $result = array([
            "code" => 1,
            "msg" => '',
            "data" => array()
        ]);
         /*
            获取上传文件
            <input type="file" name="image[]" />  ==> request()->file('image')
            <input type="file"/> ==>  request()->file("file")
            如果没有特殊指定<input type=file name=xxx>中的name属性,默认为file)
         */
        $file = request()->file("file");
        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file) {
            $info = $file->move(ROOT_PATH . "public" . DS . "uploads");
            if($info) {
                // 成功上传后 获取上传信息
                // echo($info->getExtension());
                // // 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
                // echo($info->getSaveName());
                // // 输出 42a79759f284b767dfcb2a0197904287.jpg
                // echo($info->getFilename());
                $result["data"]['url'] = $info->getSaveName();
                $result["code"] = 0; // 在layui中0表示成功!
            }
            else {
                // 上传失败获取错误信息
            }
        }
        return json($result);
    }

    /* 无效(暂未使用!)*/
    // tp5文件下载(ajax无法直接下载文件!!!)
    // https://blog.csdn.net/change_any_time/article/details/79706772
    // https://blog.csdn.net/chengxiadenghuo/article/details/79969220
    function downloadFile($file_dir, $file_name)
    {
        //检查文件是否存在    
        if (! file_exists ( $file_dir . $file_name )) {    
            header("HTTP/1.1 404 NOT FOUND");  
        } else {    
            //以只读和二进制模式打开文件   
            $file = fopen ( $file_dir . $file_name, "rb" ); 

            //告诉浏览器这是一个文件流格式的文件    
            Header ( "Content-type: application/octet-stream" ); 
            //请求范围的度量单位  
            Header ( "Accept-Ranges: bytes" );  
            //Content-Length是指定包含于请求或响应中数据的字节长度    
            Header ( "Accept-Length: " . filesize ( $file_dir . $file_name ) );  
            //用来告诉浏览器，文件是可以当做附件被下载，下载后的文件名称为$file_name该变量的值。
            Header ( "Content-Disposition: attachment; filename=" . $file_name );    

            //读取文件内容并直接输出到浏览器    
            echo fread ( $file, filesize ( $file_dir . $file_name ) );    
            fclose ( $file );    
        }
    }

    public function ajaxForDxf()
    {
        // 请求对象(包含url传过来的信息post和get)
        $request = Request::instance();
        $graph = $request->param('graph');

        try {
            $service_client = new RpcClient("vno", HOST, PORT2);
            $client = $service_client->getClient();
            // 把JSON字符串转成PHP数组(true表示为数组,false表示json对象)
            $result = json_decode($client->RunVngToDxf($graph), true);

            // 在上传文件夹中生成dxf文件!
            $file_dir = ROOT_PATH . "public" . DS . 'uploads\\';  //下载文件存放目录
            $file_name = "test.dxf";
            file_put_contents($file_dir.$file_name, $result["dxf"]);

            // 生成文件的路径
            // $result["data"]['url'] = '/uploads/'.$file_name;

            // 生成下载链接
            // $this->downloadFile($file_dir, $file_name);

            return json($result);
        }
        catch (TException $tx) {
            $result = array(
                "code" => 1, // 注意:在layui中0表示上传成功
                "msg" => $tx->getMessage(),
                "data" => array()
            );
            return json($result);
        }
    }

    public function index()
    {
        return $this->view->fetch("index");
        // return $this->view->fetch("cad");
    }

    public function news()
    {
        $newslist = [];
        return jsonp(['newslist' => $newslist, 'new' => count($newslist), 'url' => 'https://www.fastadmin.net?ref=news']);
    }
}
