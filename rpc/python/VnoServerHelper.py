# -*- coding:utf-8 -*-

# from vno import VnoService
# from vno.ttypes import *
import ThriftServerHelper
import VnoProcessor

# 创建服务器
def create_server(host, port):
    # 构造rpc消息处理接口
    handler, processor = VnoProcessor.make_processor()
    # 创建thrift rpc服务器
    server = ThriftServerHelper.make_rpc_server(processor, host=host, port=port)
    return handler, server