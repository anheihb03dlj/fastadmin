#-*- coding:utf-8 -*-
# import numpy as np
from algo.vno import *
from algo.vno_data import *
from algo.vng import *
from algo.qh_ga import *
from algo.qh import *
from algo.vng_dxf import *

import json

def __vno_result_to_dict(vnet, key_name='edges'):
    edges = []
    dg = vnet.graph()
    for e in dg.es:
        if is_zero_edge(e):
            continue
        u, v = dg.vs[e.source], dg.vs[e.target]
        x = {
            'id' : e['id'],
            's' : u['id'],
            't' : v['id'],
            'r' : e['r'],
            'q' : e['q']
        }
        edges.append(x)
    result = {}
    result[key_name] = edges
    return result
def __edges_to_dict(vnet, _edges, key_name='edges'):
    dg = vnet.graph()
    result = {}
    result[key_name] = [dg.es[i]['id'] for i in _edges]
    return result
def __comps_to_dict(vnet, _comps, key_name='components'):
    dg = vnet.graph()
    result = {}
    result[key_name] = dict(enumerate(_comps))
    return result
def __path_to_dict(vnet, _path):
    dg = vnet.graph()
    return {
        'path' : [str(dg.es[i]['id']) for i in _path],
        'h'    : vnet.hPath(_path)
    }
def __paths_to_dict(vnet, _paths, key_name='paths'):
    dg = vnet.graph()
    paths = []
    for _path in _paths:
        result = {}
        result['path'] = [str(dg.es[i]['id']) for i in _path]
        result['h']    = vnet.hPath(_path)
        paths.append(result)
    return {
        key_name : paths
    }
def run_vno(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    result = {}
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        result = {'code': 1, 'msg':'failed to build vent network'}
    # else:
    # # 判断连通性
    # if not vnet.isConnected():
    #     print u'通风网络不连通'
    #     result = {'code': 1, 'msg':'通风网络不连通'.decode('gb2312')}
    # # 查找进风井
    # elif len(vnet.sourceEdges()) == 0:  
    #     print u'通风网络中不存在进风井!'
    #     result = {'code': 1, 'msg':'通风系统中找不到进风井!'.decode('gb2312')}
    # elif len(vnet.sinkEdges()) == 0:  
    #     print u'通风网络中不存在回风井!'
    #     result = {'code': 1, 'msg':'通风系统中找不到回风井!'.decode('gb2312')}
    # elif vnet.hasCycles():  
    #     print u'通风网络中存在单向回路!'
    #     result = {'code': 1, 'msg':'通风网络中存在单向回路!'.decode('gb2312')}
    # 判断是否有向无环图(其作用等价于前面的这些判断: 连通性、进回风井、单向回路)
    elif not vnet.isDag():
        print u'通风网络不是有向无环图(DAG)'
        result = {'code': 1, 'msg':'not an dag', 'data':[]}
    else:
        print u'构造通风网络成功!!!'
        # 添加虚拟的源汇节点
        vnet.addVirtualST()
        # print u'虚拟源点:',vnet.vSource(), '虚拟汇点:',vnet.vTarget()
        # 5. 通风网络解算
        if not vno(vnet):
            print u'通风网络解算失败!'
            # 返回错误信息
            result = {'code': 1, 'msg':'vno failed', 'data':[]}
        else:
            # 6. 打印通风网络解算结果
            print_network(vnet, msg=u'vno result')
            # 计算结果转换为json
            result = __vno_result_to_dict(vnet, 'edges')
            result['code'] = 0
            result['msg'] = ''
            result['data'] = []
        # 删除虚拟的源汇节点及分支
        vnet.delVirtualST()
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def source_edges(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    result = {}
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        # 查找进风井
        edges = vnet.sourceEdges()
        if len(edges) == 0:            
            print u'can not find in shafts'
            result = {'code': 1, 'msg':'can not find in shafts', 'data':[]}
        else:
            # 6. 打印通风网络解算结果
            print u'通风网络中的有%d个进风井' % (len(edges))     
            result = __edges_to_dict(vnet, edges, 'source_edges')
            result['code'] = 0
            result['msg'] = ''
            result['data'] = []
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def sink_edges(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    result = {}
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        # 查找进风井
        edges = vnet.sinkEdges()
        if len(edges) == 0:
            print u'can not find out shafts'
            result = {'code': 1, 'msg':'can not find out shafts', 'data':[]}
        else:
            # 6. 打印通风网络解算结果
            print u'%d out shafts' % (len(edges))
            result = __edges_to_dict(vnet, edges, 'sink_edges')
            result['code'] = 0
            result['msg'] = ''
            result['data'] = []
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def is_connected(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        return False
    else:
        # 判断连通性
        return vnet.isConnected()
def is_dag(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    ret = True
    # 3. 构造通风网络对象
    vnet = VentNetwork()
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
        return False
    else:
        # 判断是否有向无环图
        return vnet.isDag()
def has_cycles(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
        return False
    else:
        # 判断由于单向回路
        return vnet.hasCycles()
def CC(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    result = {}
    vnet = VentNetwork()
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        # 查找连通块
        comps = vnet.CC()
        if len(comps) == 0:
            result = {'code': 1, 'msg':'unvalid graph of this vent network', 'data':[]}
        else:
            result = __comps_to_dict(vnet, comps, 'components')
            result['code'] = 0
            result['msg'] = ''
            result['data'] = []
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def SCC(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    result = {}
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        # 判断单向回路(强联通块)
        comps = vnet.SCC()
        result = __comps_to_dict(vnet, comps, 'loops')
        result['code'] = 0
        result['msg'] = ''
        result['data'] = []
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def negative_edges(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    result = {}
    # 3. 构造通风网络对象
    vnet = VentNetwork()
    # 注:需要读取风量
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        # 查找负风量分支
        edges = vnet.negativeEdges()
        print u'通风网络中的有%d个负风量分支' % (len(edges))
        # 返回计算结果
        result = __edges_to_dict(vnet, edges, 'negative_edges')
        result['code'] = 0
        result['msg'] = ''
        result['data'] = []
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def fixQ_edges(_graph_datas):
    # graph_datas = read_graph_datas('graph_data1.json')
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    result = {}
    # 3. 构造通风网络对象
    vnet = VentNetwork()    
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        # 查找进风井
        edges = vnet.fixQEdges()
        print u'通风网络中的有%d个固定风量分支' % (len(edges))    
        # 返回计算结果
        result = __edges_to_dict(vnet, edges, 'fixQ_edges')
        result['code'] = 0
        result['msg'] = ''
        result['data'] = []
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def run_vng(_graph_datas, _params):
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()
    result = {}
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        kwargs = json.loads(_params)
        kwargs = byteify(kwargs)
        result = vng(vnet, dot_exe='GraphViz\\bin\\dot.exe', **kwargs)
        if len(result) == 0:
            result = {'code': 1, 'msg':'failed to draw vent network', 'data':[]}
        else:
            result['code'] = 0
            result['msg'] = ''
            result['data'] = []
    # print result
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def run_qh(_graph_datas, _params):
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()
    result = {}
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        kwargs = json.loads(_params)
        kwargs = byteify(kwargs)
        # 提取参数
        # 是否进行网络解算(默认False)
        run_vno = bool(kwargs.get('run_vno', False))
        # 是否使用遗传算法
        use_ga = bool(kwargs.get('use_ga', False))
        # 添加虚拟源汇,将网络变成单一源汇通风网络
        vnet.addVirtualST()
        if run_vno and (not vno(vnet)):
            result = {'code': 1, 'msg':'vno failed', 'data':[]}
        else:
            if len(vnet.negativeEdges()) > 0:
                result = {'code': 1, 'msg':'error: negative edges exist!', 'data':[]}
            else:
                # 查找所有的关键节点(注: 也可以是一部分出度大于2的节点!!!)        
                # 6. 打印通风网络解算结果
                print_network(vnet, msg='网络解算结果')
                key_nodes = MakeKeyNodes(vnet)
                print u'关键节点个数:', len(key_nodes)
                # 打印所有节点的出边排列组合
                PrintAllVertexOutSeq(vnet.graph(), key_nodes)
                # 是否使用遗传算法
                if use_ga:
                    # (3)优化计算得到一个"最优"的节点排列序
                    # 读取json格式配置文件
                    order = QH_GA(vnet, key_nodes, kwargs)
                else:
                    # (1)随机给的一个节点排列序
                    order = [1 for v in key_nodes]
                    # (2)通过文件读取优化后的节点排列序
                    # order = np.loadtxt(QH_ORDER_FILE, dtype='int32').tolist()
                print order
                # 节点排列序写入到文件
                # WriteOrderToFile(order, QH_ORDER_FILE)
                # 绘制到dxf文件
                result = DrawQH(vnet, key_nodes, order)
                if len(result) == 0:
                    result = {'code': 1, 'msg':'failed to draw Q-H graph', 'data':[]}
                else:
                    result['code'] = 0
                    result['msg'] = ''
                    result['data'] = []
        # 删除虚拟源汇
        vnet.delVirtualST()
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示
def run_mrp(_graph_datas, _params):
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()
    result = {}
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        kwargs = json.loads(_params)
        kwargs = byteify(kwargs)
        # 提取参数
        # 是否进行网络解算(默认False)
        run_vno = bool(kwargs.get('run_vno', False))
        # n条最大阻力路线(默认1)
        n = int(kwargs.get('n', 1))
        if n <= 0:
            n = 1
        # 风机分支id(默认为空)
        fan_edge_id = str(kwargs.get('fan_edge', ''))
        # 限定阻力路线压差范围
        min_delta_h = float(kwargs.get('min_delta_h', -1))
        max_delta_h = float(kwargs.get('max_delta_h', -1))
        # 是否使用压差限定
        use_delta_h = not (max_delta_h < 0 or min_delta_h < 0 or max_delta_h <= min_delta_h)
        # 添加虚拟源汇,将网络变成单一源汇通风网络
        vnet.addVirtualST()
        # 进行网络解算
        if run_vno and (not vno(vnet)):
            result = {'code': 1, 'msg':'vno failed', 'data':[]}
        elif len(vnet.negativeEdges()) > 0:
            result = {'code': 1, 'msg':'error: negative edges exist!', 'data':[]}
        else:
            # 6. 打印通风网络解算结果
            print_network(vnet, msg=u'网络解算结果')
            # 查找风机分支的内部编号
            fan_edge = vnet.getFanEdgeById(fan_edge_id)
            # 记录多条最大阻力路线
            paths = []
            # 风机分支无效
            if fan_edge_id != '' and fan_edge == -1:
                msg = 'fan edge of %s not exist' % (fan_edge_id)
                result = {'code': 1, 'msg':msg, 'data':[]}
            # 搜索整个通风系统的n条最大阻力路线
            elif fan_edge_id == '':
                paths = mrp_n(vnet, n)
            # 仅搜索一台风机的n条最大阻力路线
            elif fan_edge != -1:
                if use_delta_h:
                    # 查找压差范围内的一台风机的n条最大阻力路线
                    paths = mrp_fan_h(vnet, fan_edge, min_delta_h, max_delta_h, n)
                else:
                    # 不考虑压差限制,搜索一台风机的n条最大阻力路线
                    paths = mrp_fan_n(vnet, fan_edge, n)
            if len(paths) == 0 and 'code' not in result:
                msg = ''
                if fan_edge_id == '':
                    msg = 'vent network'
                elif fan_edge != -1:
                    msg = 'one fan'
                msg = 'failed to search the %d mrp of %s' % (n, msg)
                result = {'code': 1, 'msg':msg, 'data':[]}
            elif len(paths) > 0:
                result.update(__paths_to_dict(vnet, paths, 'paths'))
                result['n'] = n
                result['fan_edge'] = fan_edge_id # 风机分支的id
                result['use_delta_h'] = use_delta_h # 是否有风压限制
                result['min_delta_h'] = min_delta_h
                result['max_delta_h'] = max_delta_h
                result['code'] = 0
                result['msg'] = ''
                result['data'] = []
    # 删除虚拟源汇
        vnet.delVirtualST()
    return json.dumps(result, ensure_ascii=False)

def test_vng():
    # 选择一个简单网络
    graph_datas = read_graph_datas('data\\graph_data2.json')
    kwargs = {}
    kwargs["node_sep"] = 50.8
    kwargs["rank_sep"] = 38.1
    kwargs["node_width"] = 19.05
    kwargs["node_height"] = 12.7
    kwargs["arrow_width"] = 4
    kwargs["arrow_length"] = 9
    kwargs["text_height"] = 12.7 * 0.618
    kwargs["use_sepoint"] = True
    return run_vng(json.dumps(graph_datas), json.dumps(kwargs))

import tempfile

def read_topo_file2(f):
    # 记录所有分支的数据
    edges = []
    for line in f:
        data = line.strip().split()
        # 一条分支数据
        d = {
            'id': data[0],
            's' : data[1],
            't' : data[2],
            'r' : 0.0,
            'q' : 0.0
        }
        if len(data) == 4:
            d['r'] = data[3]
        if len(data) == 5:
            d['q'] = data[4] # 新增风量数据
        edges.append(d)
    return edges

# 临时文件用法： http://www.jb51.net/article/50374.htm
def read_graph_datas_from_txt(file_content):
    graph_datas = {}
    # 关闭文件时候删除
    f = tempfile.TemporaryFile()
    try:
        f.write(file_content)
        f.seek(0)
        graph_datas['edges'] = read_topo_file2(f)
    finally:
        f.close()
    return graph_datas

def run_vng_from_file(file_type, file_content, kwargs):
    # print file_type
    # print kwargs
    if file_type == 'json':
        # print json.dumps(graph_datas)
        return run_vng(file_content, kwargs)
    elif file_type == 'txt':
        graph_datas = read_graph_datas_from_txt(file_content)
        # print json.dumps(graph_datas)
        return run_vng(json.dumps(graph_datas), kwargs)
    elif file_type == 'dxf':
        return {'code': 1, 'msg':'dxf parser nont implement yet', 'data':[]}
    else:
        msg = '%s parser nont implement yet' % (file_type)
        return {'code': 1, 'msg': msg, 'data': []}

def run_vng_to_dxf(_graph_datas):
    graph_datas = json.loads(_graph_datas)
    graph_datas = byteify(graph_datas)
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    # graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()
    result = {}
    if not build_network(graph_datas, vnet.graph()):
        print u'failed to build vent network'
        result = {'code': 1, 'msg':'failed to build vent network', 'data':[]}
    else:
        # 生成dxf文件
        dxf_file = 'out\\vng.dxf'
        draw_graph_to_dxf(vnet.graph(), dxf_file)
        # 文件内容转换成字符串
        f = open(dxf_file, 'r')
        result['dxf'] = f.read()
        f.close()

        result['code'] = 0
        result['msg'] = ''
        result['data'] = []
    return json.dumps(result, ensure_ascii=False) # 确保中文正常显示

if __name__=="__main__":
    pass