#-- coding:utf-8 --

# 替换sdxf为ezdxf库(作者也是dxfwrite和dxfgrabber的开发者)
# http://ezdxf.readthedocs.io/en/latest/introduction.html#what-ezdxf-is-not
import ezdxf

def PT_2_MM(v):
    return  v*0.35

def draw_graph_to_dxf(dxfFile='vng.dxf'):
    dwg = ezdxf.new('R2010')  # create a new DXF R2010 drawing, official DXF version name: 'AC1024'
    msp = dwg.modelspace()  # add new entities to the model space
    # d.layers.append(sdxf.Layer(name="通风网络图",color=7))
    dwg.layers.new(name='test')
    # msp.add_line((0, 0), (10, 0), dxfattribs={'layer': 'Lines'})
    x, y = 10, 10
    width, height = 4, 3
    ratio = 1.0*height/width
    # 绘制椭圆节点
    msp.add_ellipse(center=(x, y, 0), major_axis=(width, 0, 0), ratio=ratio, dxfattribs={'layer': u'通风网络图'})
    dwg.styles.new('vng', dxfattribs={'font': 'simsun.ttf'})  # Arial, default width factor of 0.8
    # 绘制节点文字
    msp.add_text("Text Style Example: Times New Roman", dxfattribs={'style': 'vng', 'height': 6}).set_pos((x, y), align='MIDDLE_CENTER')
    dwg.saveas(dxfFile)

draw_graph_to_dxf()