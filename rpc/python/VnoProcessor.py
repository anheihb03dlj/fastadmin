# -*- coding:utf-8 -*-

from vno import VnoService
# from vno.ttypes import *
import VnoServiceUtils

class VnoServiceHandler:
    def __init__(self):
        pass
    def Test(self, n1, n2):
        print u'add(%d,%d)=%d' % (n1, n2, n1+n2)
        return n1 + n2
    def TestVng(self):
        return VnoServiceUtils.test_vng()
    def RunVngFromFile(self, file_type, file_content, kwargs):
        return VnoServiceUtils.run_vng_from_file(file_type, file_content, kwargs)
    def RunVngToDxf(self, graph_datas):
        return VnoServiceUtils.run_vng_to_dxf(graph_datas)
    def RunVno(self, graph_datas):
        return VnoServiceUtils.run_vno(graph_datas)
    def GetSourceEdges(self, graph_datas):
        return VnoServiceUtils.source_edges(graph_datas)
    def GetSinkEdges(self, graph_datas):
        return VnoServiceUtils.sink_edges(graph_datas)
    def IsConnected(self, graph_datas):
        return VnoServiceUtils.is_connected(graph_datas)
    def IsDag(self, graph_datas):
        return VnoServiceUtils.is_dag(graph_datas)
    def HasCycles(self, graph_datas):
        return VnoServiceUtils.has_cycles(graph_datas)
    def CC(self, graph_datas):
        return VnoServiceUtils.CC(graph_datas)
    def SCC(self, graph_datas):
        return VnoServiceUtils.SCC(graph_datas)        
    def GetNegativeEdges(self, graph_datas):
        return VnoServiceUtils.negative_edges(graph_datas)
    def GetFixQEdges(self, graph_datas):
        return VnoServiceUtils.fixQ_edges(graph_datas)
    def RunVng(self, graph_datas, params):
        return VnoServiceUtils.run_vng(graph_datas, params)
    def RunQH(self, graph_datas, params):
        return VnoServiceUtils.run_qh(graph_datas, params)
    def RunMrp(self, graph_datas, params):
        return VnoServiceUtils.run_mrp(graph_datas, params)

def make_processor():
    # 构造rpc消息处理接口
    handler = VnoServiceHandler()
    processor = VnoService.Processor(handler)
    return handler, processor