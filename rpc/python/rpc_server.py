#-*- coding:utf-8 -*-

import ThriftServerHelper
import VnoServerHelper
# from ThriftClientHelper import *

HOST = 'localhost'
PORT1, PORT2 = 9100, 9090

def main():
    # 创建service服务器
    handler, server = VnoServerHelper.create_server(HOST, PORT2)
    # 单开一个线程给service服务器用
    print u'启动service服务器(端口:9090)...'
    ThriftServerHelper.run_rpc_server(server)
    # 启动控制服务器,监听shutdown消息(会阻塞主线程)
    print u'启动control服务器(端口:9100)...'
    # ThriftServerHelper.run_rpc_server(ctrlServer)
 
if __name__ == '__main__':
    main()