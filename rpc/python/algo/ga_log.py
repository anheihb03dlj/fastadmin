#-*- coding:utf-8 -*-

import logging

def InitGALogSystem():
    logger = logging.getLogger('inspyred.ec')
    logger.setLevel(logging.DEBUG)
    file_handler = logging.FileHandler('inspyred.log', mode='w')
    file_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

def DEBUG(s):
    logging.getLogger('inspyred.ec').debug(s)

def INFO(s):
    logging.getLogger('inspyred.ec').info(s)

def WARNING(s):
    logging.getLogger('inspyred.ec').warning(s)    