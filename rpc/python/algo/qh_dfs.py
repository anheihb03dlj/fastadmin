#-*- coding:utf-8 -*-

# python标准库里的优先队列
from Queue import PriorityQueue
# python标准库里的队列
# from collections import deque
# python标准库里的排列组合
import itertools

from igraph import *

class VertexOutSeqNode:
    def __init__(self, e, w):
        self.e = e
        self.w = w
    def __lt__(self, other):
        return self.w < other.w

# 重排出边顺序
def AdjustVertexOutSeq(dg, key_nodes, order, seq_name='seq'):
    # 节点增加一个出对顺序数字属性
    dg.vs['seqPos'] = 0
    for u in dg.vs:
        u['seqPos'] = u.degree(type='out') # 初始值等于出度
    # 转换为个体值
    for u, x in zip(key_nodes, order):
        u['seqPos'] = x

    # 构造一个优先级队列,用于对节点出边进行重排序
    pq = PriorityQueue()
    # 所有节点增加一个出边顺序表
    dg.vs[seq_name] = None
    for u in dg.vs:
        # 节点出度
        d = u.degree(type='out')
        if d < 1:
            continue            
        # 节点出边的排列组合
        seq = list(itertools.permutations(range(1, d+1)))
        # 节点出边的初始顺序
        edges = [i for i in dg.incident(u, mode=OUT)]
        # 当前的排列组合取值
        k = u['seqPos']
        if k < 1 or k > len(seq):
            k = 1
        for e, w in zip(edges, seq[k-1]):
            node = VertexOutSeqNode(e, w)
            pq.put(node)
        # 得到重新排列后的出边顺序
        seq = []        
        while not pq.empty():
            node = pq.queue[0]
            seq.append(node.e)
            pq.get()
        # 逆序
        # seq.reverse()
        u[seq_name] = seq
    # 删除临时的优先级队列对象
    del pq
    del dg.vs['seqPos']

def PrintAllVertexOutSeq(dg, key_nodes):
    print u'---------------节点的出边排列组合(begin)------------------>'
    for u in dg.vs:
        # 节点出度
        d = u.degree(type='out')
        if d < 2:
            continue
        print u'节点v%s的出边排列组合:' % (str(u['id'])) 
        # 节点出边的排列组合
        seqs = list(itertools.permutations(range(1, d+1)))
        # 节点出边的初始顺序
        edges = [i for i in dg.incident(u, mode=OUT)]
        for k,child_seq in enumerate(seqs):
            pq = PriorityQueue()
            for e, w in zip(edges, child_seq):
                node = VertexOutSeqNode(e, w)
                pq.put(node)
            # 得到重新排列后的出边顺序
            seq = []
            while not pq.empty():
                node = pq.queue[0]
                seq.append(node.e)
                pq.get()
            print u'\t(%d)' % (k+1),
            for i in seq:
                print u'e%s' % (str(dg.es[i]['id'])),
            # 删除优先队列对象
            del pq
            # 2列换行
            if (k+1) % 2 == 0:
                    print
    print u'<---------------节点的出边排列组合(end)--------------------'

def PrintOutSeq(dg):
    print u'---------------节点的出边顺序(begin)------------------>'
    for u in dg.vs:
        print u'节点v%s:' % (str(u['id'])),
        print u'原始:',
        for i in dg.incident(u, type=OUT):
            print u'e%s' % (str(dg.es[i]['id'])),
        print u'调整后:',
        if u['seq'] is not None:
            for i in u['seq']:
                print u'e%s' % (str(dg.es[i]['id'])),
        print 
    print u'<---------------节点的出边顺序(end)--------------------'

# 通过A*算法改造而来,利用seq控制出边顺序
# 给定节点顺序进行深度优先搜索,得到一条路径
def SeqDfs(dg, s, t, seq_name='seq', visited_name='visited', pred_name='pred', k=1):
    class DfsNode:
        def __init__(self, u, pred, g=0, h=0):
            self.u = u
            self.pred = pred
            self.g = g
            self.h = h
        def __lt__(self, other):
            f1 = self.g + self.h
            f2 = other.g + other.h
            return f1 < f2

    if s == t:
        return False
    stack = []
    cnt = 0
    # 增加pred属性
    dg.vs[s][pred_name] = -1
    node = DfsNode(s, -1)
    stack.append(node)
    while len(stack) > 0:
        node = stack.pop()
        u = node.u
        dg.vs[u][pred_name] = node.pred
        # print u'u=', dg.vs[u]['id']
        if u == t:
            cnt = cnt + 1
        if cnt == k:
            break
        # 已设定好节点出边顺序(seq)!!!
        # for i in dg.incident(u, mode=OUT):
        if dg.vs[u][seq_name] is None:
            continue
        for i in reversed(dg.vs[u][seq_name]):
            v = dg.es[i].target
            # 如果节点已被访问过了,就不再压入到栈中
            # 该处理仅针对独立通路法!
            if dg.es[i][visited_name]:
                continue
            dg.vs[v][pred_name] = u
            child_node = DfsNode(v, u)
            stack.append(child_node)
    return cnt == k

def PrintPath(dg, path):
    for i in path:
        print u'e%s' % (str(dg.es[i]['id'])),
    print 