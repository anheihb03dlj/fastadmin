#-*- coding:utf-8 -*-
from multiprocessing import Process, Queue, Manager, Pool
import multiprocessing
import random, time, os
import numpy as np

class MyClass:
    def __init__(self, a):
        self.a = a

# http://www.cnblogs.com/kuoaidebb/p/4786879.html
# https://my.oschina.net/leejun2005/blog/203148
# http://www.cnblogs.com/yygsj/p/5829774.html
def worker(num, datas, svm_samples, lock=None):
    # with lock:    
    print u'pid:',os.getpid()
    for i in range(3):
        svm_samples.put((num, [x*random.random() for x in datas]))
        # print svm_samples
    # time.sleep(random.random())

def print_queue(q):
    print u'<---------------pid:%s-----------------' % os.getpid()
    while q.qsize()>0:
        print q.get()
    print u'---------------pid:%s----------------->' % os.getpid()

def test_multiprocess():
    # lock = multiprocessing.Lock()
    svm_samples = Queue()
    lock = None
    datas = np.array([1.9, 4.4, 5.8, 2.3, 6.1, 3.3])
    # datas = [1,2,3,4,5,6,7,8,9,10]
    jobs = []
    for i in range(10):
        p = Process(target = worker, args=(i, datas, svm_samples, lock))
        p.daemon = True
        jobs.append(p)
        p.start()
    for p in jobs:
        p.join()
    # print dir(svm_samples)
    print_queue(svm_samples)
    print u"end"

def test_pool():
    manager = Manager()
    shared_svm_samples = manager.Queue()
    # lock = manager.Lock()
    lock = None
    datas = np.array([1.9, 4.4, 5.8, 2.3, 6.1, 3.3])
    # 线程池
    proc_pool = Pool(processes=multiprocessing.cpu_count())
    # datas = [1,2,3,4,5,6,7,8,9,10]
    # 遍历每条分支并修改风阻
    for i in range(10):
        # 每条分支进行50次网络解算,得到50组数据
        # time.sleep(0.5)
        result = proc_pool.apply_async(worker, args=(i, datas, shared_svm_samples, lock))
        # print result.successful()
        time.sleep(0.5)
    # 关闭线程池
    proc_pool.close()
    # 等待线程池中的所有子进程结束
    proc_pool.join()
    # print_queue(shared_svm_samples)
    print u"end"
    print_queue(shared_svm_samples)

def worker2(q1,q2,lock,L1):
    with lock:
        while q1.qsize() > 0 and L1[0].value > 0:
            x = q1.get()
            q2.put(x*100)
            L1[0].value -= 1
            # print u'pid:',os.getpid(), 'x=',x, 'N(q2)=', q2.size(), "N(q1)=", q1.size()

def test_pool2():
    manager = Manager()
    q1 = manager.Queue()
    for i in range(1000):
        q1.put(random.random())

    q2 = manager.Queue()
    lock = manager.Lock()
    L1 = manager.list()
    n = manager.Value('i', 10)
    L1.append(n)
    # 线程池
    # n_cpu_cores = multiprocessing.cpu_count()
    n_cpu_cores = 6
    proc_pool = Pool(processes=n_cpu_cores)
    # datas = [1,2,3,4,5,6,7,8,9,10]
    # 遍历每条分支并修改风阻
    # for i in range(10):
    proc_num = 0
    while q2.qsize() < 50:
        # 每条分支进行50次网络解算,得到50组数据
        result = proc_pool.apply_async(worker2, args=(q1, q2, lock, L1))
        proc_num = proc_num + 1
        print u'启动第%d个进程' % (proc_num)
        # time.sleep(0.1)
    # 关闭线程池
    proc_pool.close()
    # 等待线程池中的所有子进程结束
    proc_pool.join()
    # print_queue(shared_svm_samples)
    print u"end"
    # print_queue(q1)
    print q1.qsize(), q2.qsize(), [x.value for x in L1]
    # print_queue(q2)

def main():
    # test_multiprocess()
    # test_pool()
    test_pool2()

if __name__=="__main__":
    main()   