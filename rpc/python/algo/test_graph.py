#-*- coding:utf-8 -*-
import numpy as np

# python标准库里的优先队列
from Queue import PriorityQueue

from igraph import *
from vno import *

#测试python的优先队列模块
def test_priority_queue():
    class Node:
        def __init__(self,a,b):
            self.a = a
            self.b = b
        def __lt__(self, other):
            return self.a < other.a  # 最小优先队列
            return self.a > other.a  # 最大优先队列
        def __str__(self):
            return 'a=%d b=%d' % (self.a, self.b)
    pq = PriorityQueue()
    pq.put(Node(-1,2))
    pq.put(Node(-2,2))
    pq.put(Node(-3,2))
    pq.put(Node(-4,2))
    print pq.get(), pq.get()

def test_max_flow():
    dg = Graph(directed=True)
    dg.add_vertices(8)
    dg.add_edges([(0,4), (0,5), (0,7), (1,5), (2,4), (2,6), (3,5)])
    dg.es['capacity'] = DBL_MAX
    # 增加虚拟的源点s和汇点t
    dg.add_vertices(2)
    dg.add_edges([(8,0), (8,1), (8,2), (8,3)])
    dg.add_edges([(4,9), (5,9), (6,9), (7,9)])
    dg.es[7:]['capacity'] = 1
    print dg.es['capacity']
    ss, tt = 8, 9
    # all_st_mincuts
    flow = dg.maxflow(ss, tt, capacity="capacity")
    print u'最大流:', flow.value
    print u'最小割:', [dg.es[i].tuple for i in flow.cut]
    # print u'去掉最小割分支后左右两侧的节点V1和V2', flow.partition

#测试dfs
def test_dfs_path():
    dg = Graph(directed=True)
    dg.add_vertices(4)
    dg.add_edges([(0,1), (1,2), (1,3)])
    print dfs_path(dg, 0, 3)
    print dfs_path(dg, 0, 2)
    print dfs_path(dg, 3, 0)
    print dfs_path(dg, 2, 3)
    print u'-----------------------'
    print node_path_to_edge_path(dg, dfs_path(dg, 0, 3))
    print node_path_to_edge_path(dg, dfs_path(dg, 0, 2))
    print node_path_to_edge_path(dg, dfs_path(dg, 3, 0))
    print u'-----------------------'
    #转为无向图
    dg.to_undirected()
    print dfs_path(dg, 2, 3)
    print dfs_path(dg, 3, 2)

if __name__=="__main__":
    # test_pygraphviz()
    test_priority_queue()
    # test_max_flow()
    # test_dfs_path()