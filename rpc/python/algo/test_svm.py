#-*- coding:utf-8 -*-
import os
import time, math, random
import numpy as np
import multiprocessing  # 多进程
# import pandas as pd
import matplotlib.pyplot as plt
# from pylab import *  
# mpl.rcParams['font.sans-serif'] = ['SimHei'] #指定默认字体    
# mpl.rcParams['axes.unicode_minus'] = False #解决保存图像是负号'-'显示为方块的问题  

#from scipy.cluster.hierarchy import linkage,fcluster,dendrogram
#from scipy.cluster.vq import vq, kmeans, whiten,kmeans2
#from scipy import spatial

#from sklearn import metrics
#from sklearn.cluster import KMeans,Ward
#from sklearn.decomposition import PCA
#from sklearn.preprocessing import scale
from sklearn import svm

from vno import *
from vno_path import *
from vno_data import *

def get_dict_value(kwargs, key, value):
    if key in kwargs:
        return kwargs[key]
    else:
        return value

# 生成风阻r数据用于svm训练
# n表示风阻个数, c表示风阻变化的间距
def build_r_1(dg, e, kwargs):
    n = get_dict_value(kwargs, 'n', 50)
    c = get_dict_value(kwargs, 'c', 0.1)
    # 匿名函数(生成n个风阻,按10%的间距增加风阻值)
    f = lambda e: [(1+c*i)*e['r'] for i in range(1,n+1)]
    return f(e)

# 生成风阻r数据用于svm预测
# n表示风阻个数, c表示风阻变化的间距
def build_r_3(dg, e, kwargs):
    n = get_dict_value(kwargs, 'n', 50)
    c = get_dict_value(kwargs, 'c', 0.1)
    # mu = get_dict_value(kwargs, 'mu', 0)
    # sigma = get_dict_value(kwargs, 'sigma', 1)
    # 匿名函数(生成n个风阻,按正态分布概率计算风阻风阻)
    f = lambda e: np.abs(np.random.normal(e['r'], e['r']*c, n)).tolist()
    return f(e)

def select_build_r_func(func_id):
    if func_id == 1:
        return build_r_1
    elif func_id == 2:
        return build_r_1
    else:
        return build_r_3

def each_vno(vnet, e, r):
    dg = vnet.graph()
    # 风阻不变的时候,解算得到的风量
    Q0 = [ee['q'] for ee in dg.es] # 解算风量
    R0 = [ee['r'] for ee in dg.es] # 原始风阻
    P0 = [vv['p'] for vv in dg.vs] # 节点压能
    # 记录原始的分支风阻
    r0 = e['r']
    # 样本数据
    each_sample = []
    # 修改分支风阻
    e['r'] = r
    # 通风网络解算
    ret = vno(vnet)
    if ret:
        # print_network(vnet, msg='通风网络解算结果(r%d=%.3f --> %.3f)' % (e['id'], r0, r))
        # 收集风阻、风量、总风量数据
        Q = [ee['q'] for ee in dg.es]  # 解算风量
        P = [vv['p'] for vv in dg.vs]  # 节点压能
        # important:通过多次的实验发现,输入的是Δq或Δp等数据,svm基本无能为力!!!输入q值则效果较好!!
        # 多多个array连接在一起(格式: 总风量   风阻   风量变化   |   原始风阻   风阻变化值   风阻变化位置编号  节点压能变化)
        # 方法: np.concatenate(([1,2],[4,5],[666]))
        # X = np.concatenate( ( [ vnet.totalFlow() ], R0, np.subtract(Q, Q0), [ r0, r-r0, e['id'] ], np.subtract(P, P0) ) )
        X = np.concatenate( ( [ vnet.totalFlow() ], R0, Q, [ r0, r-r0, e['id'] ], np.subtract(P, P0) ) )
        # 多多个array连接在一起(格式: 总风量   风阻   风量变化   |   原始风阻   风阻变化值   风阻变化位置编号  节点压能)
        # X = np.concatenate( ( [ vnet.totalFlow() ], R0, np.subtract(Q, Q0), [ r0, r-r0, e['id'] ], P ) )
        # 增加一个样本
        each_sample.extend(X.tolist())
    # 恢复分支风阻
    e['r'] = r0
    return  each_sample

# 进程执行状态
def state_2_str(i):
    if i==0:
        return '正在运行'
    else:
        return '已结束'

# 字符串对齐
# http://blog.csdn.net/sbdxxcjh/article/details/38051573?locationNum=9
def align_str(string, length=0):
    if length == 0:
        return string
    slen = len(string)
    re = string
    if isinstance(string, str):
        placeholder = ' '
    else:
        placeholder = u'  '
    while slen < length:
        re += placeholder
        slen += 1
    return re

# 管理多进程的状态和数据(共享变量)
class State:
    def __init__(self):
        pass
    
    def init(self, manager):
        self.lock = manager.Lock()
        # 共享变量: svm样本数据
        self.samples = manager.Queue()
        # 共享变量: vno参数
        self.R50 = manager.Queue()
        # 各worker的状态
        self.r50_state = manager.Value('i', 0) # 可扩展为多个
        self.vno_states = manager.list() # 不能使用python自带的list!
        self.file_state = manager.Value('i', 0) # 可扩展为多个
        self.align_length = 12 # 字符串对齐用!

    def add_r(self, x):
        self.R50.put(x)

    def get_r(self):
        return self.R50.get()

    def add_sample(self, x):
        self.samples.put(x)

    def get_sample(self):
        return self.samples.get()

    def is_r50_empty(self):
        return self.R50.qsize() == 0

    def is_sample_empty(self):
        return self.samples.qsize() == 0

    def add_vno(self, manager):
        self.vno_states.append(manager.Value('i', 0))
        return len(self.vno_states)-1

    def is_r50_running(self):
        return self.r50_state.value == 0

    def end_r50_worker(self):
        with self.lock:
            self.r50_state.value = 1    
    
    def is_vno_running(self, i):
        if i < 0 or i >= len(self.vno_states):
            return False
        return self.vno_states[i].value == 0

    '''
    问题: 在多进程下无法修改list的内容!!!
    来源:https://docs.python.org/2/library/multiprocessing.html
        Note Modifications to mutable values or items in dict and list proxies will not be propagated through the manager, 
        because the proxy has no way of knowing when its values or items are modified. 
        To modify such an item, you can re-assign the modified object to the container proxy:
    '''
    def end_vno_worker(self, i):
        if i < 0 or i >= len(self.vno_states):
            return
        with self.lock:
            state = self.vno_states[i]
            state.value = 1
            self.vno_states[i] = state # re-assign才能生效!

    def is_all_vno_end(self):
        return sum([state.value for state in self.vno_states]) == len(self.vno_states)

    def is_file_running(self):
        return self.file_state.value == 0

    def end_file_worker(self):
        with self.lock:
            self.file_state.value = 1
    
    # def printf(self):
    #     with self.lock:
    #         print u'------------- %s -------------' % (time.strftime('%Y-%m-%d %X', time.localtime(time.time())))
    #         print u'进程类型\t个数\t剩余数据\t进程状态'
    #         print u'r50_worker\t1个\t%d个    \t%s' % (self.R50.qsize(), state_2_str(self.r50_state.value))
    #         msg = '  '.join([state_2_str(state.value) for state in self.vno_states])
    #         print u'vno_worker\t%d个\t%d个    \t%s' %(len(self.vno_states), self.samples.qsize(), msg)
    #         print u'file_worker\t1个\t--      \t%s' % (state_2_str(self.file_state.value))

    def __align(self, s):
        return align_str(s, self.align_length)
    # 格式化输出
    def __print_title(self):
        print self.__align('进程'), self.__align('个数'), self.__align('剩余数据'), self.__align('状态')
    def __print_data(self, name, worker_nums, data_nums, state, length=15):
        print self.__align(name), self.__align('%d个' % worker_nums), self.__align('%d' % data_nums), self.__align(state)
    def printf(self):
        with self.lock:
            print u'-------------------- %s --------------------' % (time.strftime('%Y-%m-%d %X', time.localtime(time.time())))
            self.__print_title()
            self.__print_data('r50_worker', 1,  self.R50.qsize(), state_2_str(self.r50_state.value))
            msg = '  '.join([state_2_str(state.value) for state in self.vno_states])
            self.__print_data('vno_worker', len(self.vno_states), self.samples.qsize(), '['+msg+']')
            self.__print_data('file_worker', 1, 0, state_2_str(self.file_state.value))

def state_worker(state):
    do_work = True
    while do_work:
        if not state.is_file_running():
            do_work = False
            break
        else:
            time.sleep(1)
            state.printf()

def r50_worker(vnet, edges, func_id, kwargs, state):
    if not state.is_r50_running():
        return
    dg = vnet.graph()
    g_r = select_build_r_func(func_id)
    for i in edges:
        e = dg.es[i]
        r50 = g_r(dg, e, kwargs)
        for r in r50:
            # time.sleep(0.3)
            state.add_r((i, r))
    state.end_r50_worker() # 标记r50进程结束
    # print u'pid:', os.getpid()
    print u'结束r50_worker进程'

def vno_worker(vnet, n_vno_nums_per_worker, state, k):
    # print u'pid:', os.getpid()
    if not state.is_vno_running(k):
        return
    dg = vnet.graph()
    do_work = True
    while do_work:
        for j in range(n_vno_nums_per_worker):
            if state.is_r50_empty():
                if not state.is_r50_running():
                    do_work = False
                    break
            else:
                i, r = state.get_r()
                e = dg.es[i]
                X = each_vno(vnet, e, r)
                if len(X) > 0:
                    # 增加一个样本
                    state.add_sample(X)
    state.end_vno_worker(k)  # 标记进程结束
    print u'结束第%d个vno_worker进程' % (k)

def file_worker(filename, n_write_cache, state):
    if not state.is_file_running():
        return        
    do_work = True
    while do_work:
        small_samples = []
        for k in range(n_write_cache):
            if state.is_sample_empty():  # 队列为空(可能表示已经全部被file_worker取完,也可能表示计算过程较慢,还跟不上file_worker的速度)
                if state.is_all_vno_end():  # vno计算全部结束
                    do_work = False
                    break
            else:
                # 取出一个样本
                small_samples.append(state.get_sample())        
        if len(small_samples) == 0:
            time.sleep(0.3) # vno计算比较慢,再耐心等待~
        else:
            f = open(filename, 'a')
            for X in small_samples:
                # 格式化列表,并写入到文件中
                s = '\t'.join(['%.4f' % x for x in X])
                f.write(s)
                f.write('\n')
            f.close()
    state.end_file_worker() # 进程结束
    print u'结束file_worker进程'

# g表示一个风阻变化函数g(r)
def make_sample_multiprocess(vnet, edges, filename, func_id, kwargs):
    # 新建文件
    f = open(filename, 'w')
    f.close()

    # 使用内置的线程池管理器
    # 涉及到父进程与子进程之间的数据通信(使用线程池的时候)
    # http://www.cnblogs.com/yygsj/p/5829774.html
    manager = multiprocessing.Manager()
    # 状态管理器(不能将manager作为参数传递到类的构造函数中!)
    state = State()
    state.init(manager)
    # cpu核心个数
    n_cpu_cores = get_dict_value(kwargs, 'n_cpu_cores', 2)
    # 启动的进程个数
    n_vno_workers = get_dict_value(kwargs, 'n_vno_workers', 2)
    # 每个vno_worker进行网络解算的次数
    n_vno_nums_per_worker = get_dict_value(kwargs, 'n_vno_nums_per_worker', 1)
    # 每个file_worker写入到文件中的数据个数
    n_write_cache = get_dict_value(kwargs, 'n_write_cache', 1)

    if n_cpu_cores < n_vno_workers+3:
        # n_cpu_cores = n_vno_workers + 3
        print u'【警告】n_cpu_cores的个数小于%d(当前个数%d)!' % (n_vno_workers+3, n_cpu_cores)
        return

    # 线程池
    proc_pool = multiprocessing.Pool(processes=n_cpu_cores)
    # 启动监视进程
    proc_pool.apply_async(state_worker, args=(state,))
    # 启动风阻数据生成进程,并加入到线程池
    print u'启动风阻生成进程...'
    proc_pool.apply_async(r50_worker, args=(vnet, [e.index for e in edges], func_id, kwargs, state))
    # 启动vno计算进程,并加入到线程池
    for i in range(n_vno_workers):
        # 每条分支进行50次网络解算,得到50组数据
        # 注意: 不能直接传递分支变量e给worker函数,目前只能传递可以被pickle序列化的变量
        # 问题: vnet不知道为啥可以直接传递???[已解决]
        # 原因: VentNetwork类包含dg, sn, tn, airEdge4个成员变量,其中dg是Graph类对象,支持pickle
        #       sn, tn, airEdge都是整数,也支持pickle,所以vnet直接传递没有任何问题
        # Manager类似于rpc技术,通过序列化的方式在进程之间交换数据,要求进程中的所有数据支持pickle!
        # 参考: http://www.xuebuyuan.com/583339.html
        # 打印相关信息
        time.sleep(1) # 挂起主进程,让r50_worker执行一会儿
        print u'启动第%d个通风网络解算进程...' % (i+1)
        k = state.add_vno(manager)
        proc_pool.apply_async(vno_worker, args=(vnet, n_vno_nums_per_worker, state, k))
    # 启动样本文件写入进程,并加入到线程池
    time.sleep(1)   # 挂起主进程,让vno_worker执行一会儿
    print u'启动文件写入进程...'
    proc_pool.apply_async(file_worker, args=(filename, n_write_cache, state))    
    # 关闭线程池
    proc_pool.close()
    # 等待线程池中的所有子进程结束
    proc_pool.join()
    # 打印相关信息
    # state.printf()

def make_vno_data(vnet, e, r_50):
    # 样本数据
    samples = []
    for r in r_50:
        each_sample = each_vno(vnet, e, r)
        if len(each_sample) == 0:
            break
        else:
            # 增加一个样本
            samples.append(each_sample)
    return samples

# g表示一个风阻变化函数g(r)
def make_sample(vnet, edges, filename, func_id, kwargs):
    dg = vnet.graph()
    g_r = select_build_r_func(func_id)
    # 生成风阻变化样本数据
    R_50 = [g_r(dg, e, kwargs) for e in edges]
    # 支持向量机样本
    samples = []
    # 遍历每条分支并修改风阻
    for i, e in enumerate(edges):
        # 每条分支进行50次网络解算,得到50组数据
        datas = make_vno_data( vnet, e, R_50[i] )
        if len(datas) > 0:
            samples.extend(datas)
    # 转换成numpy的2维数组
    samples = np.array(samples)
    # 写入到文件中
    # np.savetxt("train_set.txt", samples, fmt='%.18e', delimiter='\t') # 科学计数法表示浮点数
    np.savetxt(filename, samples, fmt='%f', delimiter='\t')

# 构造训练集
def make_train_set(vnet, edges, filename, func_id, **kwargs):
    # 进行网络解算(无风机无固定风量自然分风)
    if not vno(vnet):
        print u'通风网络解算失败,程序退出!'
        return False
    
    print_network(vnet, msg='通风网络解算结果')
    # print edges

    # 是否并行(利用多进程技术)
    use_parallel = get_dict_value(kwargs, 'use_parallel', False)
    # 生成支持向量机样本
    if use_parallel:
        # 并行计算
        make_sample_multiprocess(vnet, [e for i,e in edges], filename, func_id, kwargs)
    else:    
        # 串行计算
        make_sample(vnet, [e for i,e in edges], filename, func_id, kwargs)
    return True

def single_svm_train(X, Y, **kwargs):
    C = get_dict_value(kwargs, 'C', 1e3)
    epsilon = get_dict_value(kwargs, 'epsilon', 0.001)
    gamma = get_dict_value(kwargs, 'gamma', 'auto')
    kernel = get_dict_value(kwargs, 'kernel', 'rbf')
    # (4)支持向量机回归训练(使用sklearn提供的SVR类)
    #       kernel  -- 核函数一般采用rbf(径向基函数)    
    #       C       -- 代表惩罚因子,C越到代表惩罚越严厉(如果C太大，可能导致svm无法工作!!!)
    #       epsilon -- 不太明确什么含义(这里采用的SVR称为"Epsilon SVR")
    # 注意: SVR的几个参数需要不断的试!!!
    # 参考用法
    # clf=svm.SVR(C=1e3, epsilon=0.001, gamma='auto', kernel='rbf', tol=1e-3)
    # clf=svm.SVR(C=1e3, epsilon=0.001, gamma='auto', kernel='linear', tol=1e-3)
    clf=svm.SVR(C=C, epsilon=epsilon, gamma=gamma, kernel=kernel, tol=1e-3)
    clf.fit(X, Y)
    return clf

# 根据参数构造不同的输入X
def make_train_x(A, useR, useDeltaQ, useDeltaH):
    train_x = []
    if useR:
        if len(train_x) == 0:
            train_x = A[:, 1:8]
        else:
            train_x = np.concatenate( (train_x, A[:, 1:8]), axis=1)
    if useDeltaQ:
        if len(train_x) == 0:
            # train_x = A[:, 8:15]
            train_x = np.concatenate( (A[:,8:9], A[:, 11:13]), axis=1)
        else:
            # train_x = np.concatenate( (train_x, A[:, 8:15]), axis=1)
            train_x = np.concatenate( (train_x, A[:,8:9], A[:, 11:13]), axis=1)
    if useDeltaH:
        if len(train_x) == 0:
            train_x = A[:, 18:24]
        else:
            train_x = np.concatenate( (train_x, A[:, 18:24]), axis=1)
    return train_x

'''
支持向量机训练(取多个条分支 或 所有分支的数据进行训练)
filename -- 训练样本数据文件
n        -- 通风网络的分支个数
edges    -- 要训练的分支,例如: [(1, e1), (6, e6)], 其中e1、e6是一个igraph.Edge类型对象
kwargs   -- 可变参数,目前支持useR, useDeltaQ, useDeltaH这3个参数决定是否取R、ΔQ, ΔH作为输入
'''
def svm_train(filename, n, edges, **kwargs):
    # 提取参数
    useR = get_dict_value(kwargs, 'useR', True)
    useDeltaQ = get_dict_value(kwargs, 'useDeltaQ', True)
    useDeltaH = get_dict_value(kwargs, 'useDeltaH', True)
    # (1)读取文件得到所有数据
    A = np.loadtxt(filename, delimiter='\t')
    # useR, useDeltaQ, useDeltaH这3个参数决定是否取R、ΔQ, ΔH作为输入
    # R在矩阵A的[1:8)列, ΔQ在[8:15)列, ΔH在[18:24)列
    train_x = make_train_x(A, useR, useDeltaQ, useDeltaH)
    # 提取数据失败,返回空列表
    if len(train_x) == 0: return []

    # (3)取第16列作为输出(第i条分支的风阻变化Δri)
    train_y = A[:, 16]    # 第i条分支风阻变化Δri
    # 每条分支的数据个数
    m = len(train_x)/n
    # 存放支持向量机的列表
    svms = []
    # i表示序号,e表示分支对象
    for k, e in edges:
        # 取分支对应的数据
        start, end = (k-1)*m, k*m
        # 支持向量机回归训练(使用sklearn提供的SVR类)
        clf = single_svm_train(train_x[start:end], train_y[start:end], C=1e3, epsilon=0.001, gamma='auto', kernel='rbf')
        print u'支持向量个数:%d' % len(clf.support_vectors_)
        # for v in clf.support_vectors_:
        #     print v
        # print clf.support_
        # 增加到svms列表中
        svms.append(clf)
    return svms

'''
支持向量机预测
filename -- 检验样本数据文件
n        -- 通风网络的分支个数
edges    -- 要检验的分支,例如: [(1, e1), (6, e6)], 其中e1、e6是一个igraph.Edge类型对象
svms     -- svm_train()函数训练出来的svm对象(与edges是一一对应的)
kwargs   -- 可变参数,目前支持useR, useDeltaQ, useDeltaH这3个参数决定是否取R、ΔQ, ΔH作为输入
'''
def svm_predict(filename, n, edges, svms, **kwargs):
    # 如果没有传入svm对象或分支列表则返回
    if len(svms) == 0 or len(edges) == 0 or len(svms) != len(edges): return

    # 提取参数
    useR = get_dict_value(kwargs, 'useR', True)
    useDeltaQ = get_dict_value(kwargs, 'useDeltaQ', True)
    useDeltaH = get_dict_value(kwargs, 'useDeltaH', True)

    # (1)用训练好的svm进行预测
    # 读取风阻变化样本数据(取第[9, 16)列作为输入(只使用风量变化Δq数据))
    A = np.loadtxt(filename, delimiter='\t')
    # useR, useDeltaQ, useDeltaH这3个参数决定是否取R、ΔQ, ΔH作为输入
    # R在矩阵A的[1:8)列, ΔQ在[8:15)列, ΔH在[18:24)列
    train_x = make_train_x(A, useR, useDeltaQ, useDeltaH)
    # 提取数据失败,函数退出
    if len(train_x) == 0: return
    # (2)取第16列作为输出(第i条分支的风阻变化Δri)
    train_y = A[:, 16]    # 第i条分支风阻变化Δri
    # 每条分支的数据个数
    m = len(train_x)/n
    # 图形编号
    fig_num = 1
    # i表示序号,e表示分支对象
    for i, t in enumerate(edges):
        # 取出第k条分支对应的数据
        k, e = t
        start, end = (k-1)*m, k*m
        X, Y0 = train_x[start:end], train_y[start:end]
        # svm训练器
        clf= svms[i]
        # (6) 预测的y值
        Y = clf.predict(X)
        # (7)获取svm参数
        params = clf.get_params()
        c, epsilon, kernel = params['C'], params['epsilon'], params['kernel']
        # (8)绘制图形
        # 这条曲线由于数据误差太大,导致看起来比较平缓,实质取某段数据后会发现波动也是很大的!
        plt.figure(fig_num)
        fig_num = fig_num+1
        # 第1幅子图(sub plot)
        plt.subplot(211)
        plt.plot([100*(a-b)/b for a, b in zip(Y, Y0)])
        plt.xlabel('Number')
        plt.ylabel('Error(%)')
        plt.title('Support Vector Regression of e%d (c=%d epsilon=%.3f kernel=%s)' % (e['id'], c, epsilon, kernel))
        # 第2幅子图(sub plot)
        plt.subplot(212)
        plt.scatter(range(1,m+1), Y,  color='red', s=50, label='predict delta r')
        plt.scatter(range(1,m+1), Y0,  color='blue', label='real delta r')
        plt.xlabel('Number')
        plt.ylabel('Delta R')
        plt.legend()
    plt.show()

# 支持向量机预测
def test_r_q(filename):
    # (5)用训练好的svm进行预测
    # 读取风阻变化样本数据(取第[9, 16)列作为输入(只使用风量变化Δq数据))
    A = np.loadtxt(filename, delimiter='\t')
    train_x = A[:, 0]     # 总风量
    train_y = A[:, 16]    # 第i条分支风阻变化Δri
    # 样本个数 和 分支个数
    m, n = len(train_x), 7
    # 每条分支的数据个数
    m = m/n
    print m,n
    # 图形编号
    fig_num = 1
    # i表示序号,e表示分支对象
    for k in range(n):
        # 取出第k条分支对应的数据
        start, end = k*m, (k+1)*m
        Q, DR = train_x[start:end], train_y[start:end]
        print DR
        # (8)绘制图形
        # plt.figure(fig_num)
        # fig_num = fig_num+1
        plt.plot(DR, Q, label='e%d' % (k+1))
        plt.xlabel('dr')
        plt.ylabel('Q')
        # plt.title('Support Vector Regression of e%d (c=%d epsilon=%.3f kernel=%s)' % (e['id'], c, epsilon, kernel))
        plt.legend()
    plt.show()

def select_edges(dg, eIds):
    edges = []
    eIds_set = set(eIds)
    for i, e in enumerate(dg.es):
        eId = int(e['id'])
        if eId == 0:
            continue
        if eId in eIds_set:
            edges.append((i+1, e))
    return edges

def main():
    svm_config = read_json_file(SVM_CONFIG_FILE)

    # 1. 准备通风网络数据
    print u'\n1、构造通风网络'
    graph_datas = read_graph_datas(svm_config['graph_datas'])
    # 2. 构造通风网络,读取并设置相关数据
    vnet = VentNetwork()
    if not build_network(graph_datas, vnet.graph()):
        print u'\t-->构造通风网络失败!!!'
        return

    # 4. 添加虚拟源汇,将网络变成单一源汇通风网络
    vnet.addVirtualST()
    print u'\t-->构造通风网络成功!!!'
    # 先执行一次网络解算
    if not vno(vnet):
        print u'通风网络解算失败,程序退出!'
        return

    dg = vnet.graph()
    # 用户选择要生成用本的分支
    sample_edges = select_edges(dg, svm_config['sample_edges'])
    if svm_config['sample_edges'][0] == -1:
        print u'"【注意】svm_config.json中sample_edges的值是一个数组,其第1个数据为-1'
        print u'用户选择通风网络的所有分支生成样本!'
        sample_edges = [(i+1, e) for i,e in enumerate(dg.es) if not is_zero_edge(e)]
    elif len(sample_edges) == 0:
        print u'用户选择不生成样本数据,程序退出!'
        return
    # 打印通风网络
    print_network(vnet, edges=[e for i,e in sample_edges])

    # 并行参数
    use_parallel = svm_config['use_parallel']
    n_cpu_cores = svm_config['n_cpu_cores']
    n_vno_nums_per_worker = svm_config['n_vno_nums_per_worker']
    n_vno_workers = svm_config['n_vno_workers']
    n_write_cache = svm_config['n_write_cache']

    # 询问
    node = svm_config['train_set']
    if node['new']:
        # 3. 生成训练样本数据文件(train_set.txt)
        # 每条分支线性变化n次,按c的比例增加风阻值
        make_train_set(vnet, sample_edges,
            node['file'], node['func'], n=node['n'], c=node['c'],
            use_parallel=use_parallel, 
            n_cpu_cores=n_cpu_cores, n_vno_workers=n_vno_workers,
            n_vno_nums_per_worker=n_vno_nums_per_worker, n_write_cache=n_write_cache)
    else:
        print u'\t-->用户选择不重新生成训练样本数据!'

    node = svm_config['test_set']
    if node['new']:
        # 4. 生成测试样本数据文件(test_set.txt)
        # 每条分支随机变化(正态分布)
        make_train_set(vnet, sample_edges, node['file'], select_build_r_func(node['func_id']), n=node['n'], c=node['c'])
    else:
        print u'\t-->用户选择不重新生成测试样本数据!'

    return
    # 5. 选择要训练的分支
    if len(sample_edges) == 0:
        print u'\t-->没有选择任何分支,程序退出!'
        # test_r_q('train_set.txt')
    else:
        # 6. 通过useR, useDeltaQ, useDeltaH来控制输入哪些数据进行训练
        # 7. 训练支持向量机
        svms = svm_train(svm_config['train_set']['file'], len(dg.es), sample_edges, useR=svm_config['useR'], useDeltaQ=svm_config['useDeltaQ'], useDeltaH=svm_config['useDeltaH'])
        # 8. 利用支持向量机预测
        svm_predict(svm_config['test_set']['file'], len(dg.es), sample_edges, svms, useR=svm_config['useR'], useDeltaQ=svm_config['useDeltaQ'], useDeltaH=svm_config['useDeltaH'])

    # 11. 删除虚拟源汇
    vnet.delVirtualST()

if __name__=="__main__":
    start = time.time()
    main()
    end = time.time()
    print u'总耗时:%d s' % (end-start)