#-*- coding:utf-8 -*-
import os
import time, math, random
import numpy as np
import multiprocessing  # 多进程
# import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats

from vno import *
from vno_path import *
from vno_data import *

def get_dict_value(kwargs, key, value):
    if key in kwargs:
        return kwargs[key]
    else:
        return value

# 生成风阻r数据用于svm训练
# n表示风阻个数, c表示风阻变化的间距
def build_r_1(dg, e, kwargs):
    n = get_dict_value(kwargs, 'n', 50)
    c = get_dict_value(kwargs, 'c', 0.1)
    # 匿名函数(生成n个风阻,按10%的间距增加风阻值)
    f = lambda e: [(1+c*i)*e['r'] for i in range(1,n+1)]
    return f(e)

# 生成风阻r数据用于svm预测
# n表示风阻个数, c表示风阻变化的间距
def build_r_3(dg, e, kwargs):
    n = get_dict_value(kwargs, 'n', 50)
    c = get_dict_value(kwargs, 'c', 0.1)
    # mu = get_dict_value(kwargs, 'mu', 0)
    # sigma = get_dict_value(kwargs, 'sigma', 1)
    # 匿名函数(生成n个风阻,按正态分布概率计算风阻风阻)
    f = lambda e: np.abs(np.random.normal(e['r'], e['r']*c, n)).tolist()
    return f(e)

def select_build_r_func(func_id):
    if func_id == 1:
        return build_r_1
    elif func_id == 2:
        return build_r_1
    else:
        return build_r_3

def each_vno(vnet, e, r):
    dg = vnet.graph()
    # 风阻不变的时候,解算得到的风量
    Q0 = [ee['q'] for ee in dg.es] # 解算风量
    R0 = [ee['r'] for ee in dg.es] # 原始风阻
    P0 = [vv['p'] for vv in dg.vs] # 节点压能
    # 记录原始的分支风阻
    r0 = e['r']
    # 样本数据
    each_sample = []
    # 修改分支风阻
    e['r'] = r
    # 通风网络解算
    ret = vno(vnet)
    if ret:
        # print_network(vnet, msg='通风网络解算结果(r%d=%.3f --> %.3f)' % (e['id'], r0, r))
        # 收集风阻、风量、总风量数据
        Q = [ee['q'] for ee in dg.es]  # 解算风量
        P = [vv['p'] for vv in dg.vs]  # 节点压能
        X = np.concatenate( ( [vnet.totalR(), vnet.totalFlow(), vnet.totalH()], [e['id'], r0, r] ) )
        # 增加一个样本
        each_sample.extend(X.tolist())
    # 恢复分支风阻
    e['r'] = r0
    return  each_sample

def make_vno_data(vnet, e, r_50):
    # 样本数据
    samples = []
    for r in r_50:
        each_sample = each_vno(vnet, e, r)
        if len(each_sample) == 0:
            break
        else:
            # 增加一个样本
            samples.append(each_sample)
    return samples

# g表示一个风阻变化函数g(r)
def make_sample(vnet, edges, filename, func_id, kwargs):
    dg = vnet.graph()
    g_r = select_build_r_func(func_id)
    # 生成风阻变化样本数据
    R_50 = [g_r(dg, e, kwargs) for e in edges]
    # 支持向量机样本
    samples = []
    # 遍历每条分支并修改风阻
    for i, e in enumerate(edges):
        # 每条分支进行50次网络解算,得到50组数据
        datas = make_vno_data( vnet, e, R_50[i] )
        if len(datas) > 0:
            samples.extend(datas)
    # 转换成numpy的2维数组
    samples = np.array(samples)
    # 写入到文件中
    # np.savetxt("train_set.txt", samples, fmt='%.18e', delimiter='\t') # 科学计数法表示浮点数
    np.savetxt(filename, samples, fmt='%f', delimiter='\t')

# 构造训练集
def make_train_set(vnet, edges, filename, func_id, **kwargs):
    # 进行网络解算(无风机无固定风量自然分风)
    if not vno(vnet):
        print u'通风网络解算失败,程序退出!'
        return False
    
    print_network(vnet, msg='通风网络解算结果')
    # print edges

    # 是否并行(利用多进程技术)
    use_parallel = get_dict_value(kwargs, 'use_parallel', False)
    # 生成支持向量机样本
    if use_parallel:
        # 并行计算
        # make_sample_multiprocess(vnet, [e for i,e in edges], filename, func_id, kwargs)
        pass
    else:
        # 串行计算
        make_sample(vnet, [e for i,e in edges], filename, func_id, kwargs)
    return True

def select_edges(dg, eIds):
    edges = []
    eIds_set = set(eIds)
    for i, e in enumerate(dg.es):
        eId = int(e['id'])
        if eId == 0:
            continue
        if eId in eIds_set:
            edges.append((i+1, e))
    return edges

def test_kde(filename):
    Y = np.loadtxt(filename)

    n = 5
    s, t = n*100, (n+1)*100
    R = Y[s:t,0]
    Q = Y[s:t,1]
    H = Y[s:t,2]
    r = Y[s:t,5]

    R.sort()
    Q.sort()
    H.sort()
    r.sort()
    R_kde=stats.gaussian_kde(R)
    Q_kde=stats.gaussian_kde(Q)
    H_kde=stats.gaussian_kde(H)
    r_kde=stats.gaussian_kde(r)

    plt.subplot(221)
    # plt.subplot(411)
    plt.plot(r, r_kde(r), 'r-', label='score kde')
    # plt.xlabel('r')
    # plt.ylabel('Probality of score occured-'+'$p(s)$')
    plt.grid(True)
    # plt.legend(loc=0)

    plt.subplot(222)
    plt.plot(R, R_kde(R), 'b-', label='score kde')
    # plt.xlabel('r')
    # plt.ylabel('Probality of score occured-'+'$p(s)$')
    plt.grid(True)
    # plt.legend(loc=0)

    plt.subplot(223)
    plt.plot(Q, Q_kde(Q), 'b-', label='score kde')
    # plt.xlabel('r')
    # plt.ylabel('Probality of score occured-'+'$p(s)$')
    plt.grid(True)
    # plt.legend(loc=0)

    plt.subplot(224)
    plt.plot(H, H_kde(H), 'b-', label='score kde')
    # plt.xlabel('r')
    # plt.ylabel('Probality of score occured-'+'$p(s)$')
    plt.grid(True)
    # plt.legend(loc=0)

    # 显示曲线图表
    plt.show()

def main():
    svm_config = read_json_file(STOCHASTIC_CONFIG_FILE)

    # 1. 准备通风网络数据
    print u'\n1、构造通风网络'
    graph_datas = read_graph_datas(svm_config['graph_datas'])
    # 2. 构造通风网络,读取并设置相关数据
    vnet = VentNetwork()
    if not build_network(graph_datas, vnet.graph()):
        print u'\t-->构造通风网络失败!!!'
        return

    # 4. 添加虚拟源汇,将网络变成单一源汇通风网络
    vnet.addVirtualST()
    print u'\t-->构造通风网络成功!!!'
    # 先执行一次网络解算
    if not vno(vnet):
        print u'通风网络解算失败,程序退出!'
        return

    dg = vnet.graph()
    # 用户选择要生成用本的分支
    sample_edges = select_edges(dg, svm_config['sample_edges'])
    if svm_config['sample_edges'][0] == -1:
        print u'"【注意】svm_config.json中sample_edges的值是一个数组,其第1个数据为-1'
        print u'用户选择通风网络的所有分支生成样本!'
        sample_edges = [(i+1, e) for i,e in enumerate(dg.es) if not is_zero_edge(e)]
    elif len(sample_edges) == 0:
        print u'用户选择不生成样本数据,程序退出!'
        return
    # 打印通风网络
    print_network(vnet, edges=[e for i,e in sample_edges])

    # 询问
    node = svm_config['train_set']
    if node['new']:
        # 3. 生成训练样本数据文件(train_set.txt)
        # 每条分支线性变化n次,按c的比例增加风阻值
        make_train_set(vnet, sample_edges,
            node['file'], node['func'], n=node['n'], c=node['c'])
    else:
        print u'\t-->用户选择不重新生成训练样本数据!'

    # 11. 删除虚拟源汇
    vnet.delVirtualST()

    test_kde('out\\stochastic_vno.txt')

if __name__=="__main__":
    start = time.time()
    main()
    end = time.time()
    print u'总耗时:%d s' % (end-start)