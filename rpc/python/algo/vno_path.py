#-*- coding:utf-8 -*-

GA_PNG_FILE = 'out\\result_ga.png'
GA_STAT_FILE = 'out\\统计信息.txt'

SAMPLE_DOT_FILE = 'out\\sample.dot'
SAMPLE_PNG_FILE = 'out\\sample.png'

SST_MATRIX_FILE = 'out\\灵敏度矩阵.txt'
SST_EDGE_FILE = 'out\\灵敏度分支编号.txt'

SST_FILE = 'out\\相对灵敏度.txt'

SVM_TRAIN_SET_FILE = 'out\\train_set.txt'
SVM_TEST_SET_FILE = 'out\\test_set.txt'

QH_ORDER_FILE = 'out\\qh_order.txt'
QH_DXF_FILE = 'out\\qh.dxf'

QH_HLK_FILE = 'data\\hlk.txt'

GA_CONFIG_FILE = 'data\\ga_config.json'
SVM_CONFIG_FILE = 'data\\svm_config.json'
STOCHASTIC_CONFIG_FILE = 'data\\stochastic_config.json'