#-*- coding:utf-8 -*-
import numpy as np

from vno import *
from sst import *
from vno_dot import *
from vno_path import *
from vno_data import *

def test_mrp(vnet):
    # 测试第k最大阻力路线
    for k in range(1,5):
        # 搜索第k最大阻力路线
        P = mrp_k(vnet, -1, -1, k)
        # 计算路线的总阻力
        H = vnet.hPath(P)
        print u'第%d最大阻力路线总阻力:%.3f' % (k, H)
        # 打印第k最大阻力路线
        print_max_resistance_path(vnet, P)
        if len(P) == 0:continue
        # 标准化dot文件名称(test.dot, test.png)
        dot_file, png_file = standardize_dot_file_name('out\\mrp%d' % k)
        # 将通风网络写入到dot文件
        write_vnet_network_to_dotfile(vnet.graph(), dot_file)
        # 高亮第k最大阻力路线
        write_path_to_dotfile(vnet.graph(), dot_file, P)
        # 绘制并保存png图片
        draw_dot_file(dot_file, png_file)

def test_apm(vnet, fan):
    # 搜索10条最大阻力路线
    more_maxP = apm_mrp(vnet, fan, 10.0, DBL_MAX, 10)
    # 打印并绘制基于通路法搜索的最大阻力路线
    for k, P in enumerate(more_maxP):
        H = vnet.hPath(P)
        print u'基于通路法的第%d最大阻力路线总阻力:%.3f' % (k+1, H)
        # 打印第k最大阻力路线
        print_max_resistance_path(vnet, P)
        if len(P) == 0:continue
        # 标准化dot文件名称(test.dot, test.png)
        dot_file, png_file = standardize_dot_file_name('out\\apm%d' % (k+1))
        # 将通风网络写入到dot文件
        write_vnet_network_to_dotfile(vnet.graph(), dot_file)
        # 高亮第k最大阻力路线
        write_path_to_dotfile(vnet.graph(), dot_file, P)
        # 绘制并保存png图片
        draw_dot_file(dot_file, png_file)

def test_sst(vnet):
    # 13. 计算灵敏度
    E, D = edge_sst(vnet, nCount=20, factor=0.4, accuracy=0.001)
    print u'灵敏度矩阵写入到文件: "%s"' % (SST_MATRIX_FILE)
    np.savetxt(SST_MATRIX_FILE, D, fmt='%.4f')
    np.savetxt(SST_EDGE_FILE, ['e%d' % (e['id']) for e in E], fmt='%s')
    # 计算相对灵敏度
    FD1 = forward_sst1(D)
    BD1 = backward_sst1(D)
    FD2 = forward_sst2(D)
    BD2 = backward_sst2(D)
    # 相对灵敏度写入文件
    print u'相对灵敏度写入到文件: "%s"' % (SST_FILE)
    f = open(SST_FILE, 'w')
    for i in range(len(FD1)):
        s = 'e%d\t%.4f\t%.4f\t%.4f\t%.4f\n' % (E[i]['id'], FD1[i], BD1[i], FD2[i], BD2[i])
        f.write(s)
    f.close()
    # print D

def test_vno():
    # 1. 准备通风网络数据
    # (1) 用一个词典作为输入数据(包含拓扑关系和分支等数据)    
    graph_datas = build_graph_data1()
    # graph_datas = build_graph_data2()
    # graph_datas = build_graph_data3()
    # graph_datas = build_graph_data4()
    # graph_datas = build_graph_data5()
    # graph_datas = build_graph_data6()
    # graph_datas = build_graph_data7()    
    # (2) 或者通过读取文件构造这个词典
    # graph_datas = read_graph_datas('data\\graph_data12.json')
    # print graph_datas
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    graph_datas['qFixs'] = [] # 去掉固定风量

    # 3. 构造通风网络对象
    vnet = VentNetwork()
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
    else:
        # print vnet.graph()
        print u'构造通风网络成功!!!'
        # 4. 添加虚拟源汇,将网络变成单一源汇通风网络
        vnet.addVirtualST()
        # print u'虚拟源点:',vnet.vSource(), '虚拟汇点:',vnet.vTarget()
        # 5. 通风网络解算
        ret = vno(vnet)
        PP = []
        if not ret:
            print u'网络解算失败,退出!'
        else:
            # 6. 打印通风网络解算结果
            print_network(vnet, msg='网络解算结果')
            # 7. 测试最大阻力路线功能
            # test_mrp(vnet)
            # 8. 测试通路法调节功能
            # 9. test_apm(vnet, 16)
            # 打印所有风机的最大阻力路线
            print_mrp_fans(vnet)
            PP = mrp_fans(vnet)
        # 11. 删除虚拟源汇
        vnet.delVirtualST()
        # 12. 通风网络生成dot文件
        write_vnet_network_to_dotfile(vnet.graph(), SAMPLE_DOT_FILE)

        # 高亮最大阻力路线
        for fan, P in PP:
            # 高亮第k最大阻力路线
            write_path_to_dotfile(vnet.graph(), SAMPLE_DOT_FILE, P)

        # 是否计算并写入灵敏度(True表示计算, False表示不计算)
        do_sst = False
        if do_sst:
            test_sst(vnet)
        

if __name__=="__main__":
    test_vno()