#-*- coding:utf-8 -*-
#平衡图优化(Q-H图)

import json
import random
import time
import tempfile

# import numpy as np
# import matplotlib.pyplot as plt
# from scipy.spatial import distance

from inspyred import ec
from inspyred.ec import terminators
from inspyred.ec import observers
from inspyred.ec import analysis

# from ga_plot import *
from vno import *
from vno_data import *
# from vno_path import *
from qh_fitness import *
from qh import *

def MakeKeyNodes(vnet):
    dg = vnet.graph()
    return dg.vs.select(_outdegree_ge=2)

def WriteOrderToFile(order, filename):
    # np.savetxt(filename, order, fmt='%d', delimiter='\t')
    pass

#个体生成器
#下面es.envovle()函数中的所有参数构成了一个词典args
def my_generator(random, args):
    # 提取附加的参数
    key_nodes = args['key_nodes']
    return [random.randint(1, v.degree(type='out')) for v in key_nodes]

# 计算适应值
def callFitness(X, args):
    # 提取附加的参数(适应值函数计算器)
    # my_fitness = args['my_fitness']
    return args['my_fitness'].doIt(X)

#种群的适应值函数
#pops是一个数组,它由多个个体组成
def my_evaluator(candidates, args):
    #依次计算每个个体的适应值
    fitness = []
    for X in candidates:
        # 将浮点数四舍五入,强制转换为0和1,并计算个体的适应值
        score = callFitness(X, args)
        #添加到列表
        fitness.append(score)
    return fitness

# def my_terminator(population, num_generations, num_evaluations, args):
#     if num_generations > args['max_generations']:
#       return True
#     min_fitness = args.get('minimum_fitness', 0.01)
#     pop_fitness = my_evaluator([x.candidate for x in population], args)
#     print pop_fitness
#     return max(pop_fitness) < min_fitness

#inspyred也提供了多种终止条件
#例如evaluation_termination--表示适应值计算多少次后就就终止,无论是否达到最优,也不考虑算法是否收敛
#    generation_termination--表示进化多少代之后就终止,无论是否达到最优,也不考虑算法是否收敛
#    average_fitness_termination --表示种群的平均适应值连续多少代没有显著变化就终止,因为进化过程已经收敛或者假收敛了
#如果选择的是evaluation_termination,那么要在下面的envovle函数中设置max_evaluations参数
#es.terminator = terminators.evaluation_termination
#如果选择的是evaluation_termination,那么要在下面的envovle函数中设置max_generations参数
# es.terminator = terminators.generation_termination
#如果选择的是average_fitness_termination,无需设置额外的终止条件参数
#有可能会收敛的比较慢(说不定要算上几个小时),这时可以考虑选择其它的2个终止条件
# es.terminator = terminators.average_fitness_termination
# es.terminator = my_terminator
TerminatorDict = {
    # 按进化代数终止
    1:terminators.generation_termination,
    # 按适应值计算次数终止
    2:terminators.evaluation_termination,
    # 按种群的平均适应值终止
    3:terminators.average_fitness_termination,
    # 自定义终止条件(按最大相对误差达到一定精度后终止)
    # 4:my_terminator
}

# 从词典中选择终止条件函数
def selectTerminator(terminator_type):
    if not (terminator_type in TerminatorDict):
        terminator_type = 1
    return TerminatorDict[terminator_type]

# 遗传算法主框架
def runGA(args):
    #随机数初始化
    rand = random.Random()
    #如果设置的seed是一个固定值,那么优化算法的结果始终是固定不变的!!!
    #利用当前时间作为随机数种子,就可以看到不同的优化结果!!!
    rand.seed(int(time.time()))

    #选择优化算法
    #inspyred提供了多种智能优化算法
    #例如GA(遗传算法)、NSGA2(好像是多目标优化算法)、DEA(微分进化算法)、SA(模拟退火算法)、PSO(粒子群算法)等等
    es = ec.GA(rand)

    #设置算法终止条件
    es.terminator = selectTerminator(args['terminator'])

    #设置数据观察着
    #file_observer是inspyred提供了一个数据接口,它会收集进化过程中的数据,并写入到文件
    #我们可以通过这些文件来生成进化曲线,观察进化过程和优化结果是否满足我们的需要
    es.observer=observers.file_observer
    #es.observer=observers.plot_observer

    # 上下界
    key_nodes = args['key_nodes']
    my_bounder = ec.Bounder([1 for v in key_nodes], [v.degree(type='out') for v in key_nodes])

    stat_file=open(args['stat_file'], 'w')
    ind_file=open(args['ind_file'],'w')

    #设置其它参数并开始进化
    #返回的是最后一次进化的种群
    #如果优化过程真正的收敛了,final_pop里的最优个体就是我们要找的最优解
    final_pop = es.evolve(generator = my_generator,   #个体生成器(indi generator),envovle函数内部调用它连续生成多个个体,组成种群
                        evaluator = my_evaluator,   #评价函数(适应值函数),计算个体的适应值,评价个体的好坏
                        pop_size = args['popsize'], #种群规模(种群内个体的个数)
                        maximize = False,            #求最大优化问题还是最小优化问题
                        bounder = my_bounder,        #个体的上下限(每个分量的范围都限制在[1,n]内)
                        # max_evaluations = 1200,   #如果前面设置的是: es.terminator = terminators.evaluation_termination 需要设置该参数
                        max_generations = args['max_generations'],   #如果前面设置的是: es.terminator = terminators.generation_termination 需要设置该参数
                        mutation_rate = args['mutation_rate'],   #变异率
                        cross_rate = args['cross_rate'],         #交叉率
                        # num_elites = 1,
                        statistics_file = stat_file,   #设置进化统计数据要写入的文件(es.observer=observers.file_observer专用)
                        individuals_file = ind_file,   #设置进化统计数据要写入的文件(es.observer=observers.file_observer专用)
                        my_fitness = args['my_fitness'],     # 适应值计算器对象
                        # minimum_fitness = args['minimum_fitness'],            # 最小适应值(废弃的参数,不再使用!)
                        key_nodes = args['key_nodes'],  # 关键节点
                        vnet = args['vnet']   # 通风网络
                        )
    #进化完成后关闭文件
    stat_file.close()
    ind_file.close()

    #analysis.generation_plot('stat.csv')
    #analysis.allele_plot('ind.csv')

    # Sort and print the best individual, who will be at index 0.
    #对种群进行逆序排序(个体按照适应值从大到小排序,适应值越大,我们认为该个体最优)
    final_pop.sort(reverse=True)
    # 打印最优解(排序后的种群中第1个个体)
    return final_pop[0].candidate

# 写入统计信息
def writeGAStatics(best_indi, ga_config, t):
    # 写入一些统计信息,记录到文件中
    f = open(GA_STAT_FILE, 'w')
    f.write('总计算耗时 -- %d (s)\n' % t)
    f.write('遗传算法参数\n')
    f.write('\t种群大小 -- %d\n' % ga_config['popsize'])  
    f.write('\t交叉率 -- %.2f\n' % ga_config['cross_rate'])
    f.write('\t变异率 -- %.2f\n' % ga_config['mutation_rate'])
    # f.write('\t适应值计算函数 -- %s\n' % getDistFuncName(ga_config['dist_func']))
    f.write('\t终止条件 -- ')
    if ga_config['terminator'] == 1:
        f.write(' 最大进化代数:%d\n' % ga_config['max_generations'])
    elif ga_config['terminator'] == 2:
        f.write(' 最大适应值计算次数:%d\n' % ga_config['max_evaluations'])
    elif ga_config['terminator'] == 3:
        f.write(' 种群平均值自适应收敛(无需设置额外的参数)\n')
    elif ga_config['terminator'] == 4:
        f.write(' 精度:%f' % ga_config['minimum_fitness'])
        f.write(' 最大进化代数:%d\n' % ga_config['max_generations'])
    f.write('\t最优个体 -- %s\n' % str(best_indi))
    f.write('\t最优适应值 -- %f\n' % callFitness(best_indi, ga_config))
    f.close()

def MakeTempFiles(debug=False):
    if debug:
        return 'stat.csv', 'ind.csv'
    else:
        stat_file = os.path.join ( tempfile.mkdtemp() + 'stat.csv' )
        ind_file = os.path.join ( tempfile.mkdtemp() + 'ind.csv' )
        return stat_file, ind_file

def QH_GA(vnet, key_nodes, ga_config, debug=False):
    start = time.time()
    # 读取json格式配置文件
    # ga_config = read_json_file(config)

    # 获取适应值距离计算函数句柄
    # DistFunc = getDistFunc(ga_config['dist_func'])
    # print ga_config

    # 人工设置观察文件的路径(外部得ga_config就不需要传递该参数了!!!)
    stat_file, ind_file = MakeTempFiles(debug=False)
    ga_config["stat_file"] = stat_file
    ga_config["ind_file"] = ind_file

    # 关键节点
    ga_config['key_nodes'] = key_nodes
    # 通风网络
    ga_config['vnet'] = vnet
    # 构造适应值计算器
    ga_config['my_fitness'] = QHFitness(ga_config['vnet'], ga_config['key_nodes'])
    print u'-------------开始GA计算---------------------'
    best_indi = runGA(ga_config)

    print u'最优个体:', best_indi
    print u'最优适应值:', callFitness(best_indi, ga_config)
    print u'-------------结束GA计算---------------------'

    # 绘制图形
    # plotGA(ga_config['stat_file'], False)

    stop = time.time()
    print u'优化计算总耗时:', stop-start

    # 写入遗传算法统计信息    
    # writeGAStatics(best_indi, ga_config, int(stop-start))
    
    # 清除参数对象
    del ga_config

    return best_indi
