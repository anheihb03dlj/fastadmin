#-*- coding:utf-8 -*-
import numpy as np

from vno import *
from vno_dot import *
from apm_ga import *
from vno_path import *
from vno_data import *

def select_edges(dg, eIds):
    #edges = []
    ss = dict([(str(e['id']), e) for e in dg.es])
    return [ss.get(str(i), None) for i in eIds]
    #for i, e in enumerate(dg.es):
    #    eId = e['id']
    #    if eId == '0' or eId == 'NULL' or eId == 'null':
    #        continue
    #    if eId in ss:
    #        edges.append(e)
    #return edges

def print_mrps(vnet):
    # 测试第k最大阻力路线
    for k in range(1,5):
        # 搜索第k最大阻力路线
        P = mrp_k(vnet, -1, -1, k)
        # 计算路线的总阻力
        H = vnet.hPath(P)
        print u'第%d最大阻力路线总阻力:%.3f' % (k, H)
        # 打印第k最大阻力路线
        print_mrp(vnet, P)
        if len(P) == 0:continue
        # 标准化dot文件名称(test.dot, test.png)
        dot_file, png_file = standardize_dot_file_name('out\\mrp%d' % k)
        # 将通风网络写入到dot文件
        write_vnet_network_to_dotfile(vnet.graph(), dot_file)
        # 高亮第k最大阻力路线
        write_path_to_dotfile(vnet.graph(), dot_file, P)

def apm_ga(vnet, apm_config):
    # 用户选择要调节的分支
    adjust_edges = select_edges(vnet.graph(), apm_config['adjust_edges'])
    print u'\n人工指定的可调分支:', ','.join(['e%s' % (e['id']) for e in adjust_edges])
    # 分支风阻调节上下限
    min_delta_r = apm_config['min_delta_r']
    max_delta_r = apm_config['max_delta_r']
    deltaR = ApmGA(vnet, adjust_edges, min_delta_r, max_delta_r, apm_config, debug=True)
    # 修改可调分支风阻
    for e, delta_r in zip(adjust_edges, deltaR):
        e['r'] = e['r'] + delta_r

def main():
    apm_config = read_json_file('data\\apm_config13.json')
    # 1. 准备通风网络数据
    print u'\n1、构造通风网络'
    graph_datas = read_graph_datas(apm_config['graph_datas'])
    #构造通风网络,读取并设置相关数据
    vnet = VentNetwork()
    # hasQ表示读取graph_datas中指定的分支风量q
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
        return

    # 添加虚拟源汇,将网络变成单一源汇通风网络
    vnet.addVirtualST()
    # 如果分支没有指定风量, 进行一次网络解算
    if not graph_datas['hasQ'] and not vno(vnet):
        vnet.delVirtualST()
        print u'通风网络解算失败!'
        return

    # 调整负风量分支
    vnet.adjustNegativeEdges()
    
    print u'===================================调节前==================================='
    # 打印通风网络解算结果
    print_network(vnet, msg='网络解算结果')
    # 打印所有阻力路线(按顺序排列)
    print_mrps(vnet)

    # 使用ga优化调节通路阻力
    apm_ga(vnet, apm_config)

    print u'===================================调节后==================================='
    # 打印通风网络解算结果
    print_network(vnet, msg='网络解算结果')
    # 打印所有阻力路线(按顺序排列)
    print_mrps(vnet)
    # 删除虚拟源汇
    vnet.delVirtualST()

if __name__=="__main__":
    main()