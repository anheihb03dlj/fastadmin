# -*- coding:utf-8 -*-
from igraph import *
from vno import *
# 摘自"Python 3.4.3等待用户输入超时自动跳过"
# https://www.zhaokeli.com/Article/6357.html
import sys,time,msvcrt
def my_input(caption, default, timeout=10):
    start_time = time.time()
    sys.stdout.write('%s(%d秒自动跳过, 默认%s):' % (caption,timeout, str(default)))
    sys.stdout.flush()
    input = ''
    while True:
        ini=msvcrt.kbhit()
        try:
            if ini:
                chr = msvcrt.getche()
                if ord(chr) == 13:  # enter_key
                    break
                elif ord(chr) >= 32:
                    input += chr.decode()
        except Exception as e:
            pass
        if len(input) == 0 and time.time() - start_time > timeout:
            break
    print u''  # needed to move to next line
    if len(input) > 0:
        return str(input)
    else:
        return str(default)
def askNewSample():
    # ans = raw_input('\n2、重新生成样本数据[Y/N]?:')
    ans = my_input('\n2、重新生成样本数据[Y/N]', 'Y', timeout=5)
    ans = ans.lower()
    ret = False
    if ans == 'y' or ans == 'yes':
        ret = True
    return ret
def askNewTest():
    # ans = raw_input('\n3、重新生成检验数据[Y/N]?:')
    ans = my_input('\n3、重新生成检验数据[Y/N]', 'Y', timeout=5)
    ans = ans.lower()
    ret = False
    if ans == 'y' or ans == 'yes':
        ret = True
    return ret
def selectEdges(vnet, edges, msg='请选择分支:'):
    dg = vnet.graph()
    print_network(vnet, msg='通风网络分支列表', edges=edges)
    print u'\n【提示】 输入"all"选择所有的分支(不包括引号)!'
    ans = raw_input(msg)
    ans = ans.lower()
    if ans == 'all':
        return [(i+1, e) for i, e in enumerate(edges)]
    else:
        nums = [int(s) for s in ans.split()]
        return [(i, edges[i-1]) for i in nums]
