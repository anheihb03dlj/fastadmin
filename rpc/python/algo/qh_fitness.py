#-*- coding:utf-8 -*-

from igraph import *
from qh import *

class QHFitness:
    def __init__(self, vnet, key_nodes):
        self.vnet = vnet
        self.key_nodes = key_nodes
    def doIt(self, order):
        # 定义平衡图计算对象
        obj = QH(self.vnet, self.key_nodes)
        # 调整节点出边顺序
        obj.adjustOutSeq(order)
        # 计算分支分割块数
        crossBlocks = obj.buildBlocks(order)
        del obj
        # 分支分隔块数作为适应值
        return crossBlocks