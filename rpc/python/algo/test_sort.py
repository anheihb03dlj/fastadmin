# coding:utf-8

class Node:
    def __init__(self, i, v):
        self.id = i
        self.v = v

def node_cmp1(o1, o2):
    print u'compare:(%d, %d) vs (%d, %d)' % (o1.id, o1.v, o2.id, o2.v)
    if o2.v == -1 and o1.v == -1:
        return cmp(o1.id, o2.id)
    elif o2.v == -1 and o1.v !=-1:
        return -1
    elif o1.v == -1 and o2.v !=-1:
        return -1
    else:
        return cmp(o1.v, o2.v)

def node_cmp2(o1, o2):
    return cmp(o1.v, o2.v)

def simple_cmp(x, y):
    return cmp(x, y)
    
def test1():
    a = [-1, 33, 2, -1, 88, 1, -1]
    # a = [33, 2, 88, 1]
    print a

    nodes = [Node(i, a[i]) for i in range(len(a))]
    print u'排序前：',
    print [(obj.id, obj.v) for obj in nodes]

    # nodes2.sort(cmp=node_cmp1)
    nodes2.sort(cmp=node_cmp2)

    print u'排序后:',
    print [(obj.id, obj.v) for obj in nodes]

    a.sort(cmp=simple_cmp)
    print a

def pair_cmp(o1, o2):
    return cmp(o1[1], o2[1])

def test2():
    a = [-1, 33, 2, -1, 88, 1, -1]
    # a = [33, 2, 88, 1]
    print a

    nodes = [Node(i, a[i]) for i in range(len(a))]
    print u'排序前：',
    print [(obj.id, obj.v) for obj in nodes]

    nodes2 = [(obj.id, obj.v) for obj in nodes if obj.v !=-1]
    nodes2.sort(cmp=pair_cmp)

    print nodes2

    # 将排序的数据按顺序填充到nodes
    k = 0
    for i in range(len(nodes)):
        if nodes[i].v == -1:continue
        nodes[i].id, nodes[i].v = nodes2[k]
        k += 1

    print u'排序后:',
    print [(obj.id, obj.v) for obj in nodes]

    a.sort(cmp=simple_cmp)
    print a

if __name__ == '__main__':
    # test1()
    test2()