#-*- coding:utf-8 -*-

# 导入dxf库
import sdxf

def DrawBlock(d, layer, block):
    x, y, w, h = block['x'], block['y'], block['w'], block['h']
    color = block['color']
    d.append(sdxf.Line(points=[(x,y),(x,y+h)], layer=layer, color=color))
    d.append(sdxf.Line(points=[(x,y+h),(x+w,y+h)], layer=layer, color=color))
    d.append(sdxf.Line(points=[(x+w,y+h),(x+w,y)], layer=layer, color=color))
    d.append(sdxf.Line(points=[(x+w,y),(x,y)], layer=layer, color=color))
    # 文字高度
    height = 0.3
    d.append(sdxf.Text('e%s(Q=%.2f,H=%.2f)' % (block['id'], block['Q'], block['H']), point=(x+w*0.5, y+h*0.5), height=height, layer=layer))

# 辅助方法--绘制平衡图并写入dxf文件
def Draw(blocks, dxfFile='qh.dxf'):
    d=sdxf.Drawing()        
    # 增加一个图层，并将平衡图放置在该图层上
    layer = "Q-H平衡图"
    d.layers.append(sdxf.Layer(name=layer,color=7))
    # 依次绘制每条分支
    for block in blocks:
        # 绘制所有的分支块
        DrawBlock(d, layer, blocks)
    d.saveas(dxfFile)