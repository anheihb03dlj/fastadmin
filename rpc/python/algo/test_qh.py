#-*- coding:utf-8 -*-
import numpy as np

from vno import *
from vno_dot import *
from qh_ga import *
from vno_path import *
from vno_data import *

def test_qh1():
    #准备数据
    graph_datas = read_graph_datas('data\\graph_data10.json')
    # graph_datas['fans'] = [] # 去掉风机
    graph_datas['qFixs'] = [] # 去掉固定风量

    #构造通风网络,读取并设置相关数据
    vnet = VentNetwork()
    # hasQ表示读取graph_datas中指定的分支风量q
    if not build_network(graph_datas, vnet.graph(), hasQ=graph_datas['hasQ']):
        print u'构造通风网络失败!!!'
    else:
        # 添加虚拟源汇,将网络变成单一源汇通风网络
        vnet.addVirtualST();
        # print u'虚拟源点:',vnet.vSource(), '虚拟汇点:',vnet.vTarget()
        # 如果风量已知,就不进行网络解算了
        if graph_datas['hasQ']:
            ret = True
        # 通风网络解算
        else:
            ret = vno(vnet)
        if not ret:
            print u'网络解算失败,退出!'
        else:
            # 调整负风量分支
            vnet.adjustNegativeEdges()
            # 打印通风网络解算结果
            print_network(vnet, msg='网络解算结果')
            # 绘制Q-H平衡图
            #查找所有出度等于0的节点(称为"关键节点"")
            key_nodes = MakeKeyNodes(vnet)
            print u'关键节点个数:', len(key_nodes)
            # 打印所有节点的出边排列组合
            PrintAllVertexOutSeq(vnet.graph(), key_nodes)
            # 使用默认的节点排列序号
            order = [1 for v in key_nodes]
            # 随机生成节点排列序
            # order = [random.randint(1, v.degree(type='out')) for v in key_nodes]
            print u'当前节点排列序:', order
            # 节点排列序写入到文件
            WriteOrderToFile(order, QH_ORDER_FILE)
            DrawQHToDxf(vnet, key_nodes, order, dxfFile=QH_DXF_FILE)
            # test_dfs(vnet, [1,1,2])
        # 删除虚拟源汇
        vnet.delVirtualST()
        # 绘制到dot文件
        write_vnet_network_to_dotfile(vnet.graph(), SAMPLE_DOT_FILE)

def test_qh2():
    # 读取json文件构造通风网络数据
    graph_datas = read_graph_datas('data\\graph_data11.json')
    #构造通风网络,读取并设置相关数据
    vnet = VentNetwork()
    # hasQ表示读取graph_datas中指定的分支风量q
    if not build_network(graph_datas, vnet.graph(), hasQ=graph_datas['hasQ']):
        print u'构造通风网络失败!!!'
    else:
        # 添加虚拟源汇,将网络变成单一源汇通风网络
        vnet.addVirtualST();
        # 如果风量已知,就不进行网络解算了
        if graph_datas['hasQ']:
            ret = True
        # 通风网络解算
        else:
            ret = vno(vnet)
        # 调整负风量分支
        vnet.adjustNegativeEdges()
        # 打印通风网络解算结果
        print_network(vnet, msg='网络解算结果')
        # 查找所有的关键节点(注: 也可以是一部分出度大于2的节点!!!)
        key_nodes = MakeKeyNodes(vnet)
        print u'关键节点个数:', len(key_nodes)
        # 打印所有节点的出边排列组合
        PrintAllVertexOutSeq(vnet.graph(), key_nodes)
        # 是否使用遗传算法
        useGA = True
        if useGA:
            # (3)优化计算得到一个"最优"的节点排列序
            order = QH_GA(vnet, key_nodes, config=GA_CONFIG_FILE)
        else:
            # (1)随机给的一个节点排列序
            # order = [1 for v in key_nodes]
            # (2)通过文件读取优化后的节点排列序
            order = np.loadtxt(QH_ORDER_FILE, dtype='int32').tolist()
        print order
        # 节点排列序写入到文件
        WriteOrderToFile(order, QH_ORDER_FILE)
        # 绘制到dxf文件
        DrawQHToDxf(vnet, key_nodes, order, dxfFile=QH_DXF_FILE)
        # 删除虚拟源汇
        vnet.delVirtualST()
        # 绘制到dot文件
        write_vnet_network_to_dotfile(vnet.graph(), SAMPLE_DOT_FILE)

if __name__=="__main__":
    # 使用简单网络
    test_qh1()
    # 复杂网络数据
    # test_qh2()