#-*- coding:utf-8 -*-
import numpy as np

from vno import *
from sst import *
from vno_dot import *
from vno_path import *
from vno_data import *
from vng_opt import *

def test_vng():
    # 1. 准备通风网络数据: 通过读取文件构造这个词典
    datas = [
        'xiaokang'    # 小康
        # 'daxing',      # 大兴
        # 'linxi',     # 林西
        # 'lvjiatuo',  # 吕家坨
        # 'tangan'       # 唐安(和吕家坨的数据是一样的!)
    ]
    for x in datas:
        graph_datas = read_graph_datas('data\\%s.json' % x)
        # print graph_datas
        # 2. 设置通风网络其它参数
        graph_datas['fans'] = [] # 去掉风机
        graph_datas['qFixs'] = [] # 去掉固定风量

        # 3. 构造通风网络对象
        vnet = VentNetwork()
        if not build_network(graph_datas, vnet.graph()):
            print u'构造通风网络失败!!!'
        else:
            # print vnet.graph()
            print u'\n构造通风网络成功!!!'
            print u'拓扑数据:', 'data\\%s.json' % x, '分支个数:', len(vnet.graph().es), '节点个数:', len(vnet.graph().vs)
            # 12. 通风网络生成dot文件
            write_vnet_network_to_dotfile(vnet.graph(), 'out\\%s.dot' % x)
            # 高亮第k最大阻力路线
            # write_path_to_dotfile(vnet.graph(), dot_file, P)
        # 销毁内存
        del vnet

def test_vng_opt():
    # graph_datas = read_graph_datas('data\\graph_data1.json')
    graph_datas = read_graph_datas('data\\graph_data2.json')
    # graph_datas = read_graph_datas('data\\xiaokang.json')
    # graph_datas = read_graph_datas('data\\daxing.json')
    # print graph_datas
    # 2. 设置通风网络其它参数
    graph_datas['fans'] = [] # 去掉风机
    graph_datas['qFixs'] = [] # 去掉固定风量

        # 3. 构造通风网络对象
    vnet = VentNetwork()
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
    else:
        print u'\n构造通风网络成功!!!'
        print u'拓扑数据:', 'data\\graph_data1.json', '分支个数:', len(vnet.graph().es), '节点个数:', len(vnet.graph().vs)
        # 12. 通风网络生成dot文件
        # write_vnet_network_to_dotfile(vnet.graph(), 'out\\graph_data1.dot')
        # 调用网络图算法
        vng_opt(vnet)
    # 销毁内存
    del vnet

if __name__=="__main__":
    test_vng()
    # test_vng_opt()
