# -*- coding:utf-8 -*-
from random import Random
from time import time
import inspyred

def ga_main(prng=None, display=False): 
    if prng is None:
        prng = Random()
        prng.seed(time()) 
    
    problem = inspyred.benchmarks.Ackley(2)
    # problem = inspyred.benchmarks.Binary(inspyred.benchmarks.Schwefel(2), dimension_bits=30)
    ea = inspyred.ec.GA(prng)
    # ea.terminator = inspyred.ec.terminators.evaluation_termination
    ea.terminator = inspyred.ec.terminators.generation_termination
    final_pop = ea.evolve(generator=problem.generator,
                          evaluator=problem.evaluator,
                          pop_size=100,
                          maximize=problem.maximize,
                          bounder=problem.bounder,
                          max_evaluations=30000,
                          max_generations=300, 
                          cross_rate=0.75,
                          mutation_rate=0.015,
                          num_elites=1)
                          
    if display:
        best = max(final_pop)
        print('Best Solution: \n{0}'.format(str(best)))
    return ea
            
def pso_main(prng=None, display=False):
    if prng is None:
        prng = Random()
        prng.seed(time()) 
    
    problem = inspyred.benchmarks.Ackley(2)
    ea = inspyred.swarm.PSO(prng)
    ea.terminator = inspyred.ec.terminators.generation_termination
    ea.topology = inspyred.swarm.topologies.ring_topology
    final_pop = ea.evolve(generator=problem.generator,
                          evaluator=problem.evaluator, 
                          pop_size=100,
                          bounder=problem.bounder,
                          maximize=problem.maximize,
                          max_evaluations=30000, 
                          max_generations=300,
                          neighborhood_size=5)

    if display:
        best = max(final_pop) 
        print('Best Solution: \n{0}'.format(str(best)))
    return ea

if __name__ == '__main__':
    # ga_main(display=True)  # 遗传算法
    pso_main(display=True)   # 粒子群算法