#-- coding:utf-8 --
# import multiprocessing as mp

# 导入dxf读写库
# import sdxf
# 替换sdxf为ezdxf库(作者也是dxfwrite和dxfgrabber的开发者)
# http://ezdxf.readthedocs.io/en/latest/introduction.html#what-ezdxf-is-not
import ezdxf

import numpy as np
import math
import json

from vno import *
from vno_data import *
from vno_dot import *

def PT_2_MM(v):
    return  v*0.35

def make_point1(x, y):
    return np.array([x, y])

# 构造点向量(使用numpy)
# def make_point2(dg, v):
    # return make_point1(dg.vs[v]['x'], dg.vs[v]['y'])

def point_dist(p1, p2):
    v = p1 - p2
    return math.sqrt(v.dot(v))

def normal_vector1(v):
    L = math.sqrt(v.dot(v))
    return v/L

def normal_vector2(spt, ept):
    return normal_vector1(ept - spt)

def vector_angle(v):
    L = math.sqrt(v.dot(v))
    cos_angle = v.dot(np.array([1,0]))/L
    angle = np.arccos(cos_angle)
    if v[1] < 0:
        angle = 2*np.pi - angle
    return angle

def rad2deg(angle):
    return angle/np.pi*180

def deg2rad(angle):
    return angle/180*np.pi

# 根据两点和弧率，计算半径
def ge_radius1(spt, ept, arc_ratio):
    if abs(arc_ratio) < 0.001:
        return 0.0
    else:
        v = ept - spt
        L = math.sqrt(v.dot(v))
        return (arc_ratio**2+0.25)*L/(2*arc_ratio)

def ge_arc_ratio1(spt, ept, r):
    v = ept - spt
    L = math.sqrt(v.dot(v))
    ar = abs(r/L)-math.sqrt(r**2/L**2 - 0.25)
    if r < 0:
        return -1*ar
    else:
        return ar

def ge_arc_ratio2(dg, v, w, r):
    return ge_arc_ratio1(make_point2(dg, v), make_point2(dg, w), r)

def ge_arc_center(spt, ept, r):
    x1, y1 = spt[0], spt[1]
    x2, y2 = ept[0], ept[1]
    k = -1.0*(x1-x2)/(y1-y2)
    xa = (x1+x2)*0.5
    ya = (y1+y2)*0.5
    s = r*r*4 - (x1-x2)**2-(y1-y2)**2
    # 解方程组
    # yc=ya+k*(xc-xa)
    # (yc-ya)^2+(xc-xa)^2=s/4
    #A=k*k+1, B=2*(k*ya-k*xa-xa);
    xc, yc = 0.0, 0.0
    C = (ya-xa)**2+xa**2-s/4.0
    if r > 0:
        xc = xa + math.sqrt(s/(4*(k*k+1)))
    else:
        xc = xa - math.sqrt(s/(4*(k*k+1)))
    yc = ya + k*(xc - xa)
    if abs(xc) < 0.001:xc = 0.0
    if abs(yc) < 0.001:yc = 0.0
    return make_point1(xc, yc)

def ge_x_coord(spt, ept, r, y):
    c = ge_arc_center(spt, ept, r)
    t = math.sqrt(r**2-(y-c[1])**2)
    x = 0.0
    if r > 0:
        x = c[0] - t
    else:
        x = c[0] + t
    return x

def draw_node(msp, v):
    dg = v.graph
    x, y = float(v['x']), float(v['y'])
    width, height = float(v['width']), float(v['height'])
    font_size = float(v['font_size'])
    name = v['name']
    # 绘制节点圆
    msp.add_ellipse(center=(x, y, 0), major_axis=(width*0.5, 0, 0), ratio=1.0*height/width, dxfattribs={'layer': 'vng'})
    # 绘制节点文字
    msp.add_text(name, dxfattribs={'layer': 'vng', 'style': 'vng', 'height': font_size}).set_pos((x, y), align='MIDDLE_CENTER')

def draw_line_edge(msp, e):
    dg = e.graph
    u, v = e.tuple
    x1, y1 = float(dg.vs[u]['x']), float(dg.vs[u]['y'])
    x2, y2 = float(dg.vs[v]['x']), float(dg.vs[v]['y'])
    font_size = float(e['font_size'])
    name = e['name']
    # 绘制直线分支
    ctrl_points = json.loads(e['points'])
    if len(ctrl_points) == 2:
        msp.add_line((x1, y1), (x2, y2), dxfattribs={'layer': 'vng'})
    else:
        # 多条直线
        points = [(x1, y1)]
        # 去掉首尾坐标
        for i in range(1, len(ctrl_points)-1):
            pt = ctrl_points[i]
            points.append((float(pt['x']), float(pt['y'])))
        points.append((x2, y2))
        msp.add_lwpolyline(points, dxfattribs={'layer': 'vng'})
    
    # 绘制分支文字
    data = e['midPoint'].split(' ')
    tpx, tpy = float(data[0]), float(data[1])
    msp.add_text(name, dxfattribs={'layer': 'vng', 'style': 'vng', 'height': font_size}).set_pos((tpx, tpy), align='MIDDLE_CENTER')
    # 绘制箭头
    # points = [(tpx, tpy, 0, 3), (tpx+3, tpy, 0, 0)]
    # msp.add_lwpolyline(points)

def draw_arc_edge(msp, e):
    dg = e.graph
    u, v = e.tuple
    x1, y1 = float(dg.vs[u]['x']), float(dg.vs[u]['y'])
    x2, y2 = float(dg.vs[v]['x']), float(dg.vs[v]['y'])
    # 计算圆弧中心点坐标    
    spt, ept = make_point1(x1, y1), make_point1(x2, y2)
    # 计算弧率
    arc_ratio = 1.0*e['curviness']/point_dist(spt, ept)
    print u'e',e['name']
    print u'arc_ratio:', arc_ratio
    radius = ge_radius1(spt, ept, arc_ratio)
    print u'radius:', radius
    cnt = ge_arc_center(spt, ept, radius)

    # 圆心到圆弧2个端点的单位向量
    v1, v2 = spt-cnt, ept-cnt
    # 向量与x轴的角度
    startAngle, endAngle = vector_angle(v1), vector_angle(v2)
    # 这里有一点小小的改变，将分支的起点和末点换个顺序传递给arc
    if radius > 0:
        startAngle, endAngle = endAngle, startAngle
    # print u'角度:',rad2deg(startAngle),rad2deg(endAngle)
    # 绘制弧线分支
    msp.add_arc((cnt[0], cnt[1]), abs(radius), rad2deg(startAngle), rad2deg(endAngle), dxfattribs={'layer': 'vng'})
    
    # 绘制分支文字
    font_size = float(e['font_size'])
    name = e['name']
    data = e['midPoint'].split(' ')
    tpx, tpy = float(data[0]), float(data[1])
    # print tpx, tpy, data, e['midPoint']
    msp.add_text(name, dxfattribs={'layer': 'vng', 'style': 'vng', 'height': font_size}).set_pos((tpx, tpy), align='MIDDLE_CENTER')

def draw_spline_edge(msp, e):
    dg = e.graph
    u, v = e.tuple
    # 控制点坐标
    ctrl_points = []
    # 始节点坐标
    ctrl_points.append((float(dg.vs[u]['x']), float(dg.vs[u]['y']), 0))
    # 获取曲线分支的中间点坐标
    for pt in json.loads(e['points']):
        ctrl_points.append((float(pt['x']), float(pt['y']), 0))
    # 末节点坐标
    ctrl_points.append((float(dg.vs[v]['x']), float(dg.vs[v]['y']), 0))

    # 绘制曲线分支
    msp.add_open_spline(ctrl_points)
    font_size = float(e['font_size'])

    # 绘制分支文字
    name = e['name']
    data = e['midPoint'].split(' ')
    tpx, tpy = float(data[0]), float(data[1])
    # print tpx, tpy, data, e['midPoint']
    msp.add_text(name, dxfattribs={'layer': 'vng', 'style': 'vng', 'height': font_size}).set_pos((tpx, tpy), align='MIDDLE_CENTER')

def draw_edge(msp, e):
    # print e['id'], e['category'], e['curviness']
    if e['category'] == 'spline':
        draw_spline_edge(msp, e)
    elif e['category'] == 'arc' and abs(float(e['curviness'])) > 0.1:
        draw_arc_edge(msp, e)
    else:
        draw_line_edge(msp, e)

def draw_graph_to_dxf(dg, dxfFile='out\\vng.dxf'):
    dwg = ezdxf.new('R2010')  # create a new DXF R2010 drawing, official DXF version name: 'AC1024'
    msp = dwg.modelspace()  # add new entities to the model space
    # del line.dxf.const_width
    # 增加一个vng图层(所有的通风网络图的元素都放在vng图层下)
    dwg.layers.new(name='vng')
    # 自定义文字样式vng(使用宋体)
    dwg.styles.new('vng', dxfattribs={'font': 'simsun.ttf'})
    for v in dg.vs:
        # fix: 字符串解码
        v['name'] = v['name'].decode('gbk')
        # print v['name']
        draw_node(msp, v)
    for e in dg.es:
        # fix: 字符串解码
        e['name'] = e['name'].decode('gbk')
        # print e['name']
        draw_edge(msp, e)
    dwg.saveas(dxfFile)