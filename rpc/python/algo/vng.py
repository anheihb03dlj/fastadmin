#-- coding:utf-8 --
# import multiprocessing as mp
import os
import tempfile
from vno import *
import pygraphviz as pgv
import numpy as np
# 处理spline算法
import scipy.interpolate as si
# 曲线点抽稀算法(pip install rdp)
# https://github.com/fhirschmann/rdp
# 注: 如果在js中使用, maker.js项目包含了rdp.js
from rdp import rdp

# 计算多边形面积
# 用于判断直线是否共线
def PolyArea(x,y):
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))

# 判断直线是否共线(多边形面积为0)
def IsPointsOnLine(points, p=0.1):
    x, y = [], []
    for obj in points:
        x.append(obj["x"])
        y.append(obj["y"])
    s = PolyArea(x, y)
    return abs(s) < p

# 来源: https://stackoverflow.com/questions/24612626/b-spline-interpolation-with-python
def bspline(cv, n=100, degree=3, periodic=False):
    """ Calculate n samples on a bspline

        cv :      Array ov control vertices
        n  :      Number of samples to return
        degree:   Curve degree
        periodic: True - Curve is closed
                  False - Curve is open
    """
    # If periodic, extend the point array by count+degree+1
    cv = np.asarray(cv)
    count = len(cv)
    if periodic:
        factor, fraction = divmod(count+degree+1, count)
        cv = np.concatenate((cv,) * factor + (cv[:fraction],))
        count = len(cv)
        degree = np.clip(degree,1,degree)
    # If opened, prevent degree from exceeding count-1
    else:
        degree = np.clip(degree,1,count-1)
    # Calculate knot vector
    kv = None
    if periodic:
        kv = np.arange(0-degree,count+degree+degree-1,dtype='int')
    else:
        kv = np.array([0]*degree + range(count-degree+1) + [count-degree]*degree,dtype='int')
    # Calculate query range
    u = np.linspace(periodic,(count-degree),n)
    # Calculate result
    arange = np.arange(len(u))
    points = np.zeros((len(u),cv.shape[1]))
    for i in xrange(cv.shape[1]):
        points[arange,i] = si.splev(u, (kv,cv[:,i],degree))
    return points
# 样条曲线点缩减为4个
def cubic_spline(ctrl_points1):
    points1 = []
    for obj in ctrl_points1:
        points1.append((obj["x"], obj["y"]))
    points2 = bspline(points1, n=4)
    ctrl_points2 = []
    for obj in points2:
        ctrl_points2.append({"x":obj[0], "y":obj[1]})
    return ctrl_points2

def reduce_polyline(ctrl_points1):
    points1 = []
    for obj in ctrl_points1:
        points1.append([obj["x"], obj["y"]])
    # 抽稀算法
    points2 = rdp(points1, epsilon=0.5)
    ctrl_points2 = []
    for obj in points2:
        ctrl_points2.append({"x":obj[0], "y":obj[1]})
    return ctrl_points2

# 英寸 --> 毫米.
# 单位换算:{ 1in = 2.54cm = 25.4mm = 72pt }
def INCH_2_MM(v):
    return  v*25.4

# 毫米 --> 英寸.
# 单位换算:{ 1in = 2.54cm = 25.4mm = 72pt }
def MM_2_INCH(v):
    return v*0.0393700787401575

def MM_2_PT(v):
    return v*2.8346456692913385826771653543307

# 辅助宏(简化写法)
def BOOL_2_STRING(ret):
    if ret:
        return "true"
    else:
        return "false"

def STRING_2_BOOL(ret):
    if ret == 'true':
        return True
    else:
        return False

def is_equal(a, b, p=0.001):
    # print a,b,a-b
    return abs(a-b) < p

def UnifyParam(kwargs):
    # print kwargs
    kwargs['nodesep'] = MM_2_INCH(kwargs.get('nodesep', 50.8))
    kwargs['ranksep'] = MM_2_INCH(kwargs.get('ranksep', 38.1))
    kwargs['rankdir'] = kwargs.get('rankdir', 'BT')
    kwargs['splines'] = kwargs.get('splines', 'spline')    
    kwargs['node_width'] = MM_2_INCH(kwargs.get('node_width', 40.0))
    kwargs['node_height'] = MM_2_INCH(kwargs.get('node_height', 30.0))
    kwargs['fontsize'] = MM_2_PT(kwargs.get('fontsize', 11))

    # kwargs['graphRatio'] = kwargs.get('graphRatio', 3) # 参数废除,不再使用！
    # kwargs['useDefWH'] = kwargs.get('useDefWH', True) # 参数废除,不再使用！    
    # kwargs['graphWidth'] = MM_2_INCH(kwargs.get('graphWidth', 600)) # 参数废除,不再使用！
    # kwargs['graphHeight'] = MM_2_INCH(kwargs.get('graphHeight', 800)) # 参数废除,不再使用！

    kwargs['dot_exe'] = kwargs.get('dot_exe', 'dot.exe')
    kwargs['arrow_width'] = MM_2_INCH(kwargs.get('arrow_width', 4.0))
    kwargs['arrow_length'] = MM_2_INCH(kwargs.get('arrow_length', 9.0))
    # kwargs['use_sepoint'] = STRING_2_BOOL(kwargs.get('use_sepoint', True))

# 利用pygraphviz创建有向图
def CreateDotFile2(vnet, dotFile, param):
    # print param
    g=pgv.AGraph(directed=True,strict=True)
    # 设置图、节点、分支的全局属性
    g.graph_attr['ranksep'] = param['ranksep']
    g.graph_attr['nodesep'] = param['nodesep']
    g.graph_attr['rankdir'] = param['rankdir']
    g.graph_attr['splines'] = param['splines']
    g.graph_attr['fontname'] = "SimSun"

    g.node_attr['fontname'] = "SimSun"
    g.node_attr['height'] = param['node_height']
    g.node_attr['width'] = param['node_width']
    g.node_attr['fixedsize'] = "true"
    g.node_attr['fontsize'] = param['fontsize']

    g.edge_attr['fontname'] = "SimSun"
    g.edge_attr['fontsize'] = param['fontsize']
    g.edge_attr['arrowhead'] = "none"
    g.edge_attr['arrowtail'] = "none"

    dg = vnet.graph()
    for v in dg.vs:
        # 节点的属性
        node_attr = {}
        attribs = v.attributes()
        # print attribs
        if 'width' in attribs and not is_equal(MM_2_INCH(v['width']), param['node_width']):
            node_attr["width"] = MM_2_INCH(v['width'])
        if 'height' in attribs and not is_equal(MM_2_INCH(v['height']), param['node_height']):
            node_attr["height"] = MM_2_INCH(v['height'])
        if 'fontsize' in attribs and not is_equal(MM_2_PT(v['fontsize']), param['fontsize']):
            node_attr["fontsize"] = MM_2_PT(v['fontsize'])
        # 增加节点
        g.add_node('v%s' % v.index, **node_attr)
    
    # 写入拓扑关系及分支的具体属性
    for e in dg.es:
        u = 'v%s' % (e.source)
        v = 'v%s' % (e.target)
        # 忽略零风阻分支
        # if filter_zero_edge and is_zero_edge(e):continue
        if g.has_edge(u, v):
            print u'分支e%s=(%s, %s)已存在!' % (e['id'], u, v)
            continue
        
        # 分支的属性
        edge_attr = {}
        # 分支编号
        edge_attr['label'] = 'e%d' % (e.index)
        # r, q = float(e['r']), float(e['q'])
        # # 风阻R
        # edge_attr['label'] = '%s\\lR=%.3f' % (edge_attr['label'], r)
        # # 风量Q
        # edge_attr['label'] = '%s\\lQ=%.2f' % (edge_attr['label'], q)
        # # 阻力H
        # edge_attr['label'] = '%s\\lH=%.2f' % (edge_attr['label'], r*abs(q)*q)
        
        # 分支文字的字体大小
        attribs = e.attributes()
        if 'fontsize' in attribs and not is_equal(MM_2_PT(e['fontsize']), param['fontsize']):
            edge_attr["fontsize"] = MM_2_PT(v['fontsize'])
        
        # 特殊分支标记
        # 固定风量分支: 绿色,粗线
        # if is_fix_edge(e):
        #     edge_attr['color'] ="green"
        #     edge_attr['penwidth'] = '3'
        # # 风机分支: 蓝色,粗线
        # elif is_fan_edge(e):
        #     edge_attr['color'] ="blue"
        #     edge_attr['penwidth'] = '3'
        # # style =\"dashed\", penwidth=5"        
        g.add_edge(u, v, **edge_attr)        
    # print g.string()
    print u'dot文件分支个数:', g.number_of_edges(), g.number_of_nodes()
    # 写入dot文件
    g.write(dotFile)

def CreateDotFile(vnet, dotFile, param):
    f = open(dotFile, 'w')
    f.write('strict digraph G {\n')
    # 设置图的属性
    f.write('\tgraph[fontname=SimSun, rankdir=%s, splines=%s, ranksep=%f, nodesep=%f];\n' % (param['rankdir'], param['splines'], param['ranksep'], param['nodesep']))
    # 设置节点属性
    f.write('\tnode[fontname=SimSun, height=%f, width=%f, fontsize=%f];\n' % (param['node_height'], param['node_width'], param['fontsize']))
    # 设置分支属性
    f.write('\tedge[fontname=SimSun, arrowhead=%s, arrowtail=%s, fontsize=%f];\n' % ('none', 'none', param['fontsize']))  
    dg = vnet.graph()
    for v in dg.vs:
        width, height, fontsize = param['node_width'], param['node_height'], param['fontsize']
        attribs = v.attributes()
        if 'width' in attribs:
            width = MM_2_INCH(v['width'])
        if 'height' in attribs:
            height = MM_2_INCH(v['height'])
        if 'fontsize' in attribs:
            fontsize = MM_2_PT(v['fontsize'])
        f.write('\t%s[width=%f, height=%f, fontsize=%f];\n' % ('v%d' % v.index, width, height, fontsize))
    for e in dg.es:
        u = 'v%d' % (e.source)
        v = 'v%d' % (e.target)
        fontsize = param['fontsize']
        attribs = e.attributes()
        if 'fontsize' in attribs:
            fontsize = MM_2_PT(e['fontsize'])
        f.write('\t%s -> %s [label=%s, fontsize=%f];\n' % (u, v, 'e%d' % e.index, fontsize))
    f.write('}')
    f.close()

'''
调用外部程序dot.exe
执行命令：dot -Tplain -o gd.dat gd.gv
           或 dot -Tsvg -o gd.dat gd.gv
示例：生成一个png图形==> dot -Tpng -o xx.png gd.gv
参见Graphviz的文档
'''    
def GenDrawFormatFile(dot_exe, fmt, dotFile, outFile):
    cmd = '%s -T%s -o %s %s' % (dot_exe, fmt, outFile, dotFile)
    # 利用os.system执行外部命令(该函数会阻塞线程!)
    os.system(cmd)

def ReadGraphGeo(values):
    if len(values) < 4:
        return {}    
    else:
        return {
        'ratio' : float(values[1]),
        'width' : INCH_2_MM(float(values[2])),
        'height' : INCH_2_MM(float(values[3]))
    }

def ReadNodeGeo(values):
    if len(values) < 7:
        return {}
    else:
        return {
            values[6][1:] : {
                'x' : INCH_2_MM(float(values[2])),
                'y' : INCH_2_MM(float(values[3])),
                'cx' : INCH_2_MM(float(values[4])),
                'cy' : INCH_2_MM(float(values[5]))
            }
        }

def ReadEdgeGeo(values, spline=True):
    if len(values) < 4:
        return {}
    n = int(values[3])
    if len(values) < 7+2*n:
        return {}
    else:
        # 控制点坐标
        ctrl_points = []
        for i in range(n):
            pos = 4+2*i
            x = INCH_2_MM(float(values[pos]))
            y = INCH_2_MM(float(values[pos+1]))
            ctrl_points.append({'x':x, 'y':y})
        # 分支编号坐标
        pos = 4+2*n
        text_point = {
            'x' : INCH_2_MM(float(values[pos+1])),
            'y' : INCH_2_MM(float(values[pos+2])),
        }
        # print u'分支:', values[pos][1:]
        # print u'处理前:', ctrl_points
        # 判断是否直线
        if IsPointsOnLine(ctrl_points):
            # print u'点在一条直线上!'
            ctrl_points = []
        else:
            # 曲线点抽稀
            ctrl_points = reduce_polyline(ctrl_points)
            if spline:
                # 拟合B-样条曲线(4个点)
                ctrl_points = cubic_spline(ctrl_points)
        # print u'处理后:', ctrl_points
        # 返回结果
        return {
            values[pos][1:] : {
                's'  : values[1][1:],
                't'  : values[2][1:], 
                'ctrl_points' : ctrl_points,
                'text_point' : text_point
            }
        }

def ParserPlainFile(outFile, spline=True):
    graph = {}
    nodes = {}
    edges = {}
    f = open(outFile, 'r')
    for line in f:
        line = line.strip()
        if line == 'stop':
            break
        values = line.split()
        if len(values) == 0:
            continue
        tag = values[0]
        if tag == 'graph':
            graph = ReadGraphGeo(values)
        elif tag == 'node':
            ret = ReadNodeGeo(values)
            if len(ret) == 0:
                continue
            else:
                nodes.update(ret)
        elif tag == 'edge':
            ret = ReadEdgeGeo(values, spline)
            if len(ret) == 0:
                continue
            else:
                edges.update(ret)
    f.close()
    # 返回解析结果
    return {
        'graph' : graph,
        'nodes' : nodes,
        'edges' : edges
    }

# 更新id
def UpdateGraphIds(vnet, result):
    dg = vnet.graph()
    NE = len(result['edges'])
    NV = len(result['nodes'])
    # 解析出错,与原图的节点个数或分支个数不相等!
    if len(dg.es) != NE or len(dg.vs) != NV:
        return False
    # 更新节点id
    for i in result['nodes']:
        # 增加一个key-pair对,记录节点图元的句柄
        result['nodes'][i]['id'] = dg.vs[int(i)]['id']
    # 更新分支id
    for i in result['edges']:
        # 增加一个key-pair对,记录分支图元的句柄
        result['edges'][i]['id'] = dg.es[int(i)]['id']
        u, v = dg.es[int(i)].tuple
        # fix: 修正分支的始末节点编号未更新的问题(从igraph内部编号转换成换实际的id)
        result['edges'][i]['s'] = dg.vs[u]['id']
        result['edges'][i]['t'] = dg.vs[v]['id']
    return True

def MakeTempFiles(debug=False):
    if debug:
        return 'gd.dot', 'gd.txt'
    else:
        dotFile = os.path.join ( tempfile.mkdtemp() + 'gd.dot' )
        outFile = os.path.join ( tempfile.mkdtemp() + 'gd.txt' )
        return dotFile, outFile

def vng(vnet, **kwargs):
    # 生成临时文件
    # 如果想查看dotFile和outFile的内容,设置debug参数为True,这2个文件会在当前目录下生成!
    dotFile, outFile = MakeTempFiles(debug=False)    
    # 单位换算(mm-->inch)
    UnifyParam(kwargs)
    # 生成dot文件
    CreateDotFile2(vnet, dotFile, kwargs)
    if not os.path.exists(dotFile):
        return {}
    # 生成plain格式文件
    GenDrawFormatFile(kwargs['dot_exe'], 'plain', dotFile, outFile)
    if not os.path.exists(outFile):
        return {}
    # 解析plain文件
    result = ParserPlainFile(outFile, kwargs['splines']=='spline')
    # 更新id
    if not UpdateGraphIds(vnet, result):
        return {}
    # 返回解析结果
    return result