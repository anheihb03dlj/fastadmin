#-- coding:utf-8 --
# import multiprocessing as mp

# 导入dxf读写库
# import sdxf
# 替换sdxf为ezdxf库(作者也是dxfwrite和dxfgrabber的开发者)
# http://ezdxf.readthedocs.io/en/latest/introduction.html#what-ezdxf-is-not
import ezdxf

import numpy as np
import math

from vno import *
from vno_data import *
from vno_dot import *

def PT_2_MM(v):
    return  v*0.35

def draw_node(msp, v):
    dg = v.graph
    x, y = float(v['x']), float(v['y'])
    width, height = float(v['width']), float(v['height'])
    font_size = float(v['font_size'])
    name = v['name']
    # 绘制节点圆
    msp.add_ellipse(center=(x, y, 0), major_axis=(width*0.5, 0, 0), ratio=1.0*height/width, dxfattribs={'layer': 'vng'})
    # 绘制节点文字
    msp.add_text(name, dxfattribs={'layer': 'vng', 'style': 'vng', 'height': font_size}).set_pos((x, y), align='MIDDLE_CENTER')

def draw_line_edge(msp, e):
    dg = e.graph
    u, v = e.tuple
    x1, y1 = float(dg.vs[u]['x']), float(dg.vs[u]['y'])
    x2, y2 = float(dg.vs[v]['x']), float(dg.vs[v]['y'])
    font_size = float(e['font_size'])
    name = e['name']
    # 绘制直线分支
    msp.add_line((x1, y1), (x2, y2), dxfattribs={'layer': 'vng'})
    # 绘制分支文字
    tpx, tpy = (x1+x2)*0.5, (y1+y2)*0.5
    msp.add_text(name, dxfattribs={'layer': 'vng', 'style': 'vng', 'height': font_size}).set_pos((tpx, tpy), align='MIDDLE_CENTER')

def draw_spline_edge(msp, e):
    dg = e.graph
    u, v = e.tuple
    x1, y1 = float(dg.vs[u]['x']), float(dg.vs[u]['y'])
    x2, y2 = float(dg.vs[v]['x']), float(dg.vs[v]['y'])
    # 计算圆弧中心点坐标
    spt, ept, r = make_point1(x1, y1), make_point1(x2, y2), e['radius']
    cnt = ge_arc_center(spt, ept, r)

    # 圆心到圆弧2个端点的单位向量
    v1, v2 = spt-cnt, ept-cnt
    # 向量与x轴的角度
    startAngle, endAngle = vector_angle(v1), vector_angle(v2)
    # 这里有一点小小的改变，将分支的起点和末点换个顺序传递给arc
    if r > 0:
        startAngle, endAngle = endAngle, startAngle
    # print u'角度:',rad2deg(startAngle),rad2deg(endAngle)
    # 绘制弧线分支
    msp.append(sdxf.Arc(center=(cnt[0], cnt[1], 0), radius=abs(r), startAngle=rad2deg(startAngle), endAngle=rad2deg(endAngle)))
    # 绘制分支文字
    text_v = (spt+ept)*0.5 - cnt
    text_v = normal_vector1(text_v)
    text_pt = cnt + text_v*abs(r)
    height = PT_2_MM(e['height'])
    msp.append(sdxf.Text('e%s' % (e['id']), point=(text_pt[0], text_pt[1], 0), height=height))

def draw_edge(msp, e):
    if e['category'] == 'line':
        draw_line_edge(msp, e)
    else:
        draw_spline_edge(msp, e)

def draw_graph_to_dxf(dg, dxfFile='out\\vng.dxf'):
    dwg = ezdxf.new('R2010')  # create a new DXF R2010 drawing, official DXF version name: 'AC1024'
    msp = dwg.modelspace()  # add new entities to the model space    
    # 增加一个vng图层(所有的通风网络图的元素都放在vng图层下)
    dwg.layers.new(name='vng')
    # 自定义文字样式vng(使用宋体)
    dwg.styles.new('vng', dxfattribs={'font': 'simsun.ttf'})
    for v in dg.vs:
        draw_node(msp, v)
    for e in dg.es:
        draw_edge(msp, e)
    dwg.saveas(dxfFile)