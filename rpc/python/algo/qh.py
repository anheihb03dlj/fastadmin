#-*- coding:utf-8 -*-
#平衡图优化(Q-H图)

# import numpy as np
from qh_dfs import *
import qh_dxf
from vno import *

# 分支块
class Block:
    def __init__(self, x=0, y=0, w=0, h=0, k=0):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.k = k
    def __str__(self):
        return 'x=%.2f y=%.2f w=%.2f h=%.2f k=%d' % (self.x, self.y, self.w, self.h, self.k)
    __repr__ = __str__

# 分支块集合(每条分支可能有多个块)
class Blocks:
    def __init__(self, e):
        self.e = e
        # 默认包含一个分支块
        self.blocks = [Block()]
    def addBlock(self):
        self.blocks.append(Block())
    def getBlock(self):
        return self.blocks[-1]
    def getAll(self):
        return self.blocks
    def printAll(self):
        print u'e%d:' % (self.e['id']),
        for i in range(len(self.blocks)):
            print self.blocks[i],
        print 
    def getEdge(self):
        return self.e
    def setX(self, x):
        self.blocks[-1].x = x
    def setY(self, y):
        self.blocks[-1].y = y
    def setWidth(self, w):
        self.blocks[-1].w = w
    def setHeight(self, h):
        self.blocks[-1].h = h
    def setKPath(self, k):
        self.blocks[-1].k = k
    def getX(self):
        return self.getBlock().x
    def getY(self):
        return self.getBlock().y
    def getWidth(self):
        return self.getBlock().w
    def getHeight(self):
        return self.getBlock().h
    def getKPath(self):
        return self.getBlock().k

def BlockToJson(e, block, color=7):
    return {
        'id' : e['id'],
        'x' : block.x,
        'y' : block.y,
        'w' : block.w,
        'h' : block.h,
        'Q' : e['q'],
        'H' : f0(e),
        'color' : color
    }

# 平衡图类
class QH:
    def __init__(self, vnet, key_nodes):
        self.vnet = vnet
        self.key_nodes = key_nodes
        dg = self.vnet.graph()
        # 增加相关属性
        # 分支增加visited属性(标记分支是否已被访问或着色)
        dg.es['visited'] = False # SeqDfs函数使用
        # 节点增加pred属性(记录先驱节点)
        dg.vs['pred'] = -1       # SeqDfs函数使用
        # 分支增加块流量属性(从q那儿复制过来)
        dg.es['blockQ'] = 0      # 本函数使用
        # 节点增加seq属性(记录出边顺序)
        dg.vs['seq'] = None      # SeqDfs函数使用
        # 记录分支所在最后一条通路的编号
        dg.es['kPath'] = 0
        # 分支增加矩形块属性(记录矩形的参数:左下角坐标,宽度,高度,通路编号)
        dg.es['blocks'] = None # 可能有多个矩形块
        for e in dg.es:
            # 过滤虚拟分支
            if is_zero_edge(e):
                continue
            # 初始化blockQ属性
            e['blockQ'] = e['q']
            # 初始化blocks属性
            if e['blocks'] is None:
                e['blocks'] = Blocks(e)
    def __del__(self):
        dg = self.vnet.graph()
        # 删除相关属性
        del dg.es['visited']
        del dg.vs['pred']
        del dg.es['blockQ']
        del dg.es['blocks']
        del dg.vs['seq']
        del dg.es['kPath']
    # 绘图黄金比例
    def drawGoldenRatio(self):
        return 0.618*self.vnet.totalFlow()/self.vnet.totalH()
    # 调整节点出边顺序
    def adjustOutSeq(self, order):
        dg = self.vnet.graph()
        AdjustVertexOutSeq(dg, self.key_nodes, order)
    def printOutSeq(self):
        dg = self.vnet.graph()
        PrintOutSeq(dg)
    def printBlocks(self):
        dg = self.vnet.graph()
        for e in dg.es:
            if is_zero_edge(e):
                continue
            blocks = e['blocks']
            print u'e%d:' % (e['id']),
            blocks.printAll()
    # 计算交叉数以及分支块的几何坐标
    def buildBlocks(self, order, qPrecise=0.1):
        dg = self.vnet.graph()
        # 将宽和高比例限定为0.618(黄金比例)
        c = self.drawGoldenRatio()
        # c = 1
        print u'缩放系数(绘图用):', c
        # 交叉分支
        crossBlocks = 0
        # 当前是第k条通路?
        kPath = 0
        # 起始坐标
        x, y = 0, 0
        # 虚拟源汇点
        s, t = self.vnet.vST()
        # 搜索基于最小流量的独立通路
        while SeqDfs(dg, s, t, k=1):
            path = edge_path_from_pred(dg, s, t, True)
            # 打印路径
            print u'第%d条通路:' % (kPath+1),
            PrintPath(dg, path)
            # 提取通路流量(过滤掉虚拟的分支)
            pathQ = [e['blockQ'] for e in dg.es.select(path) if not is_zero_edge(e)]
            # 该通路只包含虚拟分支,没有意义
            if len(pathQ) == 0:
                continue
            # 第k条独立通路
            kPath = kPath + 1
            # 计算通路宽度(找最小流量的分支)
            delta_q = min(pathQ)
            print u'第%d条通路流量宽度Δq=%.2f' % (kPath, delta_q)
            # 计算每个矩形块的参数
            for i in path:
                e = dg.es[i]
                # 过滤虚拟分支
                if is_zero_edge(e):
                    continue
                # 分支x增量
                delta_x = 0
                # 第一次绘制分支块
                if e['kPath'] == 0:
                    delta_x = x
                # 新增分支块
                elif e['kPath']+1 < kPath:
                    e['blocks'].addBlock()
                    delta_x = x
                    crossBlocks = crossBlocks + 1
                # print u'---------------------------------'
                # e['blocks'].printAll()
                e['kPath'] = kPath
                e['blocks'].setX(e['blocks'].getX()+delta_x)
                e['blocks'].setY(y)
                e['blocks'].setWidth(e['blocks'].getWidth()+delta_q)
                e['blocks'].setHeight(c*f0(e))
                e['blocks'].setKPath(kPath)
                # e['blocks'].printAll()
                # print u'---------------------------------'
                y = y + e['blocks'].getHeight()
            # 更新下一条通路的起始坐标
            x, y = x + delta_q, 0
            # 计算通路中分支流量
            for i in path:
                e = dg.es[i]
                # 过滤虚拟分支
                if is_zero_edge(e):
                    continue
                # 虚拟分支或流量为0的分支
                if abs(e['blockQ']) < qPrecise:
                    e['visited'] = True
                    continue
                # 减去流量宽度(通路的最小流量)
                e['blockQ'] = e['blockQ'] - delta_q
                # 如果流量等于0,则分支标记为已访问
                if abs(e['blockQ']) < qPrecise:
                    e['visited'] = True
        # 返回交叉块的个数
        print u'交叉块数:', crossBlocks
        return crossBlocks
    def toJson(self):
        dg = self.vnet.graph()
        blocks = []
        # 依次绘制每条分支
        for e in dg.es:
            # 过滤虚拟分支
            if is_zero_edge(e):
                continue
            # 绘制所有的分支块
            edge_blocks = e['blocks'].getAll()
            color = 7 # 白色
            if len(edge_blocks) > 1:
                color = 3 # 绿色
            for block in edge_blocks:
                blocks.append(BlockToJson(e, block, color=color))
        return blocks

# 辅助方法--绘制平衡图并写入dxf文件
def DrawQHToDxf(vnet, key_nodes, order, dxfFile='qh.dxf'):
    obj = QH(vnet, key_nodes)
    # 调整节点出边顺序
    obj.adjustOutSeq(order)
    # 计算分支交叉
    crossBlocks = obj.buildBlocks(order)
    # 生成json数据
    blocks = obj.toJson()
    # 打印节点出边排列
    # obj.printOutSeq()
    del obj
    # 写入到dxf文件
    qh_dxf.Draw(blocks, dxfFile)

# 绘制平衡图并写入dxf文件
def DrawQH(vnet, key_nodes, order):
    obj = QH(vnet, key_nodes)
    # 内部绘图用的比例因子(0.68*Q/H)
    ratio = obj.drawGoldenRatio()
    # 调整节点出边顺序
    obj.adjustOutSeq(order)
    # 计算分支交叉
    crossBlocks = obj.buildBlocks(order)
    # 生成json数据
    blocks = obj.toJson()
    # 打印节点出边排列
    # obj.printOutSeq()
    del obj
    if len(blocks) == 0:
        return {}
    else:
        # 返回计算结果
        return { 
        'blocks' : blocks,
        'graph'  : {
            'width'  : vnet.totalFlow(),
            'height' : vnet.totalH(),
            'ratio'  : ratio
        }
    }