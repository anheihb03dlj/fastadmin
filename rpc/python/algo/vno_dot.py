#-*- coding:utf-8 -*-

import pygraphviz as pgv
import numpy as np
from vno import *

# 标准化dot文件名称
def standardize_dot_file_name(dot_file):
    # 拆分dot文件路径
    path = os.path.splitext(dot_file)
    # 得到dot文件路径和扩展名
    fname, ext = path[0], path[1][1:]
    if ext != 'dot':
        dot_file = '%s.dot' % (fname)
    # 合成png文件路径
    png_file = '%s.png' % (fname)
    # print dot_file, png_file
    return dot_file, png_file

# 将通风网络写入到dot文件中
def write_vnet_network_to_dotfile(dg, dot_file, filter_zero_edge=True):
    # 利用pygraphviz创建有向图
    g=pgv.AGraph(directed=True,strict=True)
    # 设置图、节点、分支的全局属性
    g.graph_attr['rankdir']='BT'
    g.graph_attr['fontname']="SimSun"
    g.graph_attr['splines'] = "spline"
    g.graph_attr['nodesep'] = 2.0
    g.graph_attr['ranksep'] = 1.5

    g.node_attr['fontname']="SimSun"
    g.node_attr['width']=1.574803
    g.node_attr['height']=1.181102
    g.node_attr['fontsize']=34.015748
    g.node_attr['fixedsize']="true"

    g.edge_attr['fontname']="SimSun"
    g.edge_attr['arrowhead']=None
    g.edge_attr['arrowtail']=None
    g.edge_attr['fontsize']=34.015748
    # 写入拓扑关系及分支的具体属性
    for v in dg.vs:
        node_attr = {"width":1.574803, "height":1.181102, "fontsize":34.015748}
        g.add_node('v%s' % v['id'], **node_attr)

    for e in dg.es:
        u = 'v%s' % (dg.vs[e.source]['id'])
        v = 'v%s' % (dg.vs[e.target]['id'])
        # 忽略零风阻分支
        if filter_zero_edge and is_zero_edge(e):continue

        # 分支的属性
        edge_attr = {}
        # 分支编号
        edge_attr['label'] = 'e%s' % (e['id'])
        # 风阻R
        # edge_attr['label'] = '%s\\lR=%.3f' % (edge_attr['label'], e['r'])
        # 风量Q
        # edge_attr['label'] = '%s\\lQ=%.2f' % (edge_attr['label'], e['q'])
        # 阻力H
        # edge_attr['label'] = '%s\\lH=%.2f' % (edge_attr['label'], f0(e))
        
        # 分支文字的字体大小
        edge_attr['fontsize'] = 34.015748
        # 特殊分支标记
        # 固定风量分支: 绿色,粗线
        if is_fix_edge(e):
            edge_attr['color'] ="green"
            edge_attr['penwidth'] = '3'
        # 风机分支: 蓝色,粗线
        elif is_fan_edge(e):
            edge_attr['color'] ="blue"
            edge_attr['penwidth'] = '3'
        # style =\"dashed\", penwidth=5"
        if g.has_edge(u, v):
            print u'分支e%s=(%s, %s)已存在!' % (e['id'], u, v)
        g.add_edge(u, v, **edge_attr)
    # print g.string()
    print u'dot文件分支个数:', g.number_of_edges(), g.number_of_nodes()
    # 写入dot文件
    g.write(dot_file)

def write_edges_to_dotfile(dg, dot_file, edges, style='dashed', color='black', penwidth=3):
    # 利用pygraphviz创建有向图
    g=pgv.AGraph(dot_file)
    f1 = lambda i:dg.vs[dg.es[i].source]['id']
    f2 = lambda i:dg.vs[dg.es[i].target]['id']
    gv_edges = [('v%s' % f1(i), 'v%s' % f2(i)) for i in edges]
    # print gv_edges
    for u, v in gv_edges:
        if g.has_edge(u, v):
            edge = g.get_edge(u, v)
            # print edge
            # 最大阻力路线分支: 虚线加粗
            edge.attr['style'] = style
            edge.attr['color'] = color
            edge.attr['penwidth'] = str(penwidth)
    # for edge in g.edges():
    #   print edge.attr
    # 写入dot文件
    g.write(dot_file)

# 修改画dot文件,并高亮最大阻力路线
def write_path_to_dotfile(dg, dot_file, P):
    write_edges_to_dotfile(dg, dot_file, P, style='dashed', penwidth=3)

# 修改画dot文件,并高亮最大阻力路线
def write_tree_to_dotfile(dg, dot_file, tree):
    write_edges_to_dotfile(dg, dot_file, tree, style='dashed', color='blue', penwidth=3)

# 利用pygraphviz解析并绘制dot文件
def draw_dot_file(dot_file, png_file):
    g=pgv.AGraph(dot_file)
    # 使用dot.exe进行布局
    g.layout('dot') # layout with dot
    # 绘制有向图
    g.draw(png_file) # write to file
    # g.draw('file.ps',prog='circo') # use circo to position,write PS file