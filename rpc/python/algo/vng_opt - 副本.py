#-- coding:utf-8 --
from vno import *
from vno_dot import *

import pygraphviz as pgv
# python标准库里的队列
from collections import deque
# python标准库里的优先队列
from Queue import PriorityQueue
# 导入dxf读写库
import sdxf
import numpy as np
import math

#表示无穷大
DBL_MAX = 1.0e6
# 判断无穷大
def is_inf(x):
    return abs(abs(x)-DBL_MAX)<0.01

def get_node_id(dg, v):
    return dg.vs[v]['id']

def get_edge_id(dg, i):
    return dg.vs[i]['id']

#通过pred属性得到路径
def node_path_from_pred(dg, s, t, pred_name='pred'):
    #记录路径(用节点表示)
    pred = dg.vs[pred_name]
    path = [t]
    u = t
    while pred[u] >= 0 and u != pred[u]:
        u = pred[u]
        path.append(u)
    if len(path) > 1:
        if path[-1] != s:
            path = []
        else:
            path.reverse()
    else:
        path = []
    return path
# 节点路径转换成分支路径
def node_path_to_edge_path(dg, node_path, directed=True):
    if len(node_path) < 2:
        return []
    # print node_path
    edge_path = []
    for u,v in zip(node_path[:-1], node_path[1:]):
        i = dg.get_eid(u, v, directed=directed, error=False)
        if i < 0:
            continue
        else:
            edge_path.append(i)
    return edge_path
def edge_path_from_pred(dg, s, t, directed=True, pred_name='pred'):
    # 通过先驱节点返回节点路径
    path = node_path_from_pred(dg, s, t, pred_name=pred_name)
    # 节点路径转换成分支路径
    return node_path_to_edge_path(dg, path, directed)

def rank_lpm(dg, s):
    # 标记节是否已访问过
    dg.vs['lpm_visited'] = False
    dg.vs['lpm_pred'] = -1
    # 构造一个空的队列
    dq = deque()
    # 出发节点进队,并标记为已访问
    dq.append(s)
    dg.vs[s]['lpm_visited'] = True

    # 判断队列是否为空
    while len(dq) != 0:
        # 考查队首节点
        v = dq[0]
        # print v, '出队'
        # 考查节点v的出边分支
        for i in dg.incident(v, mode=OUT):
            # w是一个整数,不是节点对象
            w = dg.es[i].target
            # 如果节点w已经被访问
            if dg.vs[w]['lpm_visited']:
                if dg.vs[w]['rank'] < dg.vs[v]['rank'] + 1:
                    dg.vs[w]['lpm_pred'] = v
                    dg.vs[w]['rank'] = dg.vs[v]['rank'] + 1
                    # 更新所有以w为前节点的最长路径长度
                    for uu in dg.vs.select(lpm_pred=w):
                        j = uu.index
                        dg.vs[j]['rank'] = dg.vs[w]['rank'] + 1
            else:
                dq.append(w)
                dg.vs[w]['lpm_visited'] = True
                dg.vs[w]['lpm_pred'] = v
                dg.vs[w]['rank'] = dg.vs[v]['rank'] + 1
        # 节点v出队
        dq.popleft()
        # 恢复颜色
        dg.vs[v]['lpm_visited'] = False

    # 删除属性数据
    del dg.vs['lpm_visited']
    del dg.vs['lpm_pred']

def balance_rank(dg):
    m, n = len(dg.vs), len(dg.es)
    print m,n

    # 复制有向图并删除间距大于1的分支
    rank_dg = dg.copy()
    span_func = lambda dg, e:dg.vs[e.target]['rank']-dg.vs[e.source]['rank']
    rank_dg.delete_edges([e.tuple for e in dg.es if span_func(rank_dg, e)>1])
    # node_id, edge_id = m*10, n*10
    # 间距大于1的分支插入节点和分支
    for e in dg.es:
        s, t = e.tuple
        span = dg.vs[t]['rank'] - dg.vs[s]['rank']
        u, v = s, t
        print u'节点span(v%s, v%s)=%d' % (dg.vs[s]['id'], dg.vs[t]['id'], span)
        k = 0
        while span > 1:
            k = k + 1
            node_id = '%s_%s_%d' % (dg.vs[s]['id'], dg.vs[t]['id'], k)
            rank_dg.add_vertex(id=node_id, rank=dg.vs[s]['rank']+k)
            
            v = rank_dg.vs.indices[-1]
            edge_id = '%s_%d' % (e['id'], k)
            rank_dg.add_edge(u, v, id=edge_id)
            i = rank_dg.es.indices[-1]
            print u'新增分支e%s=(v%s, v%s)' % (rank_dg.es[i]['id'], rank_dg.vs[u]['id'], rank_dg.vs[v]['id'])
            u = v
            span = span - 1
        if v != t:
            k = k + 1
            edge_id = '%s_%d' % (e['id'], k)
            rank_dg.add_edge(v, t, id=edge_id)
            i = rank_dg.es.indices[-1]
            print u'新增分支e%s=(v%s, v%s)' % (rank_dg.es[i]['id'], rank_dg.vs[v]['id'], rank_dg.vs[t]['id'])

    # 图的其它属性使用默认(保证某些函数能正确执行,属性数据无实质用处!)
    default_graph_property(rank_dg)
    return rank_dg

def debug_network(dg, filename='debug_network'):
    # 打印节点层次
    # for v in dg.vs:
        # print u'L(v%s)=%d' % (v['id'], v['rank'])
    print u'分支个数:', len(dg.es), '节点个数:', len(dg.vs)
    # 通风网络生成dot文件
    # 注: filter_zero_edge=False表示不过滤虚拟分支
    write_vnet_network_to_dotfile(dg, 'out\\%s.dot' % filename, filter_zero_edge=False)

# 节点分层
def rank(dg, s):
    # 采用最长路径法计算节点分层
    rank_lpm(dg, s)
    # debug_network(dg)
    # 平衡节点层次
    rank_dg = balance_rank(dg)
    debug_network(rank_dg)
    return rank_dg

# 使用bfs得到一个初始的节点排序
def init_order(dg, s):
    dq = deque()
    dq.append(s)
    dg.vs[s]['order'] = 0

    # 使用词典记录每一层的当前order
    order = {}
    while len(dq) > 0:
        u = dq.pop()
        for i in dg.incident(u, mode=OUT):
            v = dg.es[i].target
            layer = dg.vs[v]['rank']
            if not order.has_key(layer):
                order[layer] = 0
            # 节点v尚未访问
            if dg.vs[v]['order'] < 0:
                # print u'v%s' % dg.vs[v]['id'], layer, order[layer], order
                dg.vs[v]['order'] = order[layer]
                order[layer] = order[layer] + 1
            dq.append(v)

def adj_position(dg, v, forward):
    mode = OUT if forward else IN
    f = lambda i:dg.vs[dg.es[i].target]['order'] if forward else dg.vs[dg.es[i].source]['order']
    P = [f(i) for i in dg.incident(v, mode=mode)]
    if len(P) > 1:P.sort()
    return P

# 计算节点v的相邻层(adj_rank)的中位值
def median_value(dg, v, forward):
    P = adj_position(dg, v, forward)
    # print u'P=', P
    if len(P) == 0:
        return -1.0
    m = len(P)/2
    if len(P)%2 == 1:
        return P[m]*1.0
    elif len(P) == 2:
        return (P[0]+P[1])/2.0
    else:
        left = P[m-1] - P[0]
        right = P[len(P)-1] - P[m]
        return 1.0*(P[m-1]*right+P[m]*left)/(left+right)

class OrderNode:
    def __init__(self, dg, v, e=-1):
        self.dg = dg
        self.v = v
        self.e = e
    def getNode(self):
        return self.v
    def getEdge(self):
        return self.e
    def setEdge(self, e):
        self.e = e
    def getId(self):
        return self.dg.vs[self.v]['id']
    def getRank(self):
        return self.dg.vs[self.v]['rank']
    def getOrder(self):
        return self.dg.vs[self.v]['order']
    def setOrder(self, value):
        self.dg.vs[self.v]['order'] = value
    def exchange(self, other):
        # 交换节点
        self.v, other.v = other.v, self.v
        # 交换顺序
        o1, o2 = self.getOrder(), other.getOrder()
        self.setOrder(o2)
        other.setOrder(o1)
    def getMedian(self):
        return self.dg.vs[self.v]['median']
    def setMedian(self, value):
        self.dg.vs[self.v]['median'] = value

def make_order(dg, r):
    seq = [v.index for v in dg.vs if v['rank'] == r]
    order = [OrderNode(dg, -1, -1) for i in seq]
    for v in seq:
        k = dg.vs[v]['order']
        order[k].v = v
    return order

# o1、o2是一个2元tuple,第1个节点,第2个位置
# 切记: 使用cmp而不是<, >, ==运算符
def order_cmp(o1, o2):
    return cmp(o1[1], o2[1])

def adj_order(dg, v, forward):
    mode = OUT if forward else IN
    f1 = lambda i:dg.es[i].target if forward else dg.es[i].source
    f2 = lambda i:dg.vs[dg.es[i].target]['order'] if forward else dg.vs[dg.es[i].source]['order']
    seq = [(f1(i), f2(i), i) for i in dg.incident(v, mode=mode)]
    seq.sort(cmp=order_cmp)
    return [OrderNode(dg, x[0], x[2]) for x in seq]
    
def print_order(dg, max_rank):
    for r in range(max_rank+1):
        order = make_order(dg, r)
        print u'L(%d)=' % r, ' '.join([obj.getId() for obj in order])

# o1、o2是一个2元tuple,第1个节点,第2个中位值
# 切记: 使用cmp而不是<, >, ==运算符
def median_cmp(o1, o2):
    return cmp(o1[1], o2[1])

def sort_order(order):
    # 复制一份新的节点排序
    nodes = [(obj.v, obj.getMedian()) for obj in order if obj.getMedian() >-1.0]
    nodes.sort(cmp=median_cmp)
    # 将排序的数据按顺序填充到order(-1表示保持位置不变)
    k = 0
    for i in range(len(order)):
        order[i].setOrder(i)
        if order[i].getMedian() <= -1.0:continue
        order[i].v = nodes[k][0]
        k += 1

def __wmedian(dg, max_rank, forward):
    ranks = range(1, max_rank+1) if forward else range(max_rank-1, 0, -1)
    for r in ranks:
        # 找出第i层的节点排序
        order = make_order(dg, r)
        # print u'第%d层' % r
        # print u'    当前节点排序:', ' '.join([obj.getId() for obj in order])
        # 按照顺序遍历该层上的节点
        for obj in order:
            # 计算该层节点与上一层的median值
            obj.setMedian(median_value(dg, obj.getNode(), not forward))
        # 按中位值排序
        sort_order(order)
        # print u'    中位值排序结果:', [(obj.getId(), obj.getOrder(), obj.getMedian()) for obj in order]

def wmedian(dg, max_rank, iter):
    # print u'-----第%d次迭代-----' % iter
    # 前向遍历(自下而上)
    if iter % 2 != 0:
        __wmedian(dg, max_rank, True)
    # 反向遍历(自上而下)
    else:
        __wmedian(dg, max_rank, False)

class EdgeNode:
    def __init__(self, dg, e):
        self.dg = dg
        self.e = e
    def getEdge(self):
        return self.e
    def getId(self):
        return self.e['id']
    def getTargetOrder(self):
        return self.dg.vs[self.e.target]['order']
    def getSourceOrder(self):
        return self.dg.vs[self.e.source]['order']
    def getOrder(self):
        return self.getSourceOrder(), self.getTargetOrder()

def edge_cmp(o1, o2):
    i_k, j_k = o1.getOrder()
    i_l, j_l = o2.getOrder()
    if i_k != i_l:
        return cmp(i_k, i_l)
    else:
        return cmp(j_k, j_l)

# 计算逆序对
def inv_pairs(dg, order, forward=True):
    edges = []
    # 搜索前一层还是下一层
    mode = OUT if forward else IN
    for obj in order:
        edges.extend([EdgeNode(dg, dg.es[i]) for i in dg.incident(obj.getNode(), mode=mode)])        
    if len(edges) == 0:
        return []
    else:
        # 对分支进行排序
        # print [(obj.getId(), obj.getTargetOrder()) for obj in edges]
        edges.sort(cmp=edge_cmp)
        # print [(obj.getId(), obj.getTargetOrder()) for obj in edges]
        # 取出末节点,构成逆序对
        return [obj.getTargetOrder() for obj in edges]

# 计算south_seq中的逆序对个数
# 其中q表示S层的节点个数
def accumulator_tree (south_seq):
    # print u'P=', south_seq
    if len(south_seq) < 2:
        return 0
    # 计算末节点个数
    q = len(set(south_seq))
    # build the accumulator tree
    first_index = 1
    while(first_index < q):
        first_index *= 2
    # number of tree nodes
    tree_size = 2*first_index - 1
    first_index -= 1
    tree = [0]*tree_size
    # 计算交叉数
    cross_count = 0
    # 分支个数
    r = len(south_seq)
    for k in range(r):
        index = south_seq[k] + first_index
        tree[index] += 1
        while index > 0:
            if index % 2 != 0:
                cross_count += tree[index+1]
            index = (index - 1)/2
            tree[index] += 1
    return cross_count

# 计算两层之间(order层<-->相邻层)的分支交叉数
def bipartite_crossing(dg, order, forward=True):
    south_seq = inv_pairs(dg, order, forward)
    return accumulator_tree(south_seq)

def transpose(dg, max_rank):
    improved = True
    while improved:
        improved = False
        for r in range(max_rank+1):
            # 第r层的节点排序
            # n表示该层的节点个数
            order = make_order(dg, r)
            n = len(order)
            for i in range(n-1):
                # 计算交换前的交叉数
                c1 = bipartite_crossing(dg, order, False) + bipartite_crossing(dg, order, True)
                # 交换i和i+1节点的顺序
                order[i].exchange(order[i+1])
                # 计算交换后的交叉数(前后2层的交叉)
                c2 = bipartite_crossing(dg, order, False) + bipartite_crossing(dg, order, True)
                # 分支交叉数有所减少
                if c1 > c2:
                    improved = True
                else:
                    # 交换i和i+1节点的顺序
                    order[i].exchange(order[i+1])

# 计算整个图的分支交叉数
def crossing(dg, max_rank):
    c = 0
    for r in range(max_rank):
        order = make_order(dg, r)
        c += bipartite_crossing(dg, order, True)
        # print u'第%d层:' % r, ' '.join([obj.getId() for obj in order]), '交叉:%d' % c
    return c

def copy_to_best(dg):
    for v in dg.vs:
        v['best'] = v['order']

def copy_from_best(dg):
    for v in dg.vs:
        v['order'] = v['best']

# 节点排序
def ordering(dg, s, max_iterations=24):
    # 节点最高层
    max_rank = max([v['rank'] for v in dg.vs])
    print u'最大层', max_rank
    # 初始化一个节点排序
    init_order(dg, s)
    # 打印节点排序
    print_order(dg, max_rank)
    # 初始节点排序的分支交叉数
    best = crossing(dg, max_rank)
    print u'初始节点排序交叉数:', best
    # 复制order属性到best属性
    copy_to_best(dg)
    # 迭代
    for iter in range(max_iterations):
        print u'第%d次迭代' % iter,
        # 中位启发式算子
        wmedian(dg, max_rank, iter)
        # 相邻互换启发式算子
        transpose(dg, max_rank)
        # 计算分支交叉数
        c = crossing(dg, max_rank)
        print u'交叉数:', c
        if c < best:
            best = c
            # 记录当前最好的节点排序
            # copy_to_best(dg)
    print u'最优交叉数:', best
    # 打印节点排序
    print_order(dg, max_rank)
    return best

class CoordNode(OrderNode):
    def __init__(self, dg, v, node_type=0):
        OrderNode.__init__(self, dg, v)
        self.node_type = node_type
    def outDeg(self):
        return self.dg.vs[self.v]['out_deg']
    def inDeg(self):
        return self.dg.vs[self.v]['in_deg']
    def __lt__(self, other):
        r1, r2 = self.getRank(), other.getRank()
        if self.node_type == 1:
            if self.outDeg() == 0:
                r1 += 1e5
            return r1 < r2
        elif self.node_type == 2:
            if self.inDeg() == 0:
                r2 -= 1e5
            return r1 > r2

# def build_nodes(dg, s_or_t=True):
#     nodes = []
#     if s_or_t:
#         nodes.extend([CoordNode(dg, u.index, 1) for u in dg.vs.select(_outdegree_ge=2)])
#     else:
#         nodes.extend([CoordNode(dg, u.index, 2) for u in dg.vs.select(_indegree_ge=2)])
#     return nodes

# def build_special_nodes(dg):
#     for v in dg.vs:
#         if v['coord_node'] is None:
#             v['coord_node'] = CoordNode(dg, v.index, 0)

# def init_coord_nodes(dg, nodes):
#     # 初始化节点属性
#     for obj in nodes:
#         v = obj.getNode()
#         dg.vs[v]['coord_node'] = obj

def make_priority_queue(nodes):
    pq = PriorityQueue()
    for obj in nodes:
        pq.put(obj)
    return pq

def make_coord_node(dg, v, node_type=1):
    obj = CoordNode(dg, v, node_type)
    # dg.vs[v]['coord_node'] = obj
    return obj

def build_st_priority_queue(dg, s, t):
    sNodes = [make_coord_node(dg, s, 1)]
    tNodes = [make_coord_node(dg, t, 2)]
    return make_priority_queue(sNodes), make_priority_queue(tNodes)

def pq_put(pq, dg, v, node_type):
    if not is_node_active(dg, v):
        active_node(dg, v)
        pq.put(make_coord_node(dg, v, node_type))

def pq_get(pq, dg, v):
    kill_active_node(dg, v)
    pq.get()

# 逐层正向搜索或反向搜索
def order_dfs(dg, s, t, forward=True):
    # 源汇节点的层
    rs, rt = dg.vs[s]['rank'], dg.vs[t]['rank']
    print u'搜索:%s->%s' % (dg.vs[s]['id'], dg.vs[t]['id']), '源点层次:', rs, '汇点层次:', rt
    if rs >= rt:return []

    dg.vs['dfs_pred'] = -1
    dg.vs['dfs_visited'] = False

    stack = [s]
    dg.vs[s]['dfs_pred'] = -1
    dg.vs[s]['dfs_visited'] = True

    # 当前层次
    u, r = s, rs
    while len(stack) > 0:
        # 弹出栈顶节点
        u = stack.pop()
        # print u'-->', dg.vs[u]['id']
        # 已搜索到目标节点
        if u == t:
            break
        # 超过搜索层次限制
        elif r > rt:
            continue
        elif dg.vs[u]['drawed']:
            continue
        
        # 逐层搜索
        r = r + 1
        # 搜索节点u在下一邻接层的节点排序
        order = adj_order(dg, u, True)
        start, end, step = 0, len(order), 1
        # forward表示正向搜索,反之则为反向搜索
        if forward:
            start, end, step = len(order)-1, -1, -1
        # print u'order:',  ' '.join([order[i].getId() for i in range(start, end, step)])
        for i in range(start, end, step):
            v = order[i].getNode()
            # 如果节点已被访问过了,就不再压入到栈中
            if dg.vs[v]['dfs_visited']:
                continue
            # 分支是否绘制
            j = order[i].getEdge()
            if j == -1 or dg.es[j]['drawed']:
                continue
            dg.vs[v]['dfs_pred'] = u
            dg.vs[v]['dfs_visited'] = True
            stack.append(v)
    
    # print stack, r
    path = []
    # 搜索到目标终点,且不超出层次范围
    if u == t:
        path.extend(edge_path_from_pred(dg, s, t, pred_name='dfs_pred'))

    del dg.vs['dfs_visited']
    del dg.vs['dfs_pred']

    return path

def set_pred_from_path(dg, path):
    for i in range(len(path)-1, -1, -1):
        e = dg.es[path[i]]
        u, v = e.tuple
        dg.vs[v]['pred'] = u

def make_point1(x, y):
    return np.array([x, y])

# 构造点向量(使用numpy)
def make_point2(dg, v):
    return make_point1(dg.vs[v]['x'], dg.vs[v]['y'])

def normal_vector1(v):
    L = math.sqrt(v.dot(v))
    return v/L

def normal_vector2(spt, ept):
    return normal_vector1(ept - spt)

def vector_angle(v):
    L = math.sqrt(v.dot(v))
    cos_angle = v.dot(np.array([1,0]))/L
    angle = np.arccos(cos_angle)
    if v[1] < 0:
        angle = 2*np.pi - angle
    return angle

def rad2deg(angle):
    return angle/np.pi*180

def deg2rad(angle):
    return angle/180*np.pi

# 根据两点和弧率，计算半径
def ge_radius1(spt, ept, arc_ratio):
    v = ept - spt
    L = math.sqrt(v.dot(v))
    return (arc_ratio**2+0.25)*L/(2*arc_ratio)

def ge_radius2(dg, v, w, arc_ratio):
    return ge_radius1(make_point2(dg, v), make_point2(dg, w), arc_ratio)

def ge_arc_ratio(spt, ept, r):
    v = ept - spt
    L = math.sqrt(v.dot(v))
    ar = abs(r/L)-math.sqrt(r**2/L**2 - 0.25)
    if r < 0:
        return -1*ar
    else:
        return ar

def ge_arc_ratio2(dg, v, w, r):
    return ge_arc_ratio(make_point2(dg, v), make_point2(dg, w), r)

def ge_arc_center(spt, ept, r):
    x1, y1 = spt[0], spt[1]
    x2, y2 = ept[0], ept[1]
    k = -1.0*(x1-x2)/(y1-y2)
    xa = (x1+x2)*0.5
    ya = (y1+y2)*0.5
    s = r*r*4 - (x1-x2)**2-(y1-y2)**2
    # 解方程组
    # yc=ya+k*(xc-xa)
    # (yc-ya)^2+(xc-xa)^2=s/4
    #A=k*k+1, B=2*(k*ya-k*xa-xa);
    xc, yc = 0.0, 0.0
    C = (ya-xa)**2+xa**2-s/4.0
    if r > 0:
        xc = xa + math.sqrt(s/(4*(k*k+1)))
    else:
        xc = xa - math.sqrt(s/(4*(k*k+1)))
    yc = ya + k*(xc - xa)
    if abs(xc) < 0.001:xc = 0.0
    if abs(yc) < 0.001:yc = 0.0
    return make_point1(xc, yc)

def ge_x_coord(spt, ept, r, y):
    c = ge_arc_center(spt, ept, r)
    t = math.sqrt(r**2-(y-c[1])**2)
    x = 0.0
    if r > 0:
        x = c[0] - t
    else:
        x = c[0] + t
    return x

def is_node_drawed(dg, v):
    x, y = dg.vs[v]['x'], dg.vs[v]['y']
    return not is_inf(x) and not is_inf(y)

def set_node_xy1(dg, v, pt):
    dg.vs[v]['x'] = pt[0]
    dg.vs[v]['y'] = pt[1]

def set_node_xy2(dg, v, x, y):
    dg.vs[v]['x'] = x
    dg.vs[v]['y'] = y

def find_drawed_node(dg, path):    
    w = -1
    # 逆序搜索(排除路径的起点和终点)
    for i in range(len(path)-1, 0, -1):
        e = dg.es[path[i]]
        u, v = e.tuple
        # print make_point2(dg, u), make_point2(dg, v)
        if is_node_drawed(dg, u):
            w = u
            break
    return w

# 在路径中查找实际出度大于1的节点(正向搜索分支的始节点)
def find_path1(dg, path):
    k = -1
    for i in range(0, len(path)):
        e = dg.es[path[i]]
        u, v = e.tuple
        if dg.vs[u]['out_deg'] > 1:
            k = i
            break
    return k

# 在路径中查找实际入度大于1的节点(反向搜索分支的末节点)
def find_path2(dg, path):
    k = -1
    for i in range(len(path)-1, -1, -1):
        e = dg.es[path[i]]
        u, v = e.tuple
        if dg.vs[v]['in_deg'] > 1:
            k = i
            break
    return k

def get_from(dg, path, k):
    if k == -1:
        return -1
    e = dg.es[path[k]]
    return e.source

def get_to(dg, path, k):
    if k == -1:
        return -1
    e = dg.es[path[k]]
    return e.target

def find_from_node(dg, path):
    k = find_path1(dg, path)
    return k, get_from(dg, path, k)

def find_to_node(dg, path):
    k = find_path2(dg, path)
    return k, get_to(dg, path, k)

# 分支着色. 出度/入度减1
def color_line_path(dg, path):
    for i in range(len(path)):
        e = dg.es[path[i]]
        u, v = e.tuple
        e['drawed'] = True
        e['radius'] = 0.0
        # 出度/入度减1
        if dg.vs[u]['out_deg'] > 0:dg.vs[u]['out_deg'] -= 1
        print u'v%s 出度 减1' % dg.vs[u]['id']
        if dg.vs[v]['in_deg'] > 0:dg.vs[v]['in_deg'] -= 1
        print u'v%s 入度 减1' % dg.vs[v]['id']

def cacl_line_path(dg, path, spt, v, d):
    pt1, pt2 = spt, spt
    for i in range(len(path)):
        # 分支始末节点
        uu, vv = dg.es[path[i]].tuple
        # 利用向量计算分支的末节点坐标
        pt2 = pt2 + v*d
        set_node_xy1(dg, uu, pt1)
        set_node_xy1(dg, vv, pt2)
        pt1 = pt2

def two_point_distance(spt, ept):
    v = ept - spt
    return math.sqrt(v.dot(v))

def two_point_vector1(spt, ept, n):
    # 两点构成一个向量
    v = ept - spt
    # 向量的长度(模)
    L = math.sqrt(v.dot(v))
    # 单位化向量
    v = v / L
    # 直线n等分
    d = L / n
    return v, d

def two_point_vector2(dg, s, t, n):
    spt, ept = make_point2(dg, s), make_point2(dg, t)
    v, d = two_point_vector1(spt, ept, n)
    return spt, v, d

def path_vector(dg, path):
    # 路径的源点和汇点
    s, t = dg.es[path[0]].source, dg.es[path[-1]].target
    return two_point_vector2(dg, s, t, len(path))

# 这里其实并不存在所谓的"画直线"的过程
# 对路径进行分析，从2个方向（from->, <-to）搜索path，并删除path中出度为1的节点
# 确定新的from'和to'，并压入到sources和targets中        
def draw_line_path(dg, path):
    # 查找新的源点
    ks, sn = find_from_node(dg, path)
    # 查找新的汇点
    kt, tn = find_to_node(dg, path)

    if ks > kt:return False

    # 计算起点坐标spt、方向向量v、以及增量d
    spt, v, d = path_vector(dg, path)
    # 路径长度
    n = len(path)
    print u'draw_line_path:', ks, kt
    # 该路径上的所有节点度均为1(纯粹的直线路径)
    # 此时不存在子并联通路
    if ks == -1 and kt == -1:
        # 整个路径
        cacl_line_path(dg, path, spt, v, d)
        color_line_path(dg, path)
    elif ks == -1 and kt != -1:
        # 右半部分
        cacl_line_path(dg, path[kt+1:n], spt+v*(kt+1)*d, v, d)
        color_line_path(dg, path[kt+1:n])
        # dg['cur_to'] = tn
        # dg.vs[tn]['pred'] = -1
    elif ks != -1 and kt == -1:
        # 左半部分
        cacl_line_path(dg, path[0:ks], spt, v, d)
        color_line_path(dg, path[0:ks])
        # dg['cur_from'] = sn
        # dg.vs[sn]['pred'] = -1
    else:
        # 左半部分
        cacl_line_path(dg, path[0:ks], spt, v, d)
        color_line_path(dg, path[0:ks])
        # 右半部分
        cacl_line_path(dg, path[kt+1:n], spt+v*(kt+1)*d, v, d)
        color_line_path(dg, path[kt+1:n])
        dg['cur_from'] = sn
        dg['cur_to'] = tn
        '''
        在删除节点的同时，也应该将节点的predecessor置空
        这里采用一种偷懒的做法，将newTo的predecessor置空即可
        这样可以认为newFrom和newTo之间不存在路径
        '''
        dg.vs[tn]['pred'] = -1

    return True

def draw_arc_path(dg, path, r):
    # 路径的源点和汇点
    s, t = dg.es[path[0]].source, dg.es[path[-1]].target
    spt, ept = make_point2(dg, s), make_point2(dg, t)
    # 路径长度
    n = len(path)
    # n等分
    dy = (ept[1] - spt[1]) / n
    # 计算弧线路径上每一点的坐标
    for i in range(n):
        # 分支始末节点
        e = dg.es[path[i]]
        u, v = e.tuple
        e['radius'] = r
        if v == t:continue
        y = spt[1] + (i+1)*dy
        x = ge_x_coord(spt, ept, r, y)
        set_node_xy2(dg, v, x, y)
    # 弧线分支着色,出度/入度减1
    for i in range(n):
        e = dg.es[path[i]]
        u, v = e.tuple
        dg.es[path[i]]['drawed'] = True
        if dg.vs[u]['out_deg'] > 0:dg.vs[u]['out_deg'] -= 1
        if dg.vs[v]['in_deg'] > 0:dg.vs[v]['in_deg'] -= 1

def is_node_drawed1(dg, v):
    return dg.vs[v]['in_deg'] == 0

def is_node_drawed2(dg, v):
    return dg.vs[v]['out_deg'] == 0

def analyse_node_in_path(dg, v):
    # print u's=v%s, t=v%s, v=v%s' % (get_node_id(dg, s), get_node_id(dg, t), get_node_id(dg, v))
    flag1 = is_node_drawed1(dg, v) # 入边都绘制完成?
    flag2 = is_node_drawed2(dg, v) # 出边都绘制完成?
    # print flag1, flag2
    if flag1 and flag2:
        # 从源汇数组中删除节点.
        # 由于我们使用的是优先队列,
        #     默认将出度为0的节点放在源点队列末尾,
        #     默认将入度为0的节点放在汇点队列末尾
        # 无需额外的删除操作(另外删除操作也较为费时!!)
        return -1, -1
    elif flag1:
        return v, -1
    elif flag2:
        return -1, v
    else:
        return -1, -1
    
'''
1、经过分析，我发现所谓的节点分析过程实质上可以等价于程序的编写者将他的画图思想融入到算法过程中
2、我的画图思想：在发现一条路径之后，每次都尝试在路径中最远的2个节点之间寻找路径，并绘制路径
3、找到一条路径之后，将节点按照路径的反向顺序加入到sources和targets中，
    这就等价于“尝试在路径中最远的2个节点之间寻找路径”思想
'''
def analyse_path(dg, path):
    s, t = dg.es[path[0]].source, dg.es[path[-1]].target
    # 新增的源汇节点
    sNodes, tNodes = [], []
    # 反向搜索
    for i in range(len(path)-1, -1, -1):
        # 分支始末节点
        e = dg.es[path[i]]
        u, v = e.tuple
        # 排除搜索起始节点
        if u == s:continue

        # print u'start analyse v%s --> v%s' % (get_node_id(dg, u), get_node_id(dg, v))
        # 分析节点u
        sn, tn  = analyse_node_in_path(dg, u)
        # print sn, tn
        # print u'end analyse v%s' % (get_node_id(dg, u))
        if sn != -1:
            sNodes.append(sn)
        if tn != -1:
            tNodes.append(tn)
    return sNodes, tNodes

# 节点v和w是否存在以r为半径绘制的路径(简化版dfs)
def is_path_drawed(dg, v, w, r):    
    span = dg.vs[w]['rank'] - dg.vs[v]['rank']
    # path = []
    u = w    
    while u != v:
        span = span - 1
        if span < 0:
            break
        # 查找节点w的入边分支
        for i in dg.incident(u, mode=IN):
            e = dg.es[i]
            if abs(e['radius'] - r) < 0.01:
                u = e.source
                # path.append(e)
                break
    return u == v

def reset_arc_artio(dg, v, w):
    dg['arc_ratio'] = dg['orig_arc_ratio']
    dg['arc_radius'] = ge_radius2(dg, v, w, dg['arc_ratio'])

def update_arc_ratio1(dg, v, w, r):
    dg['arc_radius'] = -1*r
    dg['arc_ratio'] = ge_arc_ratio2(dg, v, w, dg['arc_radius'])

def update_arc_ratio2(dg, v, w, count):
    if count == 0:
        return
    elif count % 2 == 0:
        dg['arc_ratio'] *= -1*0.7
        dg['arc_radius'] = ge_radius2(dg, v, w, dg['arc_ratio'])
    else:
        dg['arc_ratio'] *= -1
        dg['arc_radius'] *= -1

def set_path_arc_radius1(dg, v, w):
    is_draw_line = False  # 绘制直线或弧线(此时假设是弧线)
    # 弧线并联通路的个数
    count = dg['arc_path_count']
    update_arc_ratio2(dg, v, w, count)
    if count != 0:
        dg['dfs_direction'] = not dg['dfs_direction']
    if dg.vs[v]['out_deg'] == 1 or dg.vs[w]['in_deg'] == 1:
        if count > 0 and count % 2 == 0:
            is_draw_line = True
    return is_draw_line

def get_pred_edge(dg, v, w):
    u = dg.vs[w]['pred']
    if u < 0:return -1
    k = dg.get_eid(u, v, directed=True, error=False)
    return k

def set_path_arc_radius2(dg, v, w):
    dg['cur_source'] = v
    dg['cur_target'] = w
    dg['arc_path_count'] = 0
    dg['dfs_direction'] = True
    is_draw_line = False  # 绘制直线或弧线(此时假设是弧线)
    
    k = get_pred_edge(dg, v, w)
    if k < 0 or not is_path_drawed(dg, v, w):
        print u'v%s->v%s not exist drawed path!' % (get_node_id(dg, v), get_node_id(dg, w))
        print dg['cur_from'], dg['cur_to']
        # 表示源点from和汇点to是第一次开始绘制（这个时候应该特殊对待）
        if v == dg['cur_from'] and w == dg['cur_to']:
            # 这种情况是总进风和总回风存在直线的情况
            if dg.vs[v]['out_deg'] == 1 and dg.vs[w]['in_deg'] == 1:
                is_draw_line = True
        else:
            is_draw_line = True
        # 重置圆弧半径参数
        reset_arc_artio(dg, v, w)
    elif k >= 0:        
        # 更新计算圆弧半径参数
        update_arc_ratio1(dg, v, w, dg.es[k]['radius'])
    return is_draw_line

def update_param(dg, v, w):
    print u'更新参数:'
    print u'v=%s, w=%s' % (get_node_id(dg, v),get_node_id(dg, w))
    if dg['cur_source'] != -1:
        print u'cur_source=%s' % (get_node_id(dg, dg['cur_source']))
    else:
        print u'cur_source=-1'
    if dg['cur_target'] != -1:
        print u'cur_target=%s' % (get_node_id(dg, dg['cur_target']))
    else:
        print u'cur_target=-1'

    if v == dg['cur_source'] and w == dg['cur_target']:
        print u'call set_path_arc_radius1()'
        return set_path_arc_radius1(dg, v, w)
    else:
        print u'call set_path_arc_radius2()'
        return set_path_arc_radius2(dg, v, w)

# 初始化所有节点的xy坐标
def init_node_xy1(dg, s, t, node_sep=12.7, ranK_sep=25.4):
    # 默认设置节点坐标和分支半径属性
    for v in dg.vs:
        v['x'], v['y'] = DBL_MAX, DBL_MAX
    for e in dg.es:
        e['radius'] = DBL_MAX
    # 节点最高层
    max_rank = max([v['rank'] for v in dg.vs])
    # y坐标原点
    y = 0
    for r in range(max_rank+1):
        order = make_order(dg, r)
        # 每一层x坐标起始位置
        x = 0
        # 每一层节点个数
        n = len(order)
        if n%2 == 0:
            x = -(n/2-0.5)*node_sep
        else:
            x = -(n/2)*node_sep
        # print r, x, n
        for obj in order:
            set_node_xy2(dg, obj.getNode(), x, y)
            x += node_sep
        y += ranK_sep

# 只初始化源点和汇点的xy坐标
def init_node_xy2(dg, s, t, node_sep=12.7, ranK_sep=25.4):
    # 默认设置节点坐标和分支半径属性
    for v in dg.vs:
        v['x'], v['y'] = DBL_MAX, DBL_MAX
    for e in dg.es:
        e['radius'] = DBL_MAX
    # 节点最高层
    max_rank = max([v['rank'] for v in dg.vs])
    # 只初始化源点和汇点的坐标
    set_node_xy2(dg, s, 0.0, 0.0)
    set_node_xy2(dg, t, 0.0, max_rank*ranK_sep)

def print_node_xy(dg):
    # 节点最高层
    max_rank = max([v['rank'] for v in dg.vs])
    for r in range(max_rank+1):
        print u'第%d层:' % r
        order = make_order(dg, r)
        for obj in order:
            print u'  x(%s)=%.1f, y(%s)=%.1f' % (obj.getId(), obj.getX(), obj.getId(), obj.getY())

# 记录节点的出度和入度
def init_node_degree(dg):
    for v in dg.vs:
        v['in_deg'] = v.indegree()
        v['out_deg'] = v.outdegree()

def is_node_active(dg, v):
    return dg.vs[v]['is_alive']

def active_node(dg, v):
    dg.vs[v]['is_alive'] = True

def kill_active_node(dg,v):
    dg.vs[v]['is_alive'] = False

def analyse_st_node(dg, v, w):
    s, t = -1, -1
    if v == w:
        print u'remove v%s --> v%s which is equal' % (get_node_id(dg, v), get_node_id(dg, w))
        s, t = v, w
    # 移除度为0的节点
    if dg.vs[v]['out_deg'] == 0:
        print u'remove v%s which out_deg=0' % (get_node_id(dg, v))
        s = v
    if dg.vs[w]['in_deg'] == 0:
        print u'remove v%s which in_deg=0' % (get_node_id(dg, w))
        t = w
    return s, t

def ready_go(dg, v, w, sNodes, tNodes):
    # 特殊情况判断,如果有需要则更新源汇队列
    ss, tt = analyse_st_node(dg, v, w)
    ret = True
    if ss != -1:
        pq_get(sNodes, dg, ss)
        ret = False
    if tt != -1:
        pq_get(tNodes, dg, tt)
        ret = False
    return ret

def optimize_node_xy(dg, s, t):
    # 临时增加节点属性
    dg.vs['in_deg'] = 0
    dg.vs['out_deg'] = 0
    dg.vs['drawed'] = False
    dg.es['drawed'] = False
    dg.vs['pred'] = -1
    dg.vs['is_alive'] = False

    # 设置全局图形参数
    dg['orig_arc_ratio'] = 0.35        # 圆弧半径比率
    dg['arc_ratio'] = 0.35             # 圆弧半径比率
    dg['arc_ratio_factor'] = 0.7       # 圆弧半径比率缩放因子
    dg['arc_radius'] = ge_radius2(dg, s, t, dg['arc_ratio'])
    dg['arc_path_count'] = 0
    dg['dfs_direction'] = True         # dfs搜索方向(True-正向, False-反向)
    dg['cur_source'] = -1
    dg['cur_target'] = -1
    dg['cur_from'] = s
    dg['cur_to'] = t

    # 记录节点的出度和入度
    init_node_degree(dg)

    # 测试用
    # print order_dfs(dg, s, t, True)

    # 构造源点和汇点优先队列
    # 使用队列的理由: 保证取出的源汇点距离最远!!!
    sNodes, tNodes = build_st_priority_queue(dg, s, t)

    count = 0
    while not sNodes.empty() and not tNodes.empty() and count < 200:
        count +=1        
        # 取出源汇节点
        v, w = sNodes.queue[0].getNode(), tNodes.queue[0].getNode()
        print u'count=',count, 'sNodes len:', len(sNodes.queue), 'tNodes len:', len(tNodes.queue), 'v%s --> v%s' % (get_node_id(dg, v), get_node_id(dg, w))
        print dg.vs[v]['out_deg'], dg.vs[v]['in_deg']
        print dg.vs[w]['out_deg'], dg.vs[w]['in_deg']

        # 特殊情况判断,如果有需要则更新源汇队列
        if not ready_go(dg, v, w, sNodes, tNodes):
            continue
        
        # 更新相关参数并判断是绘制直线还是弧线?
        is_draw_line = update_param(dg, v, w)

        print u'绘制', is_draw_line

        # 搜索最长路径
        print u'dfs搜索方向:', dg['dfs_direction']
        edge_path = order_dfs(dg, v, w, dg['dfs_direction'])
        if len(edge_path) == 0:
            print u'路径为空'
            continue

        dg['dfs_direction'] = True # 总是优先尝试正向搜索
        print u'v%s->v%s' % (dg.vs[v]['id'], dg.vs[w]['id']), 'path:', ' '.join([dg.es[i]['id'] for i in edge_path])
        # 查找已绘制的节点,并加入到汇点队列
        u = find_drawed_node(dg, edge_path)
        if u != -1:
            print u'新增汇点:v%s' % (get_node_id(dg, u))
            pq_put(tNodes, dg, u, 2)
            continue

        # 设置节点pred属性(后续反向搜索可能会用到???)
        set_pred_from_path(dg, edge_path)

        if is_draw_line:
            print u'直线:v%s->v%s' % (dg.vs[v]['id'], dg.vs[w]['id'])
            # 分析最长路径,并计算坐标
            draw_line_path(dg, edge_path)
        else:
            print u'弧线:v%s->v%s' % (dg.vs[v]['id'], dg.vs[w]['id'])
            draw_arc_path(dg, edge_path, dg['arc_radius'])

        # 分析最长路径,寻找新的源汇节点
        new_sNodes, new_tNodes = analyse_path(dg, edge_path)
        # 更新源汇队列
        for v in new_sNodes:
            print u'分析最长路径后, 新增源点:v%s' % get_node_id(dg, v)
            pq_put(sNodes, dg, v, 1)
        for v in new_tNodes:
            print u'分析最长路径后, 新增汇点:v%s' % get_node_id(dg, v)
            pq_put(tNodes, dg, v, 2)

        dg['arc_path_count'] += 1
    
    # 删除临时属性
    del dg.vs['in_deg']
    del dg.vs['out_deg']
    del dg.vs['drawed']
    del dg.es['drawed']
    del dg.vs['pred']
    del dg.vs['is_alive']

def node_position(dg, s, t, node_sep=12.7, ranK_sep=25.4):
    # 初始化所有节点坐标
    # init_node_xy1(dg, s, t, node_sep, ranK_sep)
    # 只初始化源点和汇点的坐标
    init_node_xy2(dg, s, t, node_sep, ranK_sep)
    # 优化节点坐标: 使用并联通路法绘制
    optimize_node_xy(dg, s, t)
    # 打印节点坐标
    # print_node_xy(dg)

def draw_node(d, v, radius=1.0, height=0.618):
    dg = v.graph
    x, y = v['x'], v['y']
    # 绘制节点圆
    d.append(sdxf.Circle(center=(x, y, 0), radius=radius))
    # 绘制节点文字
    d.append(sdxf.Text('v%s' % (v['id']), point=(x-0.5*radius, y-0.5*radius, 0), height=height))

def draw_line_edge(d, e, height=0.618):
    dg = e.graph
    u, v = e.tuple
    x1, y1 = dg.vs[u]['x'], dg.vs[u]['y']
    x2, y2 = dg.vs[v]['x'], dg.vs[v]['y']
    # 绘制直线分支
    d.append(sdxf.Line(points=[(x1, y1, 0), (x2, y2, 0)]))
    # 绘制分支文字
    d.append(sdxf.Text('e%s' % (e['id']), point=((x1+x2)*0.5, (y1+y2)*0.5, 0), height=height))

def draw_arc_edge(d, e, height=0.618):
    dg = e.graph
    u, v = e.tuple
    x1, y1 = dg.vs[u]['x'], dg.vs[u]['y']
    x2, y2 = dg.vs[v]['x'], dg.vs[v]['y']
    # 计算圆弧中心点坐标
    spt, ept, r = make_point1(x1, y1), make_point1(x2, y2), e['radius']
    cnt = ge_arc_center(spt, ept, r)

    # 圆心到圆弧2个端点的单位向量
    v1, v2 = spt-cnt, ept-cnt
    # 向量与x轴的角度
    startAngle, endAngle = vector_angle(v1), vector_angle(v2)
    # 这里有一点小小的改变，将分支的起点和末点换个顺序传递给arc
    if r > 0:
        startAngle, endAngle = endAngle, startAngle
    print u'角度:',rad2deg(startAngle),rad2deg(endAngle)
    # 绘制弧线分支
    d.append(sdxf.Arc(center=(cnt[0], cnt[1], 0), radius=abs(r), startAngle=rad2deg(startAngle), endAngle=rad2deg(endAngle)))
    # 绘制分支文字
    text_v = (spt+ept)*0.5 - cnt
    text_v = normal_vector1(text_v)
    text_pt = cnt + text_v*abs(r)
    d.append(sdxf.Text('e%s' % (e['id']), point=(text_pt[0], text_pt[1], 0), height=height))

def draw_edge(d, e, height=0.618):
    print u'r(e%s)=%.3f' % (e['id'], e['radius'])
    if abs(e['radius']) < 0.001:
        draw_line_edge(d, e, height)
    else:
        # draw_arc_edge(d, e, height)
        draw_line_edge(d, e, height)

def draw_graph_to_dxf(dg, dxfFile='out\\vng.dxf'):
    d=sdxf.Drawing()
    # layer = "通风网络图"
    # d.layers.append(sdxf.Layer(name=layer,color=7))
    for v in dg.vs:
        draw_node(d, v)
    for e in dg.es:
        draw_edge(d, e)
    d.saveas(dxfFile)

def vng_opt(vnet):
    # 增加虚拟源汇
    vnet.addVirtualST()

    dg, s, t = vnet.graph(), vnet.vSource(), vnet.vTarget()
    # 增加节点分层属性
    dg.vs['rank'] = 0
    # 增加节点排序属性
    dg.vs['order'] = -1
    # 增加节点属性
    dg.vs['median'] = -1.0
    # 增加一个属性, 用于记录最好的节点排序
    dg.vs['best'] = -1
    # 增加节点坐标属性
    #     DBL_MAX表示尚未设置节点坐标
    dg.vs['x'] = DBL_MAX
    dg.vs['y'] = DBL_MAX
    # 增加分支几何参数
    dg.es['radius'] = DBL_MAX # 0-表示直线分支

    # 节点分层
    rank_dg = rank(dg, s)
    # 节点排序
    ordering(rank_dg, s)
    # 节点坐标
    node_position(rank_dg, s, t)
    # 绘制图形到dxf文件
    draw_graph_to_dxf(rank_dg)

    # 删除虚拟源汇
    vnet.delVirtualST()