#-*- coding:utf-8 -*-
# 尝试使用二分匹配求解优化调节问题

import math
import networkx as nx
import pygraphviz as pgv
from igraph import *
from vno import *
import random

def write_edge_path_pairs_to_dotfile(edge_path_pairs, dot_file):
    g=pgv.AGraph('strict graph {}')
    # 设置图、节点、分支的全局属性
    g.graph_attr['rankdir']='LR'
    g.graph_attr['ranksep']='3.5'
    # g.graph_attr['nodesep']='1.0'
    # g.edge_attr['penwidth']="0.5"
    g.edge_attr['fontsize']='11'
    g.node_attr['style']='filled'
    g.node_attr['color']="skyblue"
    # 写入拓扑关系及分支的具体属性
    for i, node in enumerate(edge_path_pairs):
        u, v = 'e%d' % node[0], 'P%d' % node[1]
        # 分支的属性
        edge_attr = {}
        # 分支标签
        edge_attr['label'] = 'w%d=%.2f' % (i, node[2])
        # style =\"dashed\", penwidth=5"
        g.add_edge(u, v, **edge_attr)
    # print g.string()
    # 写入dot文件
    g.write(dot_file)
# 将通风网络写入到dot文件中
def write_bipartite_graph_to_dotfile(dg, dot_file, x='x', y='y'):
    # 利用pygraphviz创建有向图
    # pygraphviz一个bug:在AGraph的构造函数用directed=False参数无效
    # http://stackoverflow.com/questions/14374412/how-do-i-make-an-undirected-graph-in-pygraphviz
    g=pgv.AGraph('graph {}')
    # 设置图、节点、分支的全局属性
    g.graph_attr['rankdir']='LR'
    g.graph_attr['ranksep']='3.5'
    g.graph_attr['nodesep']='1.0'
    # g.graph_attr['fontname']="SimSun"
    # g.node_attr['fontname']="SimSun"
    # g.node_attr['shape']='box'
    # g.node_attr['style']='filled'
    # g.node_attr['color']='skyblue'
    # g.edge_attr['fontname']="SimSun"
    g.edge_attr['penwidth']='0.5'
    g.edge_attr['fontsize']='11'
    # 增加graphviz分支
    for u in dg.vs:
        # g.add_node(v, label='e%d' % u['id'])
        if u['type'] == 0:
            g.add_node('%s%d' % (x, u['id']))
        else:
            g.add_node('%s%d' % (y, u['id']))
    for e in dg.es:
        # 分支的属性
        edge_attr = {}
        # edge_attr['label'] = 'e%d' % (e['id'])
        # style =\"dashed\", penwidth=5"
        u = '%s%d' % (x, dg.vs[e.source]['id'])
        v = '%s%d' % (y, dg.vs[e.target]['id'])
        # print u, v
        g.add_edge(u, v, **edge_attr)
    # 写入dot文件
    g.write(dot_file)

# 修改画dot文件,并高亮最大阻力路线
def color_edges(dg, edges, dot_file, out_file, penwidth=3, color='blue', x='x', y='y'):
    # 利用pygraphviz创建有向图
    g=pgv.AGraph(dot_file)
    for i in edges:
        u, v = dg.es[i].tuple
        uu,vv = 'None', 'None'
        if dg.vs[u]['type'] == 0:
            uu = '%s%d' % (x, dg.vs[u]['id'])
        else:
            uu = '%s%d' % (y, dg.vs[u]['id'])
        if dg.vs[v]['type'] == 0:
            vv = '%s%d' % (x, dg.vs[v]['id'])
        else:
            vv = '%s%d' % (y, dg.vs[v]['id'])
        # 红色高亮最小顶点覆盖节点
        if g.has_edge(uu, vv):
            edge = g.get_edge(uu, vv)
            edge.attr['penwidth']='%.2f' % (penwidth)
            edge.attr['color']=color
    # 写入dot文件
    g.write(out_file)

# 修改画dot文件,并高亮最大阻力路线
def color_nodes(dg, nodes, dot_file, out_file, color='skyblue', x='x', y='y'):
    # 利用pygraphviz创建有向图
    g=pgv.AGraph(dot_file)
    for i in nodes:
        u = 'None'
        if dg.vs[i]['type'] == 0:
            u = '%s%d' % (x, dg.vs[i]['id'])
        else:
            u = '%s%d' % (y, dg.vs[i]['id'])
        # 红色高亮最小顶点覆盖节点
        if g.has_node(u):
            node = g.get_node(u)
            node.attr['style']='filled'
            node.attr['color']=color
    # 写入dot文件
    g.write(out_file)

# 生成分支与通路的节点对
def build_edge_path_pairs(vnet, maxH, more_maxP):
    dg = vnet.graph()
    # 构造"分支-路径"对
    # edge_path_affect_dict = {}
    edge_path_pairs = []
    for i,P in enumerate(more_maxP):
        H = vnet.hPath(P)
        for j in P:
            # 分支可调
            if dg.es[j]['adjustable']:
                # 构造"分支-分支"对
                edge_path_pairs.append([j, i, maxH-H])
    print u'二分图节点个数:',len(edge_path_pairs)
    # print edge_path_pairs
    # print edge_path_affect_dict
    return edge_path_pairs
# 2维数组转换成1维的
def two_dim_to_one_dim(d):
    if len(d) == 0:return []
    m,n=np.shape(d)
    if m*n == 0:return []
    return np.resize(d, m*n)
def search_mrp(vnet, fan):
    dg = vnet.graph()
    # 搜索风机的最大阻力路线
    maxP = mrp_fan(vnet, fan)
    maxH = vnet.hPath(maxP)
    # 搜索100条压差大于0.1Pa的最大阻力路线
    more_maxP = mrp_fan_h(vnet, fan, 100, minDeltaH=0.1, maxDeltaH=DBL_MAX)
    
    # (1) 多风井的公共分支不可调
    # unadjust_edge_set = set(apm_common_edges(vnet))
    # (2) 最大阻力路线上的分支不可调
    # unadjust_edge_set.update(maxP)
    # (3) 其他不可调分支
    # 搜索10Pa以内的最大阻力路线(这些路线的分支不参与调节)
    # unadjust_edge_set.update(two_dim_to_one_dim(apm_mrp(vnet, fan, 0, 10.0, 100)))
    print u'不可调分支:',list(unadjust_edge_set)
    
    # 原图分支增加一个dh属性
    dg.es['dh'] = 0.0
    for i,P in enumerate(more_maxP):
        H = vnet.hPath(P)
        for x,j in enumerate(P):
            if j not in unadjust_edge_set:
                dg.es[j]['dh'] = maxH-H
    # 原图分支增加一个adjustable属性
    dg.es[i]['adjustable'] = False
    # 统计所有通路上的分支
    edges = set()
    for P in more_maxP:
        edges.update(P)
    # 排除不可调分支
    edges.difference_update(unadjust_edge_set)
    # 通路上的可调分支设置为"可调"
    for i in edges:
        dg.es[i]['adjustable'] = True
    # 得到可调分支并编号(用词典记录分支的数组下标)
    print edges
    # 所有分支增加一个pos属性,表示分支在二分图中的节点编号
    dg.es['pos'] = -1
    for i, e in enumerate(edges):
        dg.es[e]['pos'] = i
    # 返回最大阻力和通路集合
    return maxH, more_maxP, edges
# 构造二分图模型
def build_bipartite_graph(vnet, fan):
    dg = vnet.graph()
    # 计算最大阻力、搜索通路集合、设置分支相关属性、可调分支集合(原图的分支作为二分图的节点)
    maxH, more_maxP, bipartite_nodes = search_mrp(vnet, fan)
    # 构造"分支-路径"对
    edge_path_pairs = build_edge_path_pairs(vnet, maxH, more_maxP)
    # 将"分支-路径"对写入到dot文件中
    write_edge_path_pairs_to_dotfile(edge_path_pairs, 'dddd.dot')
    # 渲染dot文件并输出为png图片
    draw_dot_file('dddd.dot', 'dddd.png')
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = len(bipartite_nodes)
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点的属性
    for i,e in enumerate(bipartite_nodes):
        g.vs[i]['id'] = e
        g.vs[i+n]['id'] = e
        g.vs[i]['weight'] = dg.es[e]
        g.vs[i+n]['weight'] = dg.es[e]
    # 增加边(同一条路径上的分支互相影响构成一条边)
    for i,P in enumerate(more_maxP):
        for j in P:
            for k in P:
                u = dg.es[j]['pos']
                v = dg.es[k]['pos']
                # pos属性小于0表示该分支是不可调分支
                if j == k or u < 0 or v < 0:continue
                # 同一条路径上的分支互相影响
                g.add_edge(u, v+n)
    # for i,e1 in enumerate(edges):
    #   if e1 not in edge_path_affect_dict:continue
    #   # if len(edge_path_affect_dict[e1]) == 0:continue
    #   for j,e2 in enumerate(edges):
    #       if i == j:continue
    #       e2, p2, dh2 = edge_path_pairs[j]
    #       # (1)两条分支在同一条路径中
    #       # 按照通路法的思路:一条路径只能有一条分支调节!(不一定)
    #       if p1 == p2:
    #           g.add_edge(i, j+n)
    #       # (2)同一条分支在不同的路径中
    #       elif e1 == e2:
    #           g.add_edge(i, j+n)
    #       elif p2 in edge_path_affect_dict[e1]:
    #           # g.add_edge(i, j+n)
    #           pass
    print g
    # 将构造的二分图写入到dot文件中
    write_bipartite_graph_to_dotfile(g, 'cccc.dot')
    # 将计算的最大匹配用虚线高亮显示
    # color_nodes(g, 'cccc.dot', [e.tuple for e in matching.edges()])
    # 渲染dot文件并输出为png图片
    draw_dot_file('cccc.dot', 'cccc.png')
    # 返回构造的二分图
    return g

def solve_bipartite_graph1(g):
    # (1) 用KM算法求解最大匹配
    # 二分图最大匹配
    print g.is_bipartite()
    print g.largest_independent_vertex_sets()
    print g.maximal_independent_vertex_sets()
    # print u'匹配个数:',len(matching)
    # print u'是否最大匹配?',matching.is_maximal()
    # print u'最大独立集个数:',n-len(matching)/2
    # f1 = lambda e:'e%d-P%d' % (edge_path_pairs[e.source%20][0], edge_path_pairs[e.source%20][1])
    # f2 = lambda e:'e%d-P%d' % (edge_path_pairs[e.target%20][0], edge_path_pairs[e.target%20][1])
    # f3 = lambda e:'%s -- %s' % (f1(e), f2(e))
    # print u'匹配的分支-路径对:','\n'.join([f3(e) for e in matching.edges()])
    # matching_nodes = set([f1(e) for e in matching.edges()])
    # matching_nodes.update([f2(e) for e in matching.edges()])
    # print u'匹配的节点:',matching_nodes

def solve_bipartite_graph2(dg):
    # print dg
    # (2) 用最大流方法求最大匹配
    # 原图的所有边容量为1
    dg.es['capacity'] = 1
    # 如果考虑最小权二分匹配问题, 可以转换为最小费用最大流问题,此时分支还需考虑权重问题w(e)
    # igraph没有提供该算法,如果要求解这类问题,可以使用networkx,参见本文件中的test_networkx()例子
    # dg.es['cost'] = 0
    # 增加虚拟的源点sn和汇点tn
    ss = add_virtual_source(dg)
    tt = add_virtual_target(dg)
    # 新增的虚拟分支容量为1
    for i in dg.incident(ss, mode=OUT):
        dg.es[i]['capacity'] = 1
    for i in dg.incident(tt, mode=IN):
        dg.es[i]['capacity'] = 1
    # 求最大流
    mf = dg.maxflow(ss, tt, capacity="capacity")
    # print u'节点个数(包括虚拟源汇):', len(dg.vs)
    print u'最大流:', mf.value
    # 删除虚拟源汇,以及它们的关联分支
    dg.delete_vertices([ss, tt])    
    # 查找最大匹配边
    matching_edges = []
    for e in dg.es:
        f = mf.flow[e.index]
        # print e.index, f
        # 如果分支满流(达到容量上限),即为最大匹配中的一条边
        if abs(f-e['capacity']) < 1e-3:
            matching_edges.append(e.index)
    # 删除属性数据
    del dg.es['capacity']
    return matching_edges

def bipartite_dfs(g, s):
    print u'源点:', g.vs[s]['id']
    # 增加节点属性数据,用于表示节点是否被访问或标记
    g.vs['color1'] = 0
    # 计数变量
    # 奇数表示非匹配边(e['matching']=1)
    # 偶数表示匹配边(e['matching']=0)
    n = 1
    # 堆栈
    S = [s]
    while len(S) > 0:
        # 弹出栈顶节点
        u = S.pop()
        # 标记节点
        g.vs[u]['color1'] = 1
        # 临时记录当前堆栈的元素个数
        m = len(S)
        # 遍历栈顶节点的邻接边
        for i in g.incident(u, mode=ALL):
            e = g.es[i]
            uu, v = e.tuple
            # 考虑分支有方向和无方向的情况
            if v == u:v = uu
            # 与dfs不同,无需考虑节点颜色的问题
            # if g.vs[v]['color1'] == 1: continue
            # 按照非匹配边-->匹配边-->非匹配边-->匹配边...的顺序搜索
            if (e['matching'] == 0 and n%2 != 0) or (e['matching'] == 1 and n%2 == 0):
                S.append(v)
                print (g.vs[u]['id'], g.vs[v]['id'])
        n = n + 1
    # 统计所有标记的节点
    color_nodes = [u.index for u in g.vs if u['color1'] == 1]
    # 删除属性数据
    del g.vs['color1']
    return color_nodes

# 通过二分匹配查找最小顶点覆盖
# https://www.cnblogs.com/jianglangcaijin/p/6035945.html
# https://wenku.baidu.com/view/d1b1b165783e0912a2162a9c.html
# http://dsqiu.iteye.com/blog/1689505
def find_min_vertex_cover(g, matching_edges):
    # 找最小顶点覆盖(与最小割关联的节点)
    # 增加属性数据--标记匹配边
    g.es['matching'] = 0
    # 增加属性数据--标记匹配的节点
    g.vs['matching'] = 0
    for i in matching_edges:
        g.es[i]['matching'] = 1
        u, v = g.es[i].tuple
        # print g.vs[u]['id'], g.vs[v]['id']
        g.vs[u]['matching'] = 1
        g.vs[v]['matching'] = 1
    # 标记节点
    g.vs['color2'] = 0
    # 从右边(Y集合)找出未匹配的节点
    unmatching_nodes = [u.index for u in g.vs if u['matching'] == 0 and u['type'] == 1]
    # print unmatching_nodes
    # 按照非匹配边-->匹配边-->非匹配边-->匹配边...的顺序搜索,并标记节点
    for u in unmatching_nodes:
        # 针对二分图的简化版dfs
        color_nodes = bipartite_dfs(g, u)
        for i in color_nodes:
            g.vs[i]['color2'] = 1
    # 左侧标记的点 和 右侧未标记的点选出组成集合S, 就是一个最小顶点覆盖集
    S = set()
    # n1 -- 左侧标记过的节点个数
    # n2 -- 右侧未标记过的节点个数
    n1, n2 = 0, 0
    for u in g.vs:
        if u['type'] == 0 and u['color2'] == 1:
            S.add(u.index)
            n1 = n1 + 1
        elif u['type'] == 1 and u['color2'] == 0:
            S.add(u.index)
            n2 = n2 + 1
    # 删除属性数据
    del g.es['matching']
    del g.vs['matching']
    del g.vs['color2']
    # 右侧一个未标记点都没有!!!
    if n1 == 0:
        return []
    else:
        return list(S)

def find_max_indepent_vertex_set(g, min_vertex_cover):
    # 增加属性数据--标记匹配的节点
    g.vs['color'] = 0
    for i in min_vertex_cover:
        g.vs[i]['color'] = 1
    # 节点最大独立集合
    S = g.vs.select(color_eq=0)
    # 删除属性数据
    del g.vs['color']
    return [u.index for u in S]

def main(vnet, fan):
    dg = vnet.graph()
    # 构造二分图
    g = build_bipartite_graph(vnet, fan)
    # 求二分图的最小顶点覆盖(等价于求解最大独立集)
    solve_bipartite_graph1(g)

def build_simple_bipartite_graph1():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 5
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id
    g.vs['id'] = 0
    for i in range(n):
        g.vs[i]['id'] = i%n+1
        g.vs[i+n]['id'] = i%n+1
    # 增加边
    # g.add_edge(0, 5) # 不能在x1和y1之间连边!!!切记
    g.add_edge(0, 7)
    g.add_edge(0, 8)
    g.add_edge(0, 9)
    # g.add_edge(1, 6) # 不能在x2和y2之间连边!!!切记
    g.add_edge(1, 9)
    g.add_edge(2, 5)
    # g.add_edge(2, 7) # 不能在x3和y3之间连边!!!切记
    g.add_edge(3, 5)
    # g.add_edge(3, 8) # 不能在x4和y4之间连边!!!切记
    g.add_edge(3, 9)
    g.add_edge(4, 5)
    g.add_edge(4, 6)
    g.add_edge(4, 8)
    # g.add_edge(4, 9) # 不能在x5和y5之间连边!!!切记
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph1000020000():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 5
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id
    g.vs['id'] = 0
    for i in range(n):
        g.vs[i]['id'] = i%n+1
        g.vs[i+n]['id'] = i%n+1
    # 增加边
    # g.add_edge(0, 5)
    g.add_edge(0, 7)
    g.add_edge(0, 8)
    g.add_edge(0, 9)

    # g.add_edge(1, 6)
    g.add_edge(1, 9)
    
    # g.add_edge(2, 7)

    # g.add_edge(3, 8)
    g.add_edge(3, 9)

    # g.add_edge(4, 9)
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph30000():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 5
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id
    g.vs['id'] = 0
    for i in range(n):
        g.vs[i]['id'] = i%n+1
        g.vs[i+n]['id'] = i%n+1
    # 增加边
    g.add_edge(0, 6)

    g.add_edge(1, 5)
    g.add_edge(1, 7)
    g.add_edge(1, 8)

    g.add_edge(2, 6)
    g.add_edge(2, 8)
    g.add_edge(2, 9)

    g.add_edge(3, 6)
    g.add_edge(3, 7)

    g.add_edge(4, 7)
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph10000():
    # 构造二分图
    g = Graph(directed=True)
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(12)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs['type'] = [0]*5+[1]*5
    g.vs[10]['type'] = 0
    g.vs[11]['type'] = 1
    # 设置节点id
    g.vs['id'] = 0
    for i in range(5):
        g.vs[i]['id'] = i+1
        g.vs[i+5]['id'] = i+1
    g.vs[10]['id'] = 6
    g.vs[11]['id'] = 6
    # 增加边
    g.add_edge(0, 7)
    g.add_edge(0, 8)
    g.add_edge(0, 9)
    g.add_edge(0, 11)

    g.add_edge(1, 9)

    # g.add_edge(3, 9)
    # g.add_edge(3, 11)
    # g.add_edge(10, 9)

    # e4 和 e6调换顺序
    g.add_edge(3, 9)
    g.add_edge(10, 8)
    g.add_edge(10, 9)

    # e2和e5调换顺序
    # g.add_edge(4, 6)
    # g.add_edge(3, 9)
    # g.add_edge(3, 11)
    # g.add_edge(10, 9)

    # 返回构造的二分图
    return g

def build_simple_bipartite_graph20000():
    # 构造二分图
    g = Graph(directed=True)
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(12)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs['type'] = [0]*6+[1]*6
    # 设置节点id
    g.vs['id'] = 0
    for i in range(6):
        g.vs[i]['id'] = i+1
        g.vs[i+5]['id'] = i+1
    # 增加边
    g.add_edge(0, 7)

    g.add_edge(1, 6)
    g.add_edge(1, 8)
    g.add_edge(1, 9)
    g.add_edge(1, 10)

    g.add_edge(2, 7)
    g.add_edge(2, 9)
    g.add_edge(2, 10)
    g.add_edge(2, 11)

    g.add_edge(3, 7)
    g.add_edge(3, 8)

    g.add_edge(4, 7)
    g.add_edge(4, 8)

    g.add_edge(5, 8)
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph2():
    # 构造二分图
    g = Graph(directed=True)
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(9)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*4+[1]*5
    # 设置节点id
    g.vs['id'] = 0
    for i in range(9):
        g.vs[i]['id'] = i+1
    # 增加边
    # g.add_edge(0, 4)
    g.add_edge(0, 6)
    g.add_edge(1, 4)
    g.add_edge(1, 5)
    g.add_edge(1, 7)
    g.add_edge(2, 6)
    # g.add_edge(2, 7)
    g.add_edge(3, 6)
    g.add_edge(3, 7)
    g.add_edge(3, 8)
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph3():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 8
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id
    g.vs['id'] = 0
    for i in range(n):
        g.vs[i]['id'] = i%n+1
        g.vs[i+n]['id'] = i%n+1
    # 增加边
    g.add_edge(0, 10)
    g.add_edge(0, 13)
    g.add_edge(0, 14)
    g.add_edge(0, 15)
    g.add_edge(1, 11)
    g.add_edge(1, 12)
    g.add_edge(1, 13)
    g.add_edge(1, 15)
    g.add_edge(2, 8)
    g.add_edge(2, 13)
    g.add_edge(2, 15)
    g.add_edge(3, 9)
    g.add_edge(3, 13)
    g.add_edge(3, 15)
    g.add_edge(4, 9)
    g.add_edge(4, 15)
    g.add_edge(5, 8)
    g.add_edge(5, 9)
    g.add_edge(5, 10)
    g.add_edge(5, 11)
    g.add_edge(5, 15)
    g.add_edge(6, 8)
    g.add_edge(7, 8)
    g.add_edge(7, 9)
    g.add_edge(7, 10)
    g.add_edge(7, 11)
    g.add_edge(7, 12)
    g.add_edge(7, 13)
    
    # g.add_edge(0, 8)
    # g.add_edge(1, 9)
    # g.add_edge(2, 10)
    # g.add_edge(3, 11)
    # g.add_edge(4, 12)
    # g.add_edge(5, 13)
    # g.add_edge(6, 14)
    # g.add_edge(7, 15)
    
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph33333():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 8
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id
    g.vs['id'] = 0
    for i in range(n):
        g.vs[i]['id'] = i%n+1
        g.vs[i+n]['id'] = i%n+1
    # 增加边
    g.add_edge(0, 9)
    g.add_edge(0, 11)
    g.add_edge(0, 12)

    g.add_edge(1, 8)
    g.add_edge(1, 10)
    g.add_edge(1, 14)

    g.add_edge(2, 9)
    g.add_edge(2, 11)
    g.add_edge(2, 12)
    g.add_edge(2, 14)

    g.add_edge(3, 8)
    g.add_edge(3, 10)
    g.add_edge(3, 12)
    g.add_edge(3, 14)

    g.add_edge(4, 8)
    g.add_edge(4, 10)
    g.add_edge(4, 11)
    g.add_edge(4, 13)
    g.add_edge(4, 14)

    g.add_edge(5, 12)
    g.add_edge(5, 14)

    g.add_edge(6, 9)
    g.add_edge(6, 10)
    g.add_edge(6, 11)
    g.add_edge(6, 12)
    g.add_edge(6, 13)
    g.add_edge(6, 15)

    g.add_edge(7, 14)
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph33332222():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 8
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id
    g.vs['id'] = 0
    for i in range(n):
        g.vs[i]['id'] = i%n+1
        g.vs[i+n]['id'] = i%n+1
    # 增加边
    g.add_edge(0, 10)
    g.add_edge(0, 13)
    g.add_edge(0, 14)
    g.add_edge(0, 15)

    g.add_edge(1, 11)
    g.add_edge(1, 12)
    g.add_edge(1, 13)
    g.add_edge(1, 15)

    g.add_edge(2, 13)
    g.add_edge(2, 15)

    g.add_edge(3, 13)
    g.add_edge(3, 15)

    g.add_edge(4, 15)

    g.add_edge(5, 15)    
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph4():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 4
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id
    g.vs['id'] = 0
    for i in range(n):
        g.vs[i]['id'] = i%n+1
        g.vs[i+n]['id'] = i%n+1
    # 增加边
    g.add_edge(0, 6)
    g.add_edge(1, 7)
    g.add_edge(2, 4)
    g.add_edge(3, 5)

    return g

def build_simple_bipartite_graph5():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 3
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id
    g.vs['id'] = 0
    for i in range(n):
        g.vs[i]['id'] = i%n+1
        g.vs[i+n]['id'] = i%n+1
    # 增加边
    g.add_edge(0, 4)
    g.add_edge(0, 5)
    g.add_edge(1, 3)
    g.add_edge(1, 5)
    g.add_edge(2, 3)
    g.add_edge(2, 4)
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph6():
    # 构造二分图
    g = Graph(directed=True)
    # 二分图节点个数(一半)
    n = 7
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(2*n)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*n+[1]*n
    # 设置节点id(分支编号)
    g.vs['id'] = 0
    # 设置节点的通路编号P
    g.vs['P_id'] = 0
    g.vs[0]['id']=1; g.vs[0]['P_id'] = 1
    g.vs[1]['id']=1; g.vs[1]['P_id'] = 2
    g.vs[2]['id']=2; g.vs[2]['P_id'] = 3
    g.vs[3]['id']=3; g.vs[3]['P_id'] = 1
    g.vs[4]['id']=4; g.vs[4]['P_id'] = 2
    g.vs[5]['id']=5; g.vs[5]['P_id'] = 2
    g.vs[6]['id']=5; g.vs[6]['P_id'] = 3
    for i in range(n):
        g.vs[i+n]['id'] = g.vs[i]['id']
        g.vs[i+n]['P_id'] = g.vs[i]['P_id']
    for i in range(2*n):
        g.vs[i]['id'] = g.vs[i]['id']+g.vs[i]['P_id']*100
    # 增加边
    # g.add_edge(0, 5) # 不能在x1和y1之间连边!!!切记
    g.add_edge(0, 8)
    g.add_edge(0, 10)
    g.add_edge(1, 7)
    # g.add_edge(1, 10)
    g.add_edge(1, 11)
    g.add_edge(1, 12)
    g.add_edge(2, 13)
    g.add_edge(3, 7)
    g.add_edge(4, 8)
    g.add_edge(5, 8)
    g.add_edge(5, 11)
    g.add_edge(5, 13)
    g.add_edge(6, 9)
    g.add_edge(6, 12)
    # 返回构造的二分图
    return g

def build_simple_bipartite_graph111():
    # 构造二分图
    g = Graph(directed=True)
    # 增加节点(以原图的分支作为节点)
    g.add_vertices(8)
    # 一半是左边(X集合),一半是右边(Y集合)
    g.vs["type"] = [0]*5+[1]*3
    # 设置节点id
    g.vs['id'] = 0
    # 左侧是分支e
    for i in range(5):
        g.vs[i]['id'] = i+1
    # 右侧是通路P
    for i in range(5,8):
        g.vs[i]['id'] = i-5+1
    # 增加边
    g.add_edge(0, 5)
    g.add_edge(0, 6)
    g.add_edge(1, 7)
    g.add_edge(2, 5)
    g.add_edge(3, 6)
    g.add_edge(4, 6)
    g.add_edge(4, 7)
    return g

def test_simple():
    # 设置二分图左右的节点名称前缀(x1-->y1)
    x, y = 'x', 'y'
    # 设置二分图左右的节点名称前缀(e1-->P1)
    # x, y = 'e', 'P'

    # 1、构造二分图
    # g = build_simple_bipartite_graph1()
    # g = build_simple_bipartite_graph1000020000()
    # g = build_simple_bipartite_graph30000()

    g = build_simple_bipartite_graph10000()
    # g = build_simple_bipartite_graph20000()
    # g = build_simple_bipartite_graph2()
    
    # g = build_simple_bipartite_graph3()
    # g = build_simple_bipartite_graph33333()
    # g = build_simple_bipartite_graph33332222()
    
    # g = build_simple_bipartite_graph4()
    # g = build_simple_bipartite_graph5()
    # g = build_simple_bipartite_graph6()
    # g = build_simple_bipartite_graph111()
    print u'是否二分图:',g.is_bipartite()
    # write_vnet_network_to_dotfile(g, 'out\\graph.dot')
    # 绘制二分图(生成dot文件,利用graphviz工具渲染生成图形)
    write_bipartite_graph_to_dotfile(g, 'out\\bm.dot', x=x, y=y)
    # 2、计算最大二分匹配
    matching_edges = solve_bipartite_graph2(g)
    print u'二分匹配边:',[(g.vs[g.es[i].source]['id'],g.vs[g.es[i].target]['id']) for i in matching_edges]
    # 高亮最大二分匹配的边
    color_edges(g, matching_edges, 'out\\bm.dot', 'out\\bm_matching.dot', color='red', x=x, y=y)

    # 3、根据二分匹配找出最小顶点覆盖
    min_vertex_cover = find_min_vertex_cover(g, matching_edges)
    if len(min_vertex_cover) > 0:
        print u'最小顶点覆盖:', set([g.vs[i]['id'] for i in min_vertex_cover])
        # 高亮最小顶点覆盖
        color_nodes(g, min_vertex_cover, 'out\\bm_matching.dot', 'out\\bm_cover.dot', color='red', x=x, y=y)
        return
        # 4、根据最小顶点覆盖找出最大独立集
        max_indepent_vertex_set = find_max_indepent_vertex_set(g, min_vertex_cover)
        print u'最大独立集:', set([g.vs[i]['id'] for i in max_indepent_vertex_set])
        # 高亮最大独立集
        color_nodes(g, max_indepent_vertex_set, 'out\\bm_cover.dot', 'out\\bm_cover.dot', color='skyblue', x=x, y=y)
    else:
        print u'最小顶点覆盖和最大独立集均不存在!!!'

def test_independent_vertex_sets1():
    g = Graph.Bipartite([0, 1]*3, [(0,1),(0,3), (0,5), (2,3),(2,1),(4,5)])
    # write_bipartite_graph_to_dotfile(g, 'out\\bm.dot')
    print g
    print g.is_bipartite()
    print g.largest_independent_vertex_sets()
    print g.maximal_independent_vertex_sets()

def test_independent_vertex_sets2():
    g=Graph.Tree(5, 2, TREE_UNDIRECTED)
    # write_bipartite_graph_to_dotfile(g, 'out\\bm.dot')
    print g
    print g.is_bipartite()
    print g.largest_independent_vertex_sets()
    print g.maximal_independent_vertex_sets()

# networkx自带的max_flow_min_cost算法要求weight是一个整数!!!
# workround: 将浮点数放大,转换为整数
# https://github.com/networkx/networkx/issues/2076#issuecomment-210200259
def test_networkx():
    G = nx.DiGraph()
    G.add_edges_from([(1, 2, {'capacity': 12, 'weight': 4}),
                       (1, 3, {'capacity': 20, 'weight': 6}),
                       (2, 3, {'capacity': 6, 'weight': -3}),
                       (2, 6, {'capacity': 14, 'weight': 1}),
                       (3, 4, {'weight': 9}),
                       (3, 5, {'capacity': 10, 'weight': 5}),
                       (4, 2, {'capacity': 19, 'weight': 13}),
                       (4, 5, {'capacity': 4, 'weight': 0}),
                       (5, 7, {'capacity': 28, 'weight': 2}),
                       (6, 5, {'capacity': 11, 'weight': 1}),
                       (6, 7, {'weight': 8}),
                       (7, 4, {'capacity': 6, 'weight': 6})])
    mincostFlow = nx.max_flow_min_cost(G, 1, 7)
    print mincostFlow

if __name__=="__main__":
    # main()
    test_simple()
    # test_independent_vertex_sets1()
    # test_networkx()