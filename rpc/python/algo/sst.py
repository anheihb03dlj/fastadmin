#-*- coding:utf-8 -*-
#灵敏度计算(Sensitivity)

import numpy as np
from vno import *

#生成一个序列,使用yield关键词
#优点:使用yield后代码简洁明了,内存占用少(每次只生成一个数据)
#[2 4 8 16 32 ....]
#[0.2 0.04 0.008....]
def __gen_seq1(x,n):
    t=1
    for i in range(n):
        yield t*x
        t=t*x

#另一种生成序列的方法
#缺点:内存占用较多,比如它会一次生成50个数据
def __gen_seq2(x,n):
    d=[1]*n
    d[0]=x
    for i in range(n-1):
        d[i+1]=d[i]*x
    return d

#计算第i条分支的风阻变化时,所有分支风量的变化与该分支风阻变化的比值dq/dr
# vnet -- 通风网络的参数(分支数据 节点数据 风门数据  风机数据)
# i   -- 第i条分支(vnet.edges是一个c++中的vector)
# n   -- 最大迭代次数
# e   -- 精度
# factor -- 收敛因子
def __sst(vnet, e, nCount=10, accuracy=0.0001, factor=0.45, use_Q_or_P=True):
    dg = vnet.graph()
    # 分支e的原始风阻
    r0 = e['r']
    #记录分支e的风阻没有变化之前的所有分支风量或节点压力
    X_0 = []
    if use_Q_or_P:
        X_0 = [ee['q'] for ee in dg.es]
    else:
        X_0 = [vv['p'] for vv in dg.vs]
    #记录第k次迭代时的所有分支风量或节点压力
    X_k = X_0[:]
    #分支e风阻变化时,记录风量灵敏度或压力灵敏度
    N = len(X_0)
    D = np.zeros(N)
    # 记录每条分支的计算状态: 0--表示还需要计算, 1--计算已收敛
    S = np.zeros(N)
    #记录迭代次数
    nIter = 0

    # 生成一个数列(收敛因子[w  w^2  w^3  w^4...])
    # 注意:包含yield关键词的函数可以视为一个迭代器,可以使用for语句对迭代器进行迭代
    for w in __gen_seq1(factor, nCount):
        # 用风量的收敛判定替代灵敏度的收敛判定(参见论文)
        f1 = lambda xkk, xk, x0: xkk-((1-w)*x0+w*xk)
        # (xk-x0)/dr --> (xk-x0)/(r0*w)
        f2 = lambda xk, x0: (xk-x0)/(r0*w)

        # 第i条分支的风阻变化(r=(1+w)*r)
        e['r'] = (1+w)*r0
        # print u'e%d' % (e['id']) , w, r0, (1+w)*r0
        # 第i条分支风阻变化后,执行一次网络解算
        ret = vno2(vnet)
        # print_network(vnet, msg='网络解算结果(r%d=%.9f --> %.9f)' % (e['id'], r0, e['r']))
        if ret:
            X = []
            if use_Q_or_P:
                X = [ee['q'] for ee in dg.es]  # 解算风量
            else:
                X = [vv['p'] for vv in dg.vs]  # 节点压力
            # 收敛性判定
            for k, offset in enumerate([f1(X[i], X_k[i], X_0[i]) for i in range(N)]):
                if S[k] != 0:
                    continue
                elif abs(offset) < accuracy:
                    D[k] = f2(X[k], X_0[k])
                    S[k] = 1
            # 统计S中的1的个数,如果全部计算完成,则等于N
            if int(sum(S)) == N:
                break
            else:
                # 记录上一次迭代的风量值
                X_k[:] = X
       
        # 迭代次数加一
        nIter = nIter+1
        # 恢复分支e的风阻
        e['r'] = r0
        # 恢复所有分支的原始风量
        if use_Q_or_P:
            for k, ee in enumerate(dg.es):
                ee['q'] = X_0[k]
        else:
            for k, vv in enumerate(dg.vs):
                vv['q'] = X_0[k]
    print u'e%d' % (e['id']), 'iter:',nIter, 'max iter:', nCount
    #if nIter == nCount:
        #print i,'reach max count'    
    return D

def edge_sst(vnet, nCount=10, accuracy=0.0001, factor=0.45):
    if not vno2(vnet):
        print u'网络解算失败!'
        return [],[]
    
    # print_network(vnet, msg='网络解算结果')
    dg = vnet.graph()
    # dg['precise'] = 1e-15
    #灵敏度矩阵(它是一个n*n的方阵)
    D = []
    for e in dg.es:
        #第i条风阻发生微小变化时其它节点的压力变化(也称为压力灵敏度)
        S = __sst(vnet, e, nCount, accuracy, factor, use_Q_or_P=True)
        #添加到灵敏度矩阵中
        D.append(S)
    return dg.es, np.transpose(D)

def node_sst(vnet, nCount=10, accuracy=0.0001, factor=0.45):
    if not vno2(vnet):
        print u'网络解算失败!'
        return []
    
    # print_network(vnet, msg='网络解算结果')
    dg = vnet.graph()
    #节点压力灵敏度矩阵(它是一个m*m的方阵)
    D = []
    for e in dg.es:
        #第i条风阻发生微小变化时其它节点的压力变化(也称为压力灵敏度)
        S = __sst(vnet, e, nCount, accuracy, factor, use_Q_or_P=False)
        #添加到灵敏度矩阵中
        D.append(S)
    return dg.vs, np.transpose(D)

# 分支风阻变化对其它分支的综合影响
# 计算方法: 每一列的灵敏度绝对值之和
def forward_sst1(D):
    n, m = D.shape
    ZD = []
    for i in range(n):
        x = sum([abs(x) for x in D[:, i]])
        ZD.append(x)
    return ZD

# 其它分支风阻变化对某一条分支的综合影响
# 计算方法: 每一行的灵敏度绝对值之和
def backward_sst1(D):
    n, m = D.shape
    ZD = []
    for i in range(n):
        x = sum([abs(x) for x in D[i, :]])
        ZD.append(x)
    return ZD

# 分支风阻变化对其它分支的综合影响(相对灵敏度)
# 计算方法: 每一列的灵敏度的平方和,最后开根号
def forward_sst2(D):
    n, m = D.shape
    ZD = []
    for i in range(n):
        x = np.sqrt(sum([x*x for x in D[:, i]]))
        ZD.append(x)
    return ZD

# 其它分支风阻变化对某一条分支的综合影响(相对灵敏度)
# 计算方法: 每一行的灵敏度的平方和,最后开根号
def backward_sst2(D):
    n, m = D.shape
    ZD = []
    for i in range(n):
        x = np.sqrt(sum([x*x for x in D[i, :]]))
        ZD.append(x)
    return ZD

if __name__ == '__main__':
    print [x for x in __gen_seq1(0.1, 5)]
    print __gen_seq2(1.2,5)
