#-*- coding:utf-8 -*-

import sdxf

def test_dxf():
    d=sdxf.Drawing()

    layer = "Q-H平衡图"
    #set the color of the text layer to green
    d.layers.append(sdxf.Layer(name=layer,color=3))
    d.append(sdxf.Text('Hellold!',point=(3,0),layer=layer))
    d.append(sdxf.Line(points=[(0,0),(100,100)], layer=layer))

    d.saveas('hello_world.dxf')

if __name__ == "__main__":
    test_dxf()