#-*- coding:utf-8 -*-
import numpy as np

import pygraphviz as pgv

from vno import *
from vno_dot import *
from vno_path import *
from vno_data import *

# 测试pygraphviz的用法
# http://pygraphviz.github.io/documentation/pygraphviz-1.3rc1/tutorial.html
def test_pygraphviz():
    g=pgv.AGraph(directed=True,strict=True)
    g.add_edge(1,2)
    g.add_edge(1,3)
    g.add_edge(2,4)
    g.add_edge(2,5)
    g.add_edge(5,6)
    g.add_edge(5,7)
    g.add_edge(3,8)
    g.add_edge(3,9)
    g.add_edge(8,10)
    g.add_edge(8,11)
    g.graph_attr['epsilon']='0.001'
    # print g.string() # print dot file to standard output
    g.write(SAMPLE_DOT_FILE)
    g.layout(prog='dot') # layout with dot
    g.draw(SAMPLE_PNG_FILE) # write to file
    # g.draw('file.ps',prog='circo') # use circo to position,write PS file

def test_gv():
    # 1. 准备通风网络数据
    # (1) 用一个词典作为输入数据(包含拓扑关系和分支等数据)    
    # graph_datas = build_graph_data1()
    # graph_datas = build_graph_data2()
    # graph_datas = build_graph_data3()
    # graph_datas = build_graph_data4()
    # graph_datas = build_graph_data5()
    # graph_datas = build_graph_data6()
    # graph_datas = build_graph_data7()    
    # (2) 或者通过读取文件构造这个词典
    graph_datas = read_graph_datas('data\\graph_data11.json')
    # 2. 设置通风网络其它参数
    # graph_datas['fans'] = [] # 去掉风机
    graph_datas['qFixs'] = [] # 去掉固定风量
    # 3. 构造通风网络对象
    vnet = VentNetwork()
    if not build_network(graph_datas, vnet.graph()):
        print u'构造通风网络失败!!!'
    else:
        # print vnet.graph()
        print u'构造通风网络成功!!!'
        # 4. 添加虚拟源汇,将网络变成单一源汇通风网络
        vnet.addVirtualST();
        # print u'虚拟源点:',vnet.vSource(), '虚拟汇点:',vnet.vTarget()
        # 5. 通风网络解算
        ret = vno(vnet)
        if not ret:
            print u'网络解算失败,退出!'
        else:
            # 6. 打印通风网络解算结果
            print_network(vnet, msg='网络解算结果')
        # 11. 删除虚拟源汇
        vnet.delVirtualST();
        # 12. 通风网络生成dot文件
        write_vnet_network_to_dotfile(vnet.graph(), SAMPLE_DOT_FILE)

if __name__=="__main__":
    test_pygraphviz()
    # test_gv()