﻿import time as time
import pandas as pd
from scipy.cluster.hierarchy import linkage,fcluster,dendrogram
from scipy.cluster.vq import vq, kmeans, whiten,kmeans2
from scipy import spatial

from sklearn import metrics
from sklearn.cluster import KMeans,Ward
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale

import matplotlib.pyplot as plt

def fca(data,n_clusters=3):
    # st = time.time()
    ward = Ward(n_clusters=n_clusters).fit(data)
    label = ward.labels_
    # print("Elapsed time: ", time.time() - st)
    # print("Number of points: ", label.size)
    #print label

    centroids=[]
    #reduced_data = PCA(n_components=).fit_transform(data)
    kmeans = KMeans(init='k-means++', n_clusters=1, n_init=10)
    for i in range(n_clusters):
        c_data=[]
        c_index=[]
        for j in range(label.size):
            if label[j] == i:
                #print u'i=',i,'j=',j,'data=',data
                c_data.append(data[j])
                c_index.append(j)

        kmeans.fit(c_data)
        centroid = kmeans.cluster_centers_[0]
        #print c_data
        #print centroid
       
        c_dist=[]
        for xdata in c_data:
            c_dist.append(sum([(x-y)**2 for x,y in zip(xdata,centroid)]))
        c_i=c_dist.index(max(c_dist))
        centroids.append(c_index[c_i])
    return centroids


if __name__ == '__main__':
    data  = [[0.1,0.1,0.1],
            [0.1,0.1,0.1],
            [0.1,0.1,0.1],
            [0.2,0.2,0.2],
            [0.2,0.2,0.2],
            [0.2,0.2,0.2],
            [0.3,0.3,0.3],
            [0.3,0.3,0.3],
            [0.3,0.3,0.3]]

    data2 = pd.read_csv('data.csv',index_col=0)
    data3 =[[0.2],[0.3],[1.2],[1.7],[1.24],[0.24],[1.9],[1.45]]
    
    matData = data2.as_matrix()
    whiten_data = whiten(matData)
    #whiten_data = whiten(data)
    #print whiten_data
    
    #分类后计算聚类重心的索引位置
    centroids=fca(whiten_data,12)

    print [whiten_data[i] for i in centroids]
