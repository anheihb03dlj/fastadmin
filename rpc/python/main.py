#coding:utf-8

import os
# import threading
import multiprocessing as mp
'''
解决pyinstaller打包multiprocess模块的问题
http://blog.csdn.net/xiong_big/article/details/54614231
https://github.com/pyinstaller/pyinstaller/wiki/Recipe-Multiprocessing
http://redino.net/blog/2016/04/multiprocessing-frozen-python/
https://stackoverflow.com/questions/24944558/pyinstaller-built-windows-exe-fails-with-multiprocessing
'''
import multi_process_package
import rpc_server

# 调用windows命令taskkill杀死程序
def kill_exe(exe):
    command = 'taskkill /F /IM %s' % exe
    os.system(command)

# 启动一个线程,运行rpc服务器
def run_rpc_server1():
    p = mp.Process(target = rpc_server.main, args=())
    p.daemon = True
    p.start()
    p.join()

if __name__ =='__main__':
    # 解决pyinstaller打包multirpocessing模块的问题
    mp.freeze_support()
    # 启动一个线程,运行rpc服务器
    run_rpc_server1()

    # 启动主界面
    # MainWindow.run()

    #杀死所有的python进程
    # kill_exe('python.exe')
