#-*- coding:utf-8 -*-

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

def make_rpc_server(processor, host='localhost', port=9090):
    transport = TSocket.TServerSocket(host=host, port=port)
    tfactory = TTransport.TBufferedTransportFactory()
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()
    # 1、只能连接一个客户端
    server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
    # You could do one of these for a multithreaded server
    # 2、每个客户端启动一个线程用于连接
    # server = TServer.TThreadedServer(processor, transport, tfactory, pfactory)
    # 3、使用线程池
    # server = TServer.TThreadPoolServer(processor, transport, tfactory, pfactory)
    # server.setNumThreads(10)
    # 后台进程
    server.daemon = True #enable ctrl+c to exit the server
    return server

# 运行服务器
def run_rpc_server(server):
    server.serve()