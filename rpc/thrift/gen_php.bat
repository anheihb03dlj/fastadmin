@echo off

rem 生成php的服务端需要加server
rem gen_code.bat php:server

rem 生成php客户端代码
call gen_code.bat php

REM rd /s /q "..\php\vno\"
REM md "..\php\vno\"
REM xcopy "gen-php\vno" "..\php\vno\"  /c /e /y

REM 复制到thinkphp的library下(自动加载，类似python的import机制)
xcopy "..\php\Thrift" "..\..\extend\Thrift\"  /c /e /y
xcopy "gen-php\vno" "..\..\extend\vno\"  /c /e /y

rem 暂停
pause