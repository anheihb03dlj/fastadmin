function randomUUID() {
    return v4();
}

function MisObject(viewer) {
    this.viewer = viewer;
    this.dmp = new diff_match_patch();
    this.init(this.viewer.io.serializeSketch());
  }
  
  HistoryManager.prototype.init = function(sketchData) {
    this.lastCheckpoint = sketchData;
    this.diffs = [];
    this.historyPointer = -1;
  };

var canvas = new fabric.Canvas('c');
canvas.fireMiddleClick = true;
// canvas.fireRightClick = true;
canvas.add(new fabric.Rect({ width: 50, height: 50, fill: 'blue', angle: 10 }));
canvas.add(new fabric.Circle({ radius: 50, fill: 'red', top: 44, left: 80 }));
canvas.add(new fabric.Ellipse({ rx: 50, ry: 10, fill: 'yellow', top: 80, left: 35 }));
canvas.add(new fabric.Rect({ width: 50, height: 50, fill: 'purple', angle: -19, top: 70, left: 70 }));
canvas.add(new fabric.Circle({ radius: 50, fill: 'green', top: 110, left: 30 }));
canvas.add(new fabric.Ellipse({ rx: 50, ry: 10, fill: 'orange', top: 12, left: 100, angle: 30 }));

canvas.on('mouse:down', function (opt) {
    var evt = opt.e;
    if (evt.button === 1) {
        this.isDragging = true;
        this.selection = false;
        this.lastPosX = evt.clientX;
        this.lastPosY = evt.clientY;
    }
});

canvas.on('mouse:move', function (opt) {
    if (this.isDragging) {
        var e = opt.e;
        this.viewportTransform[4] += e.clientX - this.lastPosX;
        this.viewportTransform[5] += e.clientY - this.lastPosY;
        this.requestRenderAll();
        this.lastPosX = e.clientX;
        this.lastPosY = e.clientY;
    }
});

canvas.on('mouse:up', function (opt) {
    this.isDragging = false;
    this.selection = true;
});

canvas.on('mouse:wheel', function (opt) {
    var delta = opt.e.deltaY;
    var pointer = canvas.getPointer(opt.e);
    var zoom = canvas.getZoom();
    // console.log(zoom+", "+delta)
    // zoom = zoom + delta / 250;
    zoom = zoom*(1 + delta / 500);  // 按照1.2的比例进行缩放
    // console.log(zoom)
    if (zoom > 10) zoom = 10;
    if (zoom < 0.1) zoom = 0.1;
    canvas.zoomToPoint({ x: opt.e.offsetX, y: opt.e.offsetY }, zoom);
    opt.e.preventDefault();
    opt.e.stopPropagation();
});
