var myDiagram, myMinimap, bluegrad, greengrad;
var spline_tool, arc_tool;
var slider;
var gradScaleHoriz, gradScaleVert, gradIndicatorHoriz, gradIndicatorVert;
var diagram_div = "diagram-div";
var minimap_div = "minimap-div";
var upload_files = null; // 全局变量,记录上传的文件
var upload_graph_inst = null; // 全局变量,记录上传组件对象
var loading_index = -1;
var graph_config = {
    /* 图形的基本信息 */
    graph_name: "文档1",
    company: "辽宁工程技术大学",
    author: "安全人",
    drawing_date: "",
    /* 图形导出相关的参数 */
    img_scale: 2, // 缩放比例(导出)
    img_type: "svg", // 图形格式(导出)
    data_type: "txt", // 数据格式(导入)
    /* 布局计算有关的参数 */
    nodesep: 50.8, // 水平间距
    ranksep: 38.1, // 垂直间距,
    rankdir: 'BT',
    splines: 'spline',
    node_width: 40.0,
    node_height: 30.0,
    fontsize: 12.0 // 字体高度(单位:px)
};

function initSlider() {
    slider = $("#slider1").slider({
        range: "max",
        min: 10,
        max: 500,
        step: 10,
        value: 100,
        slide: function (event, ui) {
            // 修改网络图的缩放比例
            // 参考: D:/develop/GitTest/GoJS/intro/viewport.html#ScrollingModes
            myDiagram.startTransaction("change scale value");
            myDiagram.scale = ui.value * 0.01;
            myDiagram.commitTransaction("change scale value");

            // 更新当前缩放比例值
            $("#zoom-slider > a").text(ui.value + "%");
        }
    });
}

// 这个例子的代码比较规范,使用函数将代码分开,值得借鉴和模仿
// file:///D:/develop/GitTest/GoJS/samples/systemDynamics.html
var SD = {
    mode: "pointer", // Set to default mode.  Alternatives are "node" and "link", for
    // adding a new node or a new link respectively.
    itemType: "pointer", // Set when user clicks on a node or link button.
    nodeCount: 0,
    linkCount: 0
};

function getNodeCount() {
    SD.nodeCount = myDiagram.nodes.count;
    return SD.nodeCount + 1;
}

function getLinkCount() {
    SD.linkCount = myDiagram.links.count;
    return SD.linkCount;
}

function makeFont(fontsize, isBold) {
    var font = fontsize + "px helvetica, arial, sans-serif";
    if (isBold) {
        return "Bold " + font;
    } else {
        return font;
    }
}

function getFontHeight(font) {
    var i = font.indexOf("px");
    if (i > -1) {
        return parseInt(font.substring(0, i));
    } else {
        return -1;
    }
}

// generate some colors based on hue value
function makeFill(number) {
    return HSVtoRGB(0.1 * number, 0.5, 0.7);
}
function makeStroke(number) {
    return HSVtoRGB(0.1 * number, 0.5, 0.5); // same color but darker (less V in HSV)
}
function HSVtoRGB(h, s, v) {
    var r, g, b, i, f, p, q, t;
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0:
            (r = v), (g = t), (b = p);
            break;
        case 1:
            (r = q), (g = v), (b = p);
            break;
        case 2:
            (r = p), (g = v), (b = t);
            break;
        case 3:
            (r = p), (g = q), (b = v);
            break;
        case 4:
            (r = t), (g = p), (b = v);
            break;
        case 5:
            (r = v), (g = p), (b = q);
            break;
    }
    return (
        "rgb(" +
        Math.floor(r * 255) +
        "," +
        Math.floor(g * 255) +
        "," +
        Math.floor(b * 255) +
        ")"
    );
}

function randomUUID() {
    return v4();
}

function setMode(mode, itemType) {
    myDiagram.startTransaction("mode changed");
    // 切换按钮状态(目前尚未实现)
    // document.getElementById(SD.itemType + "_button").className = SD.mode + "_normal";
    // document.getElementById(itemType + "_button").className = mode + "_selected";
    SD.mode = mode;
    SD.itemType = itemType;
    if (mode === "pointer") {
        myDiagram.allowLink = false;
        myDiagram.nodes.each(function (n) {
            n.port.cursor = "";
        });
    } else if (mode === "node") {
        // console.log('set node mode...');
        myDiagram.allowLink = false;
        myDiagram.nodes.each(function (n) {
            n.port.cursor = "";
        });
    } else if (mode === "link") {
        myDiagram.allowLink = true;
        myDiagram.nodes.each(function (n) {
            n.port.cursor = "pointer";
        });
    }
    myDiagram.commitTransaction("mode changed");
}

function configTemplates() {
    var $$ = go.GraphObject.make; // for conciseness in defining templates

    // helper functions for the templates
    function nodeStyle() {
        return [
            {
                type: go.Panel.Spot,
                // layerName: "Background",
                locationObjectName: "SHAPE",
                selectionObjectName: "SHAPE",
                locationSpot: go.Spot.Center
                // resizable: true, resizeObjectName: "PANEL",
                // selectionAdorned: false  // 拖拽时不显示外框
                // 事件绑定
                // click: function (e, obj) {
                //     alert("Clicked on " + obj.part.data.key);
                // },
                // selectionChanged: function (part) {
                //     var shape = part.elt(0);
                //     // shape.fill = part.isSelected ? "red" : "white";
                //     alert("selectionChanged " + part.data.key);
                // }
            },
            new go.Binding("location", "loc", go.Point.parse).makeTwoWay(
                go.Point.stringify
            )
        ];
    }

    function shapeStyle() {
        return {
            name: "SHAPE",
            // stroke: "#A7E7FC", // bluegrad, greengrad
            // fill: "transparent",
            stroke: null,
            fill: bluegrad,
            portId: "", // so links will go to the shape, not the whole node
            // cursor: "pointer", // the Shape is the port, not the whole Node
            fromLinkable: true,
            toLinkable: true,
            fromLinkableSelfNode: false,
            fromLinkableDuplicates: false,
            toLinkableSelfNode: false,
            toLinkableDuplicates: false
            // fromSpot: go.Spot.AllSides,
            // toSpot: go.Spot.AllSides
        };
    }

    function textStyle() {
        return [
            {
                font: makeFont(graph_config.fontsize, false),
                isMultiline: false,
                // margin: 2,
                // stroke: "blue",
                editable: false
            },
            //绑定TextBlock.text 属性为Node.data.key的值，Model对象可以通过Node.data.key获取和设置TextBlock.text
            new go.Binding("text").makeTwoWay(),
            new go.Binding("font").makeTwoWay()
        ];
    }
    // define the Node template
    myDiagram.nodeTemplate = $$(
        go.Node,
        "Horizontal", // second argument of a Node/Panel can be a Panel type
        nodeStyle(),
        /* 设置节点的外观 */
        $$(
            go.Panel,
            "Auto", //水平或垂直布局(图片在左，文字在右  或 上下)
            $$(
                go.Shape,
                "Ellipse",
                shapeStyle(),
                {
                    // figure: "Ellipse", // 节点形状为椭圆形
                    desiredSize: new go.Size(
                        graph_config.node_width,
                        graph_config.node_height
                    )
                    // minSize: new go.Size(40, 30)
                    // the port is in front and transparent, even though it goes around the text;
                    // in "link" mode will support drawing a new link
                    // isPanelMain: true
                },
                // new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify)
                new go.Binding("width", "width").makeTwoWay(),
                new go.Binding("height", "height").makeTwoWay()
                // new go.Binding("fill", "isSelected", function (s, obj) { return s ? "red" : obj.part.data.color; }).ofObject()
            ),
            $$(
                go.TextBlock,
                "new", // 默认文本
                textStyle()
            )
        )
    );

    function toPoints(data, link) {
        var points = JSON.parse(data);
        var ptList = new go.List(go.Point);  // make a list of Points
        points.forEach(function (val) {
            ptList.add(new go.Point(val.x, val.y));
        });
        return ptList;
    };

    function fromPoints(ptList, data, model) {
        var ptArray = [];
        ptList.each(function(val) {
            ptArray.push({
                "x":val.x,
                "y":val.y
            });
        });
        // model.setDataProperty(data, key, JSON.stringify(ptArray));
        return JSON.stringify(ptArray);
    };
    
    // function fromPoints1(ptList, data, model) {
    //     return fromPoints("line_points", ptList, data, model);
    // }
    
    // function fromPoints2(ptList, data, model) {
    //     return fromPoints("spline_points", ptList, data, model);
    // }

    function toOrthogonal(data, link) {
        // console.log('set routing: '+data);
        return (data==true) ? go.Link.Orthogonal : go.Link.Normal;
    };

    // replace the default Link template in the linkTemplateMap
    // 直线分支可以像多段线一样进行编辑!!!
    // file:///D:/develop/GitTest/GoJS/extensions/PolylineLinking.html
    myDiagram.linkTemplateMap.add(
        "line",
        $$(
            go.Link, // the whole link panel
            {
                routing: go.Link.Normal,
                curve: go.Link.None,
                // corner: 5,
                reshapable: true,
                resegmentable: true, // 允许分段                
                relinkableFrom: true,
                relinkableTo: true,
                adjusting: go.Link.Stretch
            },
            new go.Binding("points", "line_points", toPoints).makeTwoWay(fromPoints),
            new go.Binding("routing", "ortho", toOrthogonal),
            $$(go.Shape, { stroke: "#424242", strokeWidth: 1.5 }), // dark gray
            $$(go.Shape, { toArrow: "standard", stroke: null }),
            $$(
                go.Panel,
                "Auto",
                $$(
                    go.Shape, // the label background, which becomes transparent around the edges
                    {
                        fill: $$(go.Brush, "Radial", {
                            0: "rgb(240, 240, 240)",
                            0.3: "rgb(240, 240, 240)",
                            1: "rgba(240, 240, 240, 0)"
                        }),
                        // fill: "transparent",
                        stroke: null
                    }
                ),
                $$(
                    go.TextBlock, // the label text
                    {
                        textAlign: "center",
                        font: makeFont(graph_config.fontsize, false),
                        stroke: "#000000",
                        margin: 4,
                        editable: false // enable in-place editing
                    },
                    new go.Binding("text").makeTwoWay(),
                    new go.Binding("font").makeTwoWay()
                )
            )
        )
    );

    myDiagram.linkTemplateMap.add(
        "spline",
        $$(
            go.Link, // the whole link panel
            {
                routing: go.Link.Normal,
                curve: go.Link.Bezier,
                reshapable: true,
                toShortLength: 3,
                relinkableFrom: true,
                relinkableTo: true,
                adjusting: go.Link.Stretch
            },
            new go.Binding("points", "spline_points", toPoints).makeTwoWay(fromPoints),
            // new go.Binding("curviness").makeTwoWay(),
            $$(go.Shape, { strokeWidth: 1.5 }),
            // 箭头在分支末尾
            $$(go.Shape, { toArrow: "standard", stroke: null }),
            // 箭头在分支中间
            // $$(go.Shape, { toArrow: "OpenTriangle", segmentIndex: -Infinity }),
            $$(
                go.Panel,
                "Auto",
                $$(
                    go.Shape, // the label background, which becomes transparent around the edges
                    {
                        fill: $$(go.Brush, "Radial", {
                            0: "rgb(240, 240, 240)",
                            0.3: "rgb(240, 240, 240)",
                            1: "rgba(240, 240, 240, 0)"
                        }),
                        // fill: "transparent",
                        stroke: null
                    }
                ),
                $$(
                    go.TextBlock, // the label text
                    {
                        textAlign: "center",
                        font: makeFont(graph_config.fontsize, false),
                        stroke: "#000000",
                        margin: 4,
                        editable: false // enable in-place editing
                    },
                    new go.Binding("text").makeTwoWay(),
                    new go.Binding("font").makeTwoWay()
                )
            )
        )
    );

    myDiagram.linkTemplateMap.add(
        "arc",
        $$(
            go.Link, // the whole link panel
            {
                routing: go.Link.Normal,
                curve: go.Link.Bezier,
                reshapable: true,
                toShortLength: 3,
                relinkableFrom: true,
                relinkableTo: true
                // adjusting: go.Link.Stretch
            },
            // don't need to save Link.points, so don't need TwoWay Binding on "points"
            // new go.Binding("points").makeTwoWay(),
            new go.Binding("curviness").makeTwoWay(), // but save "curviness" automatically
            $$(go.Shape, { strokeWidth: 1.5 }),
            $$(go.Shape, { toArrow: "standard", stroke: null }),
            $$(
                go.Panel,
                "Auto",
                $$(
                    go.Shape, // the label background, which becomes transparent around the edges
                    {
                        fill: $$(go.Brush, "Radial", {
                            0: "rgb(240, 240, 240)",
                            0.3: "rgb(240, 240, 240)",
                            1: "rgba(240, 240, 240, 0)"
                        }),
                        // fill: "transparent",
                        stroke: null
                    }
                ),
                $$(
                    go.TextBlock, // the label text
                    {
                        textAlign: "center",
                        font: makeFont(graph_config.fontsize, false),
                        stroke: "#000000",
                        margin: 4,
                        editable: false // enable in-place editing
                    },
                    new go.Binding("text").makeTwoWay(),
                    new go.Binding("font").makeTwoWay()
                )
            )
        )
    );
}

function configTools() {
    var $$ = go.GraphObject.make; // for conciseness in defining templates

    // 修改平移视图默认快捷键(按住鼠标左键 --> ctrl+鼠标左键)
    myDiagram.toolManager.panningTool.canStart = function () {
        // Maybe do something else before
        // ...

        // Be careful about using 'this' within such functions!

        // In cases where you want normal behavior, call the base functionality.
        // Note the reference to the prototype
        // and the call to 'call' passing it what 'this' should be.

        // 默认左键拖动视图
        // return go.PanningTool.prototype.canStart.call(myDiagram.toolManager);
        // ctrl+鼠标左键
        // return myDiagram.lastInput.control && myDiagram.lastInput.left;
        // 鼠标中键(有bug,按住中键可以拖动视图,但是滚轮滚动却失效了)
        if (myDiagram.lastInput.middle) {
            // gojs的鼠标中键按下之后也会出发滚动scroll消息,导致滚轮缩放功能失效
            // 需要强制修改mouseWheelBehavior属性!!!
            myDiagram.toolManager.mouseWheelBehavior = go.ToolManager.WheelZoom;
            return true;
        } else {
            return false;
        }
        // Maybe do something else after
        // ...
    };

    // 修改拖拽框选的外框效果
    myDiagram.toolManager.dragSelectingTool.isPartialInclusion = false; // 必须全部包含在框里才能选中!
    // replace the magenta box with a red one
    myDiagram.toolManager.dragSelectingTool.box = $$(
        go.Part,
        { layerName: "Tool", selectable: false },
        $$(go.Shape, {
            name: "SHAPE",
            fill: "rgba(255,0,0,0.1)",
            stroke: "red",
            strokeWidth: 2
        })
    );

    myDiagram.allowLink = false; // linking is only started via buttons, not modelessly
    myDiagram.toolManager.linkingTool.portGravity = 0; // no snapping while drawing new links
    myDiagram.toolManager.linkingTool.doActivate = function () {
        // change the curve of the LinkingTool.temporaryLink
        this.temporaryLink.curve =
            SD.itemType === "line" ? go.Link.Normal : go.Link.Bezier;
        this.temporaryLink.path.stroke = "red";
        this.temporaryLink.path.strokeWidth = 3;
        go.LinkingTool.prototype.doActivate.call(this);
    };

    // 自定义分支创建过程
    // override the link creation process
    myDiagram.toolManager.linkingTool.insertLink = function (
        fromnode,
        fromport,
        tonode,
        toport
    ) {
        // to control what kind of Link is created,
        // change the LinkingTool.archetypeLinkData's category
        // myDiagram.model.setCategoryForLinkData(this.archetypeLinkData, 'line');
        // customize the data for the new node
        var newId = getLinkCount();
        this.archetypeLinkData = {
            key: randomUUID(),
            id: newId,
            // from: from, to: to, // s和t表示key
            text: "e" + newId,
            category: "line",
            r: 0.0,
            q: 0.0,
            s: 0.0,
            font: makeFont(graph_config.fontsize, false)
        };
        return go.LinkingTool.prototype.insertLink.call(
            this,
            fromnode,
            fromport,
            tonode,
            toport
        );
    };

    spline_tool = myDiagram.toolManager.linkReshapingTool;
    arc_tool = new CurvedLinkReshapingTool();

    //允许在画布上面双击的时候创建节点
    // myDiagram.clickCreatingTool.archetypeNodeData = { text: "新节点" };

    // 自定义节点插入过程
    myDiagram.toolManager.clickCreatingTool.archetypeNodeData = {}; // enable ClickCreatingTool
    myDiagram.toolManager.clickCreatingTool.isDoubleClick = true; // operates on a single click in background
    myDiagram.toolManager.clickCreatingTool.canStart = function () {
        // but only in "node" creation mode
        return (
            SD.mode === "node" && go.ClickCreatingTool.prototype.canStart.call(this)
        );
    };
    myDiagram.toolManager.clickCreatingTool.insertPart = function (loc) {
        // customize the data for the new node
        // 当前节点个数作为id
        var newId = getNodeCount();
        this.archetypeNodeData = {
            key: randomUUID(),
            id: newId,
            text: "v" + newId,
            p: 0.0,
            z: 0.0,
            width: graph_config.node_width,
            height: graph_config.node_height,
            font: makeFont(graph_config.fontsize, false)
        };
        return go.ClickCreatingTool.prototype.insertPart.call(this, loc);
    };
}

function configCommands() {
    var $$ = go.GraphObject.make; // for conciseness in defining templates

    // 自定义复制粘贴命令以及键盘方向键控制(默认粘贴时偏移20)
    myDiagram.commandHandler = new DrawCommandHandler(30, 0); // defined in DrawCommandHandler.js
    // 通过方向键可以移动节点位置
    myDiagram.commandHandler.arrowKeyBehavior = "move";
    //   myDiagram.commandHandler.arrowKeyBehavior = "select";
    //   myDiagram.commandHandler.arrowKeyBehavior = "scroll";

    myDiagram.commandHandler.groupSelection = function () {
        layer.prompt(
            {
                value: "默认",
                title: "请输入分组的名称"
            },
            function (value, index, elem) {
                // 设置输入的分组名称
                myDiagram.commandHandler.archetypeGroupData.text = value;
                // 调用分组功能
                go.CommandHandler.prototype.groupSelection.call(
                    myDiagram.commandHandler
                );
                // 关闭对话框
                layer.close(index);
            }
        );
    };
}

Date.prototype.format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        S: this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(
            RegExp.$1,
            (this.getFullYear() + "").substr(4 - RegExp.$1.length)
        );
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(
                RegExp.$1,
                RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length)
            );
        }
    }
    return fmt;
};

function updateGraphDataForm(data) {
    setValue("graph-data-form", "graph_name", data.graph_name);
    // setValue("graph-data-form", "nodesep", data.nodesep);
    // setValue("graph-data-form", "ranksep", data.ranksep);
    // setValue("graph-data-form", "fontsize", data.fontsize);
    setValue("graph-data-form", "company", data.company);
    setValue("graph-data-form", "author", data.author);
    setValue("graph-data-form", "node_width", data.node_width);
    setValue("graph-data-form", "node_height", data.node_height);
    setValue("graph-data-form", "fontsize", data.fontsize);

    var date = data.drawing_date;
    // console.log(date);
    if (date == "") date = new Date().format("yyyy-MM-dd");
    $(".form_date").datetimepicker("update", date); //赋值
}

function updateNodeDataForm(data) {
    setValue("node-data-form", "node_key", data.key);
    setValue("node-data-form", "node_id", data.id);
    setValue("node-data-form", "node_name", data.text);
    var pt = go.Point.parse(data.loc);
    pt.y *= -1;
    setValue("node-data-form", "node_loc", go.Point.stringify(pt));
    // 读取节点宽度
    var width = data.width;
    if (isNaN(width)) width = graph_config.node_width;
    setValue("node-data-form", "node_width", width);
    // 读取节点高度
    var height = data.height;
    if (isNaN(height)) height = graph_config.node_height;
    setValue("node-data-form", "node_height", height);
    // 读取节点字体大小
    var fontsize = getFontHeight(data.font);
    if (fontsize < 0) fontsize = graph_config.fontsize;
    setValue("node-data-form", "fontsize", fontsize);
    // 读取节点其它专业数据
    setValue("node-data-form", "node_p", data.p);
    setValue("node-data-form", "node_z", data.z);
}

function updatelinkReshapingTool(type) {
    if (type == "spline") {
        myDiagram.toolManager.linkReshapingTool = spline_tool;
    } else if (type == "arc") {
        myDiagram.toolManager.linkReshapingTool = arc_tool;
    } else {
        myDiagram.toolManager.linkReshapingTool = spline_tool;
    }
}

function updateEdgeDataForm(data) {
    setValue("edge-data-form", "edge_key", data.key);
    setValue("edge-data-form", "edge_id", data.id);
    setValue("edge-data-form", "edge_name", data.text);
    $("#edge-data-form #edge_category").val(data.category);
    // 更新分支的reshape更新
    updatelinkReshapingTool(data.category);
    // 读取节点字体大小
    var fontsize = getFontHeight(data.font);
    if (fontsize < 0) fontsize = graph_config.fontsize;
    setValue("edge-data-form", "fontsize", fontsize);
    setValue("edge-data-form", "edge_r", data.r);
    setValue("edge-data-form", "edge_q", data.q);
    setValue("edge-data-form", "edge_s", data.s);
}

function configListener() {
    var $$ = go.GraphObject.make; // for conciseness in defining templates

    myDiagram.addDiagramListener("ChangedSelection", function (e) {
        var parts = e.diagram.selection;
        var part = null;
        if (parts != null && parts.count == 1) {
            part = parts.first();
        }
        if (part == null) {
            console.log("切换到图形");
            console.log(graph_config);
            $("#node_detailpannel").css("display", "none");
            $("#edge_detailpannel").css("display", "none");
            $("#graph_detailpannel").css("display", "block");

            // 更新界面
            updateGraphDataForm(graph_config);
        } else if (partIsNode(part)) {
            console.log("切换到节点");
            console.log(part.data);
            $("#node_detailpannel").css("display", "block");
            $("#edge_detailpannel").css("display", "none");
            $("#graph_detailpannel").css("display", "none");

            // 更新界面
            updateNodeDataForm(part.data);
        } else if (partIsLink(part)) {
            console.log("切换到分支");
            console.log(part.data);
            // if(part.data.category == 'spline') {
            //     console.log(part.data.points.count);
            //     var it = part.data.points.iterator;
            //     while (it.next()) {
            //         var item = it.value;
            //         console.log(item);
            //     }
            // }
            $("#node_detailpannel").css("display", "none");
            $("#edge_detailpannel").css("display", "block");
            $("#graph_detailpannel").css("display", "none");

            // 更新界面
            updateEdgeDataForm(part.data);
        }
    });

    // myDiagram.addDiagramListener("SelectionMoved", function (e) {
    //     // var div = diagram.div;
    //     // div.style.width = '200px';
    //     // diagram.requestUpdate(); // Needed!
    //     console.log(e.subject);
    //     var it = e.subject.iterator;
    //     while (it.next()) {
    //         console.log(it.value);
    //         // var pt = it.value;
    //         // points.push({ x: pt.x, y: -1 * pt.y });
    //     }
    // });

    // myDiagram.addDiagramListener("ObjectSingleClicked", function (e) {
    //     // var div = diagram.div;
    //     // div.style.width = '200px';
    //     // diagram.requestUpdate(); // Needed!
    //     var part = e.subject.part;
    //     // console.log(part.data);
    //     var data = part.data;
    //     // console.log(part.type);
    // });

    // myDiagram.addDiagramListener("ObjectDoubleClicked", function (e) {
    //     var part = e.subject.part;
    //     console.log("Double-clicked at " + part.data.key);
    // });

    myDiagram.addDiagramListener("ClipboardPasted", function (e) {
        var parts = e.subject;
        parts.each(function (part) {
            // console.log(part.type);
            // 修改复制粘贴节点或分支的key
            if (partIsNode(part)) {
                myDiagram.model.setKeyForNodeData(part.data, randomUUID());
            } else if (partIsLink(part)) {
                myDiagram.model.setKeyForLinkData(part.data, randomUUID());
            }
            // 修改节点或分支的text属性值
            myDiagram.model.setDataProperty(
                part.data,
                "text",
                part.data.text + "-副本"
            );
        });
        console.log("Pasted " + e.diagram.selection.count + " parts");
    });

    myDiagram.addDiagramListener("ViewportBoundsChanged", function (e) {
        // old value
        var scale = e.subject;
        var rect = e.parameter;
        // new value
        // console.log(myDiagram.scale);
        var factor = Math.round(myDiagram.scale * 100);
        // console.log(factor);
        slider.slider("value", factor);
        $("#zoom-slider > a").text(factor + "%");
        // console.log(myDiagram.minScale);
        // console.log(myDiagram.maxScale);
        // console.log(myDiagram.scrollMode);
    });

    // generate unique label for valve on newly-created flow link
    // myDiagram.addDiagramListener("LinkDrawn", function (e) {
    //     var link = e.subject;
    //     if (link.category === "line") {
    //         myDiagram.startTransaction("updateNode");
    //         var newNodeId = "v" + getNodeCount();
    //         var labelNode = link.labelNodes.first();
    //         myDiagram.model.setDataProperty(labelNode.data, "label", newNodeId);
    //         myDiagram.commitTransaction("updateNode");
    //     }
    // });
}

function configGroup() {
    var $$ = go.GraphObject.make; // for conciseness in defining templates

    // 允许分组(Ctrl+G)
    myDiagram.commandHandler.archetypeGroupData = {
        text: "Group",
        isGroup: true,
        color: "blue"
    };
    // 允许取消分组(Ctrl+Shift+G)
    myDiagram.groupTemplate.ungroupable = true;
    // 分组的外观效果模板
    myDiagram.groupTemplate = $$(
        go.Group,
        "Auto",
        // enable Ctrl-Shift-G to ungroup a selected Group
        { selectionObjectName: "PANEL", ungroupable: true },
        $$(
            go.Shape,
            "RoundedRectangle", // surrounds everything
            {
                fill: "rgba(128,128,128,0.2)",
                stroke: "skyblue",
                strokeWidth: 3,
                portId: "",
                // cursor: "pointer", // the Shape is the port, not the whole Node
                // allow all kinds of links from and to this port
                fromLinkable: false,
                toLinkable: false
            }
        ),
        $$(
            go.Panel,
            "Vertical", // position header above the subgraph
            { defaultAlignment: go.Spot.Left },
            $$(
                go.Panel,
                "Horizontal", // the header
                { defaultAlignment: go.Spot.Top },
                $$("SubGraphExpanderButton"), // this Panel acts as a Button
                $$(
                    go.TextBlock, // group title near top, next to button
                    {
                        font: makeFont(graph_config.fontsize, true),
                        isMultiline: true,
                        editable: false
                    },
                    new go.Binding("text", "text").makeTwoWay()
                )
            ),
            $$(go.Placeholder, { margin: 5, background: "transparent" })
        )
    );
}

function initMinimap() {
    var $$ = go.GraphObject.make; // for conciseness in defining templates

    // tell it which Diagram to show and pan
    myMinimap = $$(go.Overview, minimap_div, {
        observed: myDiagram,
        maxScale: 0.5,
        contentAlignment: go.Spot.Center
    });
    // change color of viewport border in Overview
    myMinimap.box.elt(0).stroke = "dodgerblue";
}

function initDiagram() {
    // if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
    var $$ = go.GraphObject.make; // for conciseness in defining templates
    bluegrad = $$(go.Brush, "Linear", { 0: "#C4ECFF", 1: "#70D4FF" });
    greengrad = $$(go.Brush, "Linear", { 0: "#B1E2A5", 1: "#7AE060" });

    myDiagram = $$(go.Diagram, diagram_div, {
        // start everything in the middle of the viewport
        initialContentAlignment: go.Spot.Center,

        // allowZoom: false,
        // allowVerticalScroll: false,
        // allowHorizontalScroll: false,

        "commandHandler.zoomFactor": 1.2,

        // 鼠标滚轮缩放视图
        "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,

        // file:///D:/develop/GitTest/GoJS/samples/scrollModes.html
        scrollMode: go.Diagram.InfiniteScroll,

        padding: 0, // scales should be allowed right up against the edges of the viewport

        /* undo/redo开关 */
        "undoManager.isEnabled": true,

        /* 动画效果开关 */
        "animationManager.isEnabled": true,

        // so that the contents and the grid cannot appear too small
        //  参考: D:/develop/GitTest/GoJS/intro/viewport.html#ScrollingModes
        //        D:/develop/GitTest/GoJS/samples/scrollModes.html
        minScale: 0.1,
        maxScale: 5,
        /* 网格设置 */
        "grid.visible": true,
        //其它网格参数
        // "grid.gridCellSize": new go.Size(30, 20),
        grid: $$(
            go.Panel,
            "Grid",
            $$(go.Shape, "LineH", { stroke: "gray", strokeWidth: 0.5, interval: 10 }),
            // $$(go.Shape, "LineH", { stroke: "darkslategray", strokeWidth: 1, interval: 30 }),
            $$(go.Shape, "LineV", { stroke: "gray", strokeWidth: 0.5, interval: 10 })
            // $$(go.Shape, "LineV", { stroke: "darkslategray", strokeWidth: 1, interval: 30 })
        ),
        "draggingTool.isGridSnapEnabled": true
        // "resizingTool.isGridSnapEnabled": true,

        /* 深度拷贝和删除 */
        //允许使用ctrl+c、ctrl+v复制粘贴(深度copy)
        // "commandHandler.copiesTree": true,
        //允许使用delete键删除节点(深度delete)
        // "commandHandler.deletesTree": true,
        // dragging for both move and copy(深度drag)
        // "draggingTool.dragsTree": true,

        /* 最多选择节点个数 */
        // maxSelectionCount: 2
    });

    /* 自定义节点和分支模板 */
    configTemplates();

    // 创建图模型对象

    // 构造model对象,并进行必要的设置
    resetNetwork();
    // myDiagram.model = new go.GraphLinksModel([], []);
    
    // 创建缩略图
    initMinimap();

    // 创建标尺(还不完善!!!)
    // initRuler();

    /* 自定义配置 */
    configTools();
    configCommands();
    configGroup();

    /* 设置事件监听器 */
    configListener();

    /* 设置为鼠标指针模式 */
    setButtonMode("pointer");
}

function adjustCanvasHeight() {
    // [废除]删除canvas上的tabindex属性(未知bug: 点击图表区域,会自动修改top和left值,将页面整体上移,导致菜单栏消失)
    // [更新]必须有tabindex属性，否则canvas得不到键盘消息!
    // $("#" + diagram_div + " > canvas").removeAttr("tabindex");

    // canvas不设置焦点
    // https://blog.csdn.net/tom0008668/article/details/64904752
    $("#" + diagram_div + " > canvas").attr("tabindex", '-1');
    $("#" + minimap_div + " > canvas").attr("tabindex", '-2');
    
    // <div id='graph-container'>下都是absolute元素,因此必须修改position属性,否则div的高度撑不起来
    $("#" + diagram_div + " > canvas").css("position", "static");
    // 人工计算canvas的高度 = 文档高度 - 菜单栏高度 - 工具栏高度
    var height = $(document.body).height() - $(".main-header").height() - 42;
    // $("#" + diagram_div + " > canvas").css("height", height + "px");
    $("#" + diagram_div).css("height", height + "px");

    // 窗口大小改变触发resize事件!
    window.onresize = adjustCanvasHeight;
}

function reverseLink() {
    var datas = getSelectedLinks();
    if (datas == null) {
        layer.alert("请至少选择一条分支!", {
            skin: "layui-layer-moon"
        });
    } else {
        reverseLinks(datas);
    }
}

function reverseAllLinks() {
    // 选择所有分支
    var datas = getAllLinks();
    if (datas == null) {
        layer.alert("总分支个数为0!", {
            skin: "layui-layer-moon"
        });
    } else {
        reverseLinks(datas);
    }
}

function setButtonMode(type) {
    if (type == "pointer") {
        setMode("pointer", "pointer");
        $("i[data-command='pointer']").css("background-color", "#00ffff");
        $("i[data-command='drawNode']").css("background-color", "transparent");
        $("i[data-command='drawLink']").css("background-color", "transparent");
    } else if (type == "node") {
        setMode("node", "node");
        $("i[data-command='pointer']").css("background-color", "transparent");
        $("i[data-command='drawNode']").css("background-color", "#00ffff");
        $("i[data-command='drawLink']").css("background-color", "transparent");
    } else if (type == "link") {
        setMode("link", "line");
        $("i[data-command='pointer']").css("background-color", "transparent");
        $("i[data-command='drawNode']").css("background-color", "transparent");
        $("i[data-command='drawLink']").css("background-color", "#00ffff");
    }
}

function setSelectedNodeData(key, value) {
    var data = getFirstSelectedData(myDiagram.selection, "Node");
    if (data == 0) {
        layer.alert("请选择1个节点进行编辑!", { skin: "layui-layer-moon" });
        return false;
    } else {
        myDiagram.startTransaction("setData");
        myDiagram.model.setDataProperty(data, key, value);
        myDiagram.commitTransaction("setData");
        return true;
    }
}

function setSelectedLinkData(key, value) {
    var data = getFirstSelectedData(myDiagram.selection, "Link");
    if (data == 0) {
        layer.alert("请选择1条分支进行编辑!", { skin: "layui-layer-moon" });
        return false;
    } else {
        myDiagram.startTransaction("setData");
        myDiagram.model.setDataProperty(data, key, value);
        myDiagram.commitTransaction("setData");
        return true;
    }
}

// file:///D:/develop/GitTest/GoJS/samples/ruleredDiagram.html
function initRuler() {
    var $$ = go.GraphObject.make; // for conciseness in defining templates

    // Keep references to the scales and indicators to easily update them
    gradScaleHoriz = $$(
        go.Node,
        "Graduated",
        {
            graduatedTickUnit: 10,
            pickable: false,
            layerName: "Foreground",
            isInDocumentBounds: false,
            isAnimated: false
        },
        $$(go.Shape, { geometryString: "M0 0 H400" }),
        $$(go.Shape, { geometryString: "M0 0 V3", interval: 1 }),
        $$(go.Shape, { geometryString: "M0 0 V15", interval: 5 }),
        $$(go.TextBlock, {
            font: "10px sans-serif",
            interval: 5,
            alignmentFocus: go.Spot.TopLeft,
            segmentOffset: new go.Point(0, 7)
        })
    );

    gradScaleVert = $$(
        go.Node,
        "Graduated",
        {
            graduatedTickUnit: 10,
            pickable: false,
            layerName: "Foreground",
            isInDocumentBounds: false,
            isAnimated: false
        },
        $$(go.Shape, { geometryString: "M0 0 V400" }),
        $$(go.Shape, {
            geometryString: "M0 0 V3",
            interval: 1,
            alignmentFocus: go.Spot.Bottom
        }),
        $$(go.Shape, {
            geometryString: "M0 0 V15",
            interval: 5,
            alignmentFocus: go.Spot.Bottom
        }),
        $$(go.TextBlock, {
            font: "10px sans-serif",
            segmentOrientation: go.Link.OrientOpposite,
            interval: 5,
            alignmentFocus: go.Spot.BottomLeft,
            segmentOffset: new go.Point(0, -7)
        })
    );

    // These indicators are globally defined so they can be accessed by the div's mouseevents
    gradIndicatorHoriz = $$(
        go.Node,
        {
            pickable: false,
            layerName: "Foreground",
            visible: false,
            isInDocumentBounds: false,
            isAnimated: false,
            locationSpot: go.Spot.Top
        },
        $$(go.Shape, { geometryString: "M0 0 V15", strokeWidth: 2, stroke: "red" })
    );

    gradIndicatorVert = $$(
        go.Node,
        {
            pickable: false,
            layerName: "Foreground",
            visible: false,
            isInDocumentBounds: false,
            isAnimated: false,
            locationSpot: go.Spot.Left
        },
        $$(go.Shape, { geometryString: "M0 0 H15", strokeWidth: 2, stroke: "red" })
    );

    // Add listeners to keep the scales/indicators in sync with the viewport
    myDiagram.addDiagramListener("InitialLayoutCompleted", setupScalesAndIndicators);
    myDiagram.addDiagramListener("ViewportBoundsChanged", updateScales);
    myDiagram.addDiagramListener("ViewportBoundsChanged", updateIndicators);

    // Override mousemove Tools such that doMouseMove will keep indicators in sync
    myDiagram.toolManager.doMouseMove = function () {
        go.ToolManager.prototype.doMouseMove.call(this);
        updateIndicators();
    };
    myDiagram.toolManager.linkingTool.doMouseMove = function () {
        go.LinkingTool.prototype.doMouseMove.call(this);
        updateIndicators();
    };
    myDiagram.toolManager.draggingTool.doMouseMove = function () {
        go.DraggingTool.prototype.doMouseMove.call(this);
        updateIndicators();
    };
    myDiagram.toolManager.dragSelectingTool.doMouseMove = function () {
        go.DragSelectingTool.prototype.doMouseMove.call(this);
        updateIndicators();
    };

    // 绑定事件onmouseover和onmouseout事件
    $("#" + diagram_div).mouseover(function () { showIndicators(); });
    $("#" + diagram_div).mouseout(function () { hideIndicators(); });
}

function setupScalesAndIndicators() {
    var vb = myDiagram.viewportBounds;
    myDiagram.startTransaction("add scales");
    updateScales();
    // Add each node to the diagram
    myDiagram.add(gradScaleHoriz);
    myDiagram.add(gradScaleVert);
    myDiagram.add(gradIndicatorHoriz);
    myDiagram.add(gradIndicatorVert);
    myDiagram.commitTransaction("add scales");
}

function updateScales() {
    var vb = myDiagram.viewportBounds;
    myDiagram.startTransaction("update scales");
    // Update properties of horizontal scale to reflect viewport
    gradScaleHoriz.location = new go.Point(vb.x, vb.y);
    gradScaleHoriz.graduatedMin = vb.x;
    gradScaleHoriz.graduatedMax = vb.right;
    gradScaleHoriz.scale = 1 / myDiagram.scale;
    // Update properties of vertical scale to reflect viewport
    gradScaleVert.location = new go.Point(vb.x, vb.y);
    gradScaleVert.graduatedMin = vb.y;
    gradScaleVert.graduatedMax = vb.bottom;
    gradScaleVert.scale = 1 / myDiagram.scale;
    myDiagram.commitTransaction("update scales");
}

function updateIndicators() {
    var vb = myDiagram.viewportBounds;
    var mouseCoords = myDiagram.lastInput.documentPoint;
    myDiagram.startTransaction("update indicators");
    // Keep the indicators in line with the mouse as viewport changes or mouse moves
    gradIndicatorHoriz.location = new go.Point(
        Math.max(mouseCoords.x, vb.x),
        vb.y
    );
    gradIndicatorHoriz.scale = 1 / myDiagram.scale;
    gradIndicatorVert.location = new go.Point(
        vb.x,
        Math.max(mouseCoords.y, vb.y)
    );
    gradIndicatorVert.scale = 1 / myDiagram.scale;
    myDiagram.commitTransaction("update indicators");
}

// Show indicators on mouseover of the diagram div
function showIndicators() {
    myDiagram.startTransaction("show indicators");
    gradIndicatorHoriz.visible = true;
    gradIndicatorVert.visible = true;
    myDiagram.commitTransaction("show indicators");
}

// Hide indicators on mouseout of the diagram div
function hideIndicators() {
    myDiagram.startTransaction("hide indicators");
    gradIndicatorHoriz.visible = false;
    gradIndicatorVert.visible = false;
    myDiagram.commitTransaction("hide indicators");
}

// file:///D:/develop/GitTest/GoJS/api/symbols/CommandHandler.html
function initEvent() {
    //全屏事件
    $(document).on("click", "[data-toggle='fullscreen']", function () {
        var doc = document.documentElement;
        if ($(document.body).hasClass("full-screen")) {
            $(document.body).removeClass("full-screen");
            document.exitFullscreen
                ? document.exitFullscreen()
                : document.mozCancelFullScreen
                    ? document.mozCancelFullScreen()
                    : document.webkitExitFullscreen && document.webkitExitFullscreen();
        } else {
            $(document.body).addClass("full-screen");
            doc.requestFullscreen
                ? doc.requestFullscreen()
                : doc.mozRequestFullScreen
                    ? doc.mozRequestFullScreen()
                    : doc.webkitRequestFullscreen
                        ? doc.webkitRequestFullscreen()
                        : doc.msRequestFullscreen && doc.msRequestFullscreen();
        }
    });

    $(document).on("click", "#new_graph", function () {
        layer.confirm(
            "确定要新建一个空的通风网络图???",
            { icon: 3, title: "询问", skin: "layui-layer-moon" },
            function (index) {
                // 清空网络(不可逆操作!!!)
                resetNetwork();
                // 更新网络图数据界面
                updateGraphDataForm(graph_config);
                // 关闭对话框
                layer.close(index);
            }
        );
    });
    $(document).on("click", "#open_graph", function () {
        openGraph();
    });
    $(document).on("click", "#save_graph", function () {
        exportJson(graph_config.graph_name, "json");
    });

    $(document).on("click", "#ajax_test", function () {
        ajax_test();
    });
    $(document).on("click", "#simple_test", function () {
        simple_test();
    });
    $(document).on("click", "#rand_test", function () {
        rand_test();
    });

    $(document).on("click", "#line_link", function () {
        changeLinkShape("line");
    });
    $(document).on("click", "#arc_link", function () {
        changeLinkShape("arc");
    });
    $(document).on("click", "#spline_link", function () {
        changeLinkShape("spline");
    });

    $(document).on("click", "#reverse_link", function () {
        reverseLink();
    });

    $(document).on("click", "#reverse_all_links", function () {
        reverseAllLinks();
    });

    $(document).on("click", "#zoom_in", function () {
        myDiagram.commandHandler.increaseZoom();
    });
    $(document).on("click", "#zoom_out", function () {
        myDiagram.commandHandler.decreaseZoom();
    });
    $(document).on("click", "#zoom_fit", function () {
        // 图形整体居中
        myDiagram.centerRect(myDiagram.documentBounds);
        // 缩放到1.0比例尺
        myDiagram.commandHandler.zoomToFit();
        // 计算新的比例
        var db = myDiagram.documentBounds;
        var vb = myDiagram.viewportBounds;
        var scale = Math.min(vb.width / db.width, vb.height / db.height);
        if (isNaN(scale) || scale == Infinity || scale == -Infinity) {
            scale = 1.0;
        }
        myDiagram.commandHandler.resetZoom(scale);
    });

    $(document).on("click", "#undo", function () {
        myDiagram.undoManager.undo();
    });
    $(document).on("click", "#redo", function () {
        myDiagram.undoManager.redo();
    });
    $(document).on("click", "#addGroup", function () {
        myDiagram.commandHandler.groupSelection();
    });
    $(document).on("click", "#unGroup", function () {
        myDiagram.commandHandler.ungroupSelection();
    });
    //view-source:file:///D:/develop/GitTest/GoJS/extensions/BPMN.html#
    $(document).on("click", "#copy", function () {
        myDiagram.commandHandler.copySelection();
    });
    $(document).on("click", "#cut", function () {
        myDiagram.commandHandler.cutSelection();
    });
    $(document).on("click", "#paste", function () {
        myDiagram.commandHandler.pasteSelection();
    });
    $(document).on("click", "#delete", function () {
        if (myDiagram.selection.count == 0) {
            layer.alert("请至少选择一个图形!", { skin: "layui-layer-moon" });
            return;
        }
        myDiagram.commandHandler.deleteSelection();
    });
    $(document).on("click", "#clear_all", function () {
        layer.confirm(
            "确定清空所有节点和分支吗?<pre><b>[警告]: 该操作不可撤销!</b></pre>",
            { icon: 3, title: "提示", skin: "layui-layer-moon" },
            function (index) {
                resetNetwork();
                layer.close(index);
            }
        );
    });
    $(document).on("click", "#select_all", function () {
        myDiagram.commandHandler.selectAll();
    });
    // $(document).on("click", "#x-align", function () {
    //     myDiagram.commandHandler.alignCenterX();
    // });
    // $(document).on("click", "#y-align", function () {
    //     myDiagram.commandHandler.alignCenterY();
    // });
    $(document).on("click", "#prop_panel", function () {
        if ($("body").hasClass("control-sidebar-open")) {
            $("body").removeClass("control-sidebar-open");
        } else {
            $("body").addClass("control-sidebar-open");
        }
    });
    $(document).on("click", "#grid", function () {
        myDiagram.startTransaction("grid");
        myDiagram.grid.visible = !myDiagram.grid.visible;
        myDiagram.commitTransaction("grid");
    });
    $(document).on("click", "#import_graph", function () {
        importGraph();
    });
    $(document).on("click", "#export_graph", function () {
        exportGraph();
    });
    $(document).on("click", "#graph_config", function () {
        configGraph();
    });
    $(document).on("click", "#about", function () {
        layer.alert(
            "<h4>欢迎使用矿井通风网络图在线设计工具，希望能为您提供帮助!</h4>",
            { skin: "layui-layer-moon" }
        );
    });
    $(document).on("click", "#help", function () {
        layer.alert(
            "<h4>欢迎使用矿井通风网络图在线设计工具，希望能为您提供帮助!</h4>",
            { skin: "layui-layer-moon" }
        );
    });
    // 工具栏按钮事件
    $("#toolbar").on("click", "i[data-command='undo']", function () {
        $("#undo").click();
    });
    $("#toolbar").on("click", "i[data-command='redo']", function () {
        $("#redo").click();
    });
    $("#toolbar").on("click", "i[data-command='copy']", function () {
        $("#copy").click();
    });
    // $("#toolbar").on("click", "i[data-command='cut']", function () {
    //     myDiagram.commandHandler.cutSelection();
    // });
    $("#toolbar").on("click", "i[data-command='paste']", function () {
        $("#paste").click();
    });
    $("#toolbar").on("click", "i[data-command='delete']", function () {
        $("#delete").click();
    });
    $("#toolbar").on("click", "i[data-command='zoomIn']", function () {
        $("#zoom_in").click();
    });
    $("#toolbar").on("click", "i[data-command='zoomOut']", function () {
        $("#zoom_out").click();
    });
    $("#toolbar").on("click", "i[data-command='autoZoom']", function () {
        $("#zoom_fit").click();
    });

    $("#toolbar").on("click", "i[data-command='pointer']", function () {
        setButtonMode("pointer");
    });
    $("#toolbar").on("click", "i[data-command='drawNode']", function () {
        setButtonMode("node");
    });
    $("#toolbar").on("click", "i[data-command='drawLink']", function () {
        setButtonMode("link");
    });
    $("#toolbar").on("click", "i[data-command='reverseLink']", function () {
        $("#reverse_link").click();
    });
    $("#toolbar").on("click", "i[data-command='addGroup']", function () {
        $("#addGroup").click();
    });
    $("#toolbar").on("click", "i[data-command='unGroup']", function () {
        $("#unGroup").click();
    });
    $("#toolbar").on("click", "i[data-command='config']", function () {
        $("#graph_config").click();
    });
    $("#toolbar").on("click", "i[data-command='unGroup']", function () {
        $("#unGroup").click();
    });
    $("#toolbar").on("click", "i[data-command='import']", function () {
        $("#import_graph").click();
    });
    $("#toolbar").on("click", "i[data-command='export']", function () {
        $("#export_graph").click();
    });
    $("#toolbar").on("click", "i[data-command='new']", function () {
        $("#new_graph").click();
    });
    $("#toolbar").on("click", "i[data-command='open']", function () {
        $("#open_graph").click();
    });
    $("#toolbar").on("click", "i[data-command='save']", function () {
        $("#save_graph").click();
    });

    // 绑定网络图数据
    $("#graph-data-form #graph_name").on("input", function (e) {
        //获取input输入的值
        graph_config.graph_name = e.delegateTarget.value;
    });
    $("#graph-data-form #company").on("input", function (e) {
        //获取input输入的值
        graph_config.company = e.delegateTarget.value;
    });
    $("#graph-data-form #author").on("input", function (e) {
        //获取input输入的值
        graph_config.author = e.delegateTarget.value;
    });
    $("#graph-data-form #node_width").on("input", function (e) {
        //获取input输入的值
        graph_config.node_width = parseFloat(e.delegateTarget.value);
        // 强制更新所有节点的宽度
        myDiagram.model.nodeDataArray.forEach(function (data) {
            myDiagram.model.setDataProperty(data, "width", graph_config.node_width);
        });
    });
    $("#graph-data-form #node_height").on("input", function (e) {
        //获取input输入的值
        graph_config.node_height = parseFloat(e.delegateTarget.value);
        // 强制更新所有节点的高度
        myDiagram.model.nodeDataArray.forEach(function (data) {
            myDiagram.model.setDataProperty(data, "height", graph_config.node_height);
        });
    });
    $("#graph-data-form #fontsize").on("input", function (e) {
        //获取input输入的值
        graph_config.fontsize = parseInt(e.delegateTarget.value);

        // 强制更新所有节点的文字大小
        var font = makeFont(graph_config.fontsize, false);
        myDiagram.model.nodeDataArray.forEach(function (data) {
            myDiagram.model.setDataProperty(data, "font", font);
        });
        // 强制更新所有分支的文字大小
        myDiagram.model.linkDataArray.forEach(function (data) {
            myDiagram.model.setDataProperty(data, "font", font);
        });
    });

    $(".form_date")
        .datetimepicker({
            language: "zh-CN",
            format: "yyyy年mm月dd日",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        })
        .on("changeDate", function (e) {
            // console.log(e);
            graph_config.drawing_date = $("#graph-data-form #drawing_date").val();
            // graph_config.drawing_date = e.delegateTarget.value;
        });

    /* 绑定节点数据面板的相关事件 */
    $("#node-data-form #node_key").on("input", function (e) {
        //获取input输入的值
        setSelectedNodeData("key", e.delegateTarget.value);
    });
    $("#node-data-form #node_id").on("input", function (e) {
        //获取input输入的值
        setSelectedNodeData("id", e.delegateTarget.value);
        // 动态修改节点名称
        setValue("node-data-form", "node_name", "v" + e.delegateTarget.value);
        // 触发input事件
        $("#node-data-form #node_name").trigger("input");
    });
    $("#node-data-form #node_name").on("input", function (e) {
        //获取input输入的值
        setSelectedNodeData("text", e.delegateTarget.value);
    });
    $("#node-data-form #node_loc").on("input", function (e) {
        //获取input输入的值
        var pt = go.Point.parse(e.delegateTarget.value);
        pt.y *= -1;
        setSelectedNodeData("loc", go.Point.stringify(pt));
    });
    $("#node-data-form #node_width").on("input", function (e) {
        setSelectedNodeData("width", parseFloat(e.delegateTarget.value));
    });
    $("#node-data-form #node_height").on("input", function (e) {
        setSelectedNodeData("height", parseFloat(e.delegateTarget.value));
    });
    $("#node-data-form #fontsize").on("input", function (e) {
        //获取input输入的值
        var fontsize = parseInt(e.delegateTarget.value);
        if (isNaN(fontsize)) {
            layer.alert("请输入有效的字体大小(数字)!", { skin: "layui-layer-moon" });
        } else {
            setSelectedNodeData("font", makeFont(fontsize, false));
        }
    });
    $("#node-data-form #node_p").on("input", function (e) {
        //获取input输入的值
        setSelectedNodeData("p", parseFloat(e.delegateTarget.value));
    });
    $("#node-data-form #node_z").on("input", function (e) {
        //获取input输入的值
        setSelectedNodeData("z", parseFloat(e.delegateTarget.value));
    });

    /* 绑定分支数据面板的相关事件 */
    $("#edge-data-form #edge_key").on("input", function (e) {
        //获取input输入的值
        setSelectedLinkData("key", e.delegateTarget.value);
    });
    $("#edge-data-form #edge_id").on("input", function (e) {
        //获取input输入的值
        setSelectedLinkData("id", e.delegateTarget.value);
        // 动态修改分支名称
        setValue("edge-data-form", "edge_name", "e" + e.delegateTarget.value);
        // 触发input事件
        $("#edge-data-form #edge_name").trigger("input");
    });
    $("#edge-data-form #edge_name").on("input", function (e) {
        //获取input输入的值
        setSelectedLinkData("text", e.delegateTarget.value);
    });

    $("#edge-data-form #edge_category").on("change", function () {
        var value = $(this)
            .children("option:selected")
            .val(); //这就是selected的值
        // console.log(value);
        if (setSelectedLinkData("category", value)) {
            updatelinkReshapingTool(value);
            // 更新图形
            myDiagram.rebuildParts();
        }
    });
    $("#edge-data-form #fontsize").on("input", function (e) {
        //获取input输入的值
        var fontsize = parseInt(e.delegateTarget.value);
        if (isNaN(fontsize)) {
            layer.alert("请输入有效的字体大小(数字)!", { skin: "layui-layer-moon" });
        } else {
            setSelectedLinkData("font", makeFont(fontsize, false));
        }
    });
    $("#edge-data-form #edge_r").on("input", function (e) {
        //获取input输入的值
        setSelectedLinkData("r", parseFloat(e.delegateTarget.value));
    });
    $("#edge-data-form #edge_q").on("input", function (e) {
        //获取input输入的值
        setSelectedLinkData("q", parseFloat(e.delegateTarget.value));
    });
    $("#edge-data-form #edge_s").on("input", function (e) {
        //获取input输入的值
        setSelectedLinkData("s", parseFloat(e.delegateTarget.value));
    });

    // 首先更新网络图表单界面!
    updateGraphDataForm(graph_config);
}

function addNode(id, name, x, y) {
    var node_data = {
        key: randomUUID(),
        id: id,
        text: name,
        loc: go.Point.stringify(new go.Point(x, -1 * y)), // the "loc" property is a string, not a Point object
        // color: go.Brush.randomColor(128, 240)
        p: 0.0,
        z: 0.0,
        width: graph_config.node_width,
        height: graph_config.node_height,
        font: makeFont(graph_config.fontsize, false)
    };
    // add the new node data to the model
    var model = myDiagram.model;
    model.addNodeData(node_data);
    return node_data.key;
}

function makeLink(key, id, from, to, name, category, points) {
    // create a link data from the old node data to the new node data
    var link_data = {
        key: (key=='' || key==undefined || key==null) ? randomUUID() : key,
        id: id,
        // from: model.getKeyForNodeData(fromData),  // or just: fromData.id
        from: from,
        to: to, // s和t表示key
        text: name,
        category: category,
        curviness: 0,
        // line_points: JSON.stringify(buildLinePoints(from, to)),
        line_points: "[]",
        spline_points: "[]",
        ortho: false, // 是否正交直线
        r: 0.0,
        q: 0.0,
        s: 0.0,
        font: makeFont(graph_config.fontsize, false)
    };
    // gojs坐标系的y轴是朝下的
    points.forEach(function(val) { 
        val.y *= -1; 
    });

    // 样条曲线特殊处理
    if(category == 'spline' && points.length > 2) {
        link_data.spline_points = JSON.stringify(points);
    }
    else {
        link_data.line_points = JSON.stringify(points);
        // 正交线段是否合并
        link_data.ortho = (graph_config.splines == 'ortho2');
        // link_data.corner = 5;
    }

    return link_data;
}

function addLink(id, from, to, name, category, points) {
    // 生成分支数据
    var link_data = makeLink(null, id, from, to, name, category, points);
    // and add the link data to the model
    var model = myDiagram.model;
    model.addLinkData(link_data);
    return link_data.key;
}

function getLastNode() {
    var nodes = myDiagram.model.nodeDataArray;
    return myDiagram.findNodeForData(nodes[nodes.length - 1]);
}

function fitToNode(node) {
    if (node != null) {
        myDiagram.select(node);
        // if the new node is off-screen, scroll the diagram to show the new node
        // myDiagram.scrollToRect(node.actualBounds);
    }
}

// 该函数不能在事务中调用!!!
function resetNetwork() {
    myDiagram.model = new go.GraphLinksModel([], []);
    // 要保证findLinkDataForKey()有效, 必须设置linkKeyProperty
    // 参考: https://gojs.net/latest/api/symbols/GraphLinksModel.html
    myDiagram.model.linkKeyProperty = 'key';
}

function importNetworkCallBack(data) {
    resetNetwork();
    if (data.code == 1) {
        layer.alert("导入失败", { skin: "layui-layer-moon" });
        // myDiagram.rollbackTransaction("importNetworkCallBack");
        return;
    }
    myDiagram.startTransaction("importNetworkCallBack");

    // console.log(data);
    nodes_map = {};
    for (var i in data.nodes) {
        var node = data.nodes[i];
        // console.log(node);
        nodes_map[node.id] = addNode(node.id, "v" + node.id, node.x, node.y);
    }
    for (var i in data.edges) {
        var edge = data.edges[i];
        // console.log(edge);
        var ctrl_points = edge.ctrl_points;

        // 处理特殊情况
        if(ctrl_points == null || ctrl_points == undefined) {
            ctrl_points = [];
        }
        var from = nodes_map[edge.s];
        var to = nodes_map[edge.t];
        if(ctrl_points.length > 2 && graph_config.splines == 'spline') {
            // 添加曲线分支
            addLink(edge.id, from, to, "e" + edge.id, 'spline', ctrl_points);    
        } else {
            // 添加直线分支
            addLink(edge.id, from, to, "e" + edge.id, 'line', ctrl_points);
        }
    }

    myDiagram.commitTransaction("importNetworkCallBack");
}

function updateNetworkCallBack(data) {
    if (data.code == 1) {
        layer.alert("重新布局失败", { skin: "layui-layer-moon" });
        return;
    }
    myDiagram.startTransaction("updateNetworkCallBack");

    // console.log(data);
    for (var i in data.nodes) {
        var node = data.nodes[i];
        // console.log(node);
        var node_data = myDiagram.model.findNodeDataForKey(node.id);
        if (node_data == null) continue;

        myDiagram.model.setDataProperty(
            node_data,
            "loc",
            go.Point.stringify(new go.Point(node.x, -1*node.y))
        );
    }

    for (var i in data.edges) {
        var edge = data.edges[i];
        // console.log(edge);
        // 查找分支对象
        // 使用findLinkDataForKey()方法必须保证linkKeyProperty已设置!!!
        var edge_data = myDiagram.model.findLinkDataForKey(edge.id);
        // console.log(edge_data);
        if(edge_data == null) continue;

        var ctrl_points = edge.ctrl_points;
        // 处理特殊情况
        if(ctrl_points == null || ctrl_points == undefined) {
            ctrl_points = [];
        }
        var link_data = {};
        if(ctrl_points.length > 2 && graph_config.splines == 'spline') {
            // 生成曲线分支数据
            link_data = makeLink(edge_data.key, edge_data.id, edge_data.from, edge_data.to, "e" + edge_data.id, 'spline', ctrl_points);    
        } else {
            // 生成直线分支数据
            link_data = makeLink(edge_data.key, edge_data.id, edge_data.from, edge_data.to, "e" + edge_data.id, 'line', ctrl_points);            
        }
        // console.log(link_data);
        // 更新分支的坐标和形状数据
        for(var key in link_data) {
            if(key == 'category' || 
               key == 'line_points' || key == 'spline_points' || 
               key == 'ortho') {
                myDiagram.model.setDataProperty(edge_data, key, link_data[key]);
            }
        }
    }

    myDiagram.commitTransaction("updateNetworkCallBack");
}

function simple_test() {
    var nodes = [
        { id: 1, x: 10, y: 0 },
        { id: 2, x: 25, y: 50 },
        { id: 3, x: 75, y: 50 },
        { id: 4, x: 75, y: 100 },
        { id: 5, x: 100, y: 200 }
    ];
    // create an array with edges
    var edges = [
        { id: 1, s: 1, t: 3 },
        { id: 2, s: 1, t: 2 },
        { id: 3, s: 2, t: 4 },
        { id: 4, s: 2, t: 5 },
        { id: 5, s: 3, t: 5 }
    ];
    importNetworkCallBack({ code: 0, nodes: nodes, edges: edges });
}

// 使用回调解决ajax异步return得不到数据的问题
// https://www.imooc.com/article/17988
// https://www.cnblogs.com/terryMe/p/6130309.html
function ajaxVng(callback) {
    $.ajax({
        url: "index/index/ajaxVng", //这里指向的就不再是页面了，而是一个方法。
        data: {},
        type: "post",
        dataType: "json",
        success: function (data) {
            // console.log(data);
            // result_data = JSON.parse(data);
            callback(data);
        },
        error: function () {
            callback(null);
        }
    });
}

function ajax_test() {
    // ajax异步调用
    ajaxVng(importNetworkCallBack);
}

function rand_test() {
    resetNetwork();

    myDiagram.startTransaction("rand_test");

    nodes_map = {};
    var num = 10;
    for (var i = 0; i < num; i++) {
        var x = Math.floor(Math.random() * 300);
        var y = Math.floor(Math.random() * 300);
        if (y == x) y = x + 300;
        nodes_map[i] = addNode(i, "v" + i, x, y);
    }
    for (var i = 0; i < num; i++) {
        var a = Math.floor(Math.random() * num);
        var b = Math.floor((Math.random() * num) / 4) + 1;
        var from = nodes_map[a];
        var to = nodes_map[(a + b) % num];
        addLink(i, from, to, "e" + i, 'line', []);
    }
    myDiagram.commitTransaction("rand_test");

    // if the new node is off-screen, scroll the myDiagram to show the new node
    fitToNode(getLastNode());

    // Show the myDiagram's model in JSON format
    // document.getElementById("mySavedModel").value = myDiagram.model.toJson();
    // myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
}

function partIsLink(part) {
    return part.type == go.Panel.Link;
}

function partIsNode(part) {
    return part.type == go.Panel.Node || part.type == go.Panel.Spot;
}

function getFirstSelectedPart(parts) {
    if (parts == null || parts.count == 0) return null;
    if (parts.count > 1) return null;
    return parts.first();
}

function getFirstSelectedData(parts, type) {
    var part = getFirstSelectedPart(parts);
    if (part == null) return null;

    if (type == "Link" && partIsLink(part)) {
        return part.data;
    } else if (type == "Node" && partIsNode(part)) {
        return part.data;
    } else {
        return null;
    }
}

function getSelectedDatas(parts, type) {
    if (parts == null || parts.count == 0) {
        return null;
    }
    var datas = [];
    var i = 0;
    parts.each(function (part) {
        if (type == "Link" && partIsLink(part)) {
            datas[i] = part.data;
            i = i + 1;
        } else if (type == "Node" && partIsNode(part)) {
            datas[i] = part.data;
            i = i + 1;
        } else if (type == "All") {
            datas[i] = part.data;
            i = i + 1;
        } else {
            part.isSelect = false;
        }
    });
    if (i == 0) {
        return null;
    } else {
        return datas;
    }
}

function getSelectedLink() {
    var data = getFirstSelectedData(myDiagram.selection, "Link");
    if (data == 0) {
        layer.alert("请选择一条分支", { skin: "layui-layer-moon" });
        return null;
    }
    return data;
}

function getSelectedLinks() {
    var datas = getSelectedDatas(myDiagram.selection, "Link");
    if (datas == null || datas.length == 0) {
        layer.alert("请至少选择一条分支", { skin: "layui-layer-moon" });
        return null;
    } else {
        return datas;
    }
}

function getAllLinks() {
    var parts = myDiagram.links;
    if (parts == null || parts.count == 0) {
        return null;
    }
    var datas = [];
    var i = 0;
    parts.each(function (part) {
        if (partIsLink(part)) {
            datas[i] = part.data;
            i = i + 1;
        }
    });
    return datas;
}

function reverseLinks(datas) {
    myDiagram.startTransaction("reverse_link");
    // 遍历分支集合
    datas.forEach(function (data) {
        // console.log(data);
        var from = data.from;
        var to = data.to;
        myDiagram.model.setFromKeyForLinkData(data, to);
        myDiagram.model.setToKeyForLinkData(data, from);
    });
    myDiagram.commitTransaction("reverse_link");
}

function changeLinkShape(type) {
    var datas = getSelectedLinks();
    if (datas == null) {
        layer.alert("请至少选择一条分支!", {
            skin: "layui-layer-moon"
        });
    } else {
        myDiagram.startTransaction("chaneLinksType");
        // 遍历分支集合
        datas.forEach(function (data) {
            // console.log(data);
            var e = myDiagram.findLinkForData(data);
            if (e != null) myDiagram.model.setCategoryForLinkData(e, type);
        });
        myDiagram.commitTransaction("chaneLinksType");

        if (type == "spline") {
            myDiagram.toolManager.linkReshapingTool = spline_tool;
        } else if (type == "arc") {
            myDiagram.toolManager.linkReshapingTool = arc_tool;
        } else {
            myDiagram.toolManager.linkReshapingTool = null;
        }
        // myDiagram.rebuildParts();
    }
}

function buildGraphDatas(ready_export) {
    if (myDiagram.model.linkDataArray.length == 0) {
        return null;
    }

    var node_datas = [];
    // 遍历节点
    myDiagram.model.nodeDataArray.forEach(function (data) {
        // console.log(data);
        // var pt = go.Point.parse(data.loc);
        // 获取节点的几何对象
        var node = myDiagram.findNodeForData(data);
        // console.log(node);
        var rect = node.actualBounds;
        // 节点的几何信息
        var obj = {
            id: data.key,
            name: data.text,
            // width: rect.width,
            // height: rect.height,
            width: data.width,
            height: data.height
        };
        var fontsize = getFontHeight(data.font);
        if(fontsize <= 0) fontsize = graph_config.fontsize;
        obj["fontsize"] = fontsize;

        if(ready_export) {
            obj["x"] = rect.centerX;
            obj["y"] = -1 * rect.centerY;            
        }
        node_datas.push(obj);
    });

    var links = [];
    var link_datas = [];
    // 遍历分支
    myDiagram.model.linkDataArray.forEach(function (data) {
        // 分支和节点的拓扑信息
        // console.log(data);
        links.push({
            id: data.key,
            s: data.from,
            t: data.to
        });

        // 分支的几何信息
        var link = myDiagram.findLinkForData(data);
        var points = [];
        if(data.category == 'line') {
            points = JSON.parse(data.line_points);
        } else if(data.category == 'spline') {
            points = JSON.parse(data.spline_points);
        }
        points.forEach(function(val) {
            val.y *= -1;
        });
        // console.log(link.curviness);
        var tpt = link.midPoint;
        tpt.y *= -1;
        // console.log(tpt);
        var curviness = data.curviness;
        if (isNaN(curviness)) curviness = 0;

        var obj = {
            id: data.key,
            name: data.text
        };        
        var fontsize = getFontHeight(data.font);
        if(fontsize <= 0) fontsize = graph_config.fontsize;
        obj["fontsize"] = fontsize;
        
        if(ready_export) {
            obj["category"] = data.category;
            obj["curviness"] = curviness;
            obj["points"] = JSON.stringify(points);
            obj["midPoint"] = go.Point.stringify(tpt);
        }
        link_datas.push(obj);
    });
    return { edges: links, node_weights: node_datas, edge_weights: link_datas };
}

function closeDiv(id) {
    $("#" + id).css("display", "none");
}

function getValue(div, id) {
    return $("#" + div + " #" + id).val();
}

function setValue(div, id, value) {
    // return $("#" + div + " #" + id).attr("value", value);
    return $("#" + div + " #" + id).val(value);
}

function configGraph() {
    var index = layer.open({
        type: 1,
        title: "通风网络图自动布局",
        skin: "layui-layer-moon",
        scrollbar: false,
        area: ["360px", "430px"],
        shade: 0.618, // 不显示遮罩
        content: $("#graph-config-div"),
        btn: ["布局", "取消"],
        btnAlign: "c",
        // 对应第1个按钮
        yes: function (index, layero) {
            // 更新布局参数(存在同名id,必须用父级元素限定!)
            graph_config.nodesep = parseFloat(
                getValue("graph-config-div", "nodesep")
            );
            graph_config.ranksep = parseFloat(
                getValue("graph-config-div", "ranksep")
            );
            graph_config.rankdir = getValue("graph-config-div", "rankdir");
            graph_config.splines = getValue("graph-config-div", "splines");

            // 生成网络图数据并传入到服务器后台
            var graph = buildGraphDatas(false);
            if (graph == null) {
                layer.alert("生成通风网络数据失败!", { skin: "layui-layer-moon" });
                return;
            }
            var param = {
                graph: JSON.stringify(graph),
                nodesep: graph_config.nodesep,
                ranksep: graph_config.ranksep,
                rankdir: graph_config.rankdir,
                splines: graph_config.splines,
                node_width: graph_config.node_width,
                node_height: graph_config.node_height,
                fontsize: graph_config.fontsize,
                need_layout: 1 // 需要进行布局计算
            };
            
            // ortho1--表示正交分支不合并; ortho2--表示正交分支部分线段会合并
            if(param.splines == 'ortho1' || param.splines == 'ortho2') {
                param.splines = 'ortho';
            }
            // 更新重新布局后的图形
            ajaxVngFromGraph(param, updateNetworkCallBack);

            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("graph-config-div");
        },
        // 对应第2个按钮(取消)
        btn2: function (index, layero) {
            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("graph-config-div");
        },
        // 对应右上角关闭按钮
        cancel: function (index, layero) {
            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("graph-config-div");
        },
        success: function () {
            // 绑定ESC键
            $(document).on("keydown", function (e) {
                if (e.keyCode == 27) {
                    layer.close(index);
                    closeDiv("graph-config-div");
                }
            });
            // 将当前参数更新到界面上
            setValue("graph-config-div", "nodesep", graph_config.nodesep);
            setValue("graph-config-div", "ranksep", graph_config.ranksep);
            setValue("graph-config-div", "rankdir", graph_config.rankdir);
            setValue("graph-config-div", "splines", graph_config.splines);
        },
        end: function () {
            // do something
        }
    });
}

function ajaxVngFromGraph(param, callback) {
    $.ajax({
        url: "index/index/ajaxVngFromGraph", //这里指向的就不再是页面了，而是一个方法。
        data: param,
        type: "post",
        dataType: "json",
        success: function (data) {
            // console.log(data);
            // 如果php中返回的是json对象,那么这里就不要再进行解析了,否则报下面的错误
            // Uncaught SyntaxError: Unexpected token o in JSON at position 1
            // result_data = JSON.parse(data);
            // 直接就可以当作json数组来使用了
            if (data.need_layout == 1) {
                callback(data);
            } else {
                // 导入网络图模型数据(直接修改model,不能使用事务!!!)
                myDiagram.model = go.Model.fromJson(data["graph"]);
                // 导入全局配置参数
                var graph = JSON.parse(data["graph"]);
                // 覆盖全局配置对象
                var old_graph_config = graph["graph_config"];
                for(key in old_graph_config) {
                    // key存在的则覆盖!!!
                    if(graph_config[key]) {
                        graph_config[key] = old_graph_config[key]
                    }                    
                }
                // 更新网络图数据界面
                updateGraphDataForm(graph_config);
            }
        },
        error: function () {
            callback(null);
        }
    });
}

// 参考: https://blog.csdn.net/weixin_36571185/article/details/78700802
function renderUpload(_elem, _url, _graph_file_url, _file_type) {
    // 创建上传组件
    return upload.render({
        elem: _elem, // 绑定元素
        url: _url,
        accept: "file", //普通文件
        exts: _file_type, //支持上传txt、dxf和json
        auto: true, // 是否自动上传
        //如果auto=false,则指向一个按钮触发上传(btn0对应的是弹窗open函数btn参数中的第一个按钮)
        // bindAction: 'a.layui-layer-btn0',
        // 选择文件后的回调函数
        choose: function (obj) {
            console.log(
                "before与choose回调类似, auto=false调用choose回调, 反之调用before回调"
            );
            // 代码可以与choose回调一样
            this.before(obj);
        },
        // 文件提交上传前的回调
        before: function (obj) {
            // 将每次选择的文件追加到文件队列
            upload_files = obj.pushFile();
            // 预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
            obj.preview(function (index, file, result) {
                // console.log(index); //得到文件索引
                // console.log(file); //得到文件对象
                // console.log(result); //得到文件base64编码，比如图片
                // 这里还可以做一些 append 文件列表DOM的操作
            });
            // 显示加载动画
            loading_index = layer.load(0, { shade: false });
        },
        // 上传完成后的回调
        done: function (res, index, upload) {
            // 关闭加载动画
            layer.close(loading_index);

            // console.log('上传成功!')
            // 上传成功
            if (res.code == 0) {
                // 删除列表中对应的文件，一般在某个事件中使用
                delete upload_files[index];
                // 上传后服务器返回的json消息
                // console.log(res);
                // 记录数据文件的地址
                document.getElementById(_graph_file_url).value = res.data.url;
                layer.alert("上传成功，请点击【导入】按钮生成通风网络图!", {
                    skin: "layui-layer-moon"
                });
            } else {
                this.error(index, upload);
            }
            upload_files = null;
        },
        //请求异常回调
        error: function (index, upload) {
            layer.alert("上传失败，请重新上传!", { skin: "layui-layer-moon" });
        }
    });
}

function importGraph() {
    // 弹出对话框
    var index = layer.open({
        type: 1,
        title: "导入通风网络数据",
        skin: "layui-layer-moon",
        scrollbar: false,
        area: ["360px", "340px"],
        shade: 0.618, // 不显示遮罩
        content: $("#import-graph-div"),
        btn: ["导入", "取消"],
        btnAlign: "c",
        // 对应第1个按钮
        yes: function (index, layero) {
            var graph_file_url = getValue("import-graph-div", "graph_file_url");
            if (graph_file_url == null || graph_file_url == "") {
                layer.alert("请上传通风网络数据!", { skin: "layui-layer-moon" });
            } else {
                // 更新布局参数
                graph_config.graph_name = getValue("import-graph-div", "graph_name");
                // graph_config.nodesep = getValue("import-graph-div", "nodesep");
                // graph_config.ranksep = getValue("import-graph-div", "ranksep");
                graph_config.data_type = getValue("import-graph-div", "data_type");

                var param = {
                    graph_file_url: getValue("import-graph-div", "graph_file_url"),
                    nodesep: graph_config.nodesep,
                    ranksep: graph_config.ranksep,
                    rankdir: graph_config.rankdir,
                    splines: graph_config.splines,
                    node_width: graph_config.node_width,
                    node_height: graph_config.node_height,
                    fontsize: graph_config.fontsize,
                    need_layout: 1 // 是否需要进行布局计算
                };
                if (graph_config.data_type == "json") {
                    param.need_layout = 0; // json数据无需进行计算!!!
                }

                // console.log(param);
                // ortho1--表示正交分支不合并; ortho2--表示正交分支部分线段会合并
                if(param.splines == 'ortho1' || param.splines == 'ortho2') {
                    param.splines = 'ortho';
                }
                ajaxVngFromGraph(param, importNetworkCallBack);
                layer.close(index); //如果设定了yes回调，需进行手工关闭
                closeDiv("import-graph-div");
                // console.log(graph_config);
            }
        },
        // 对应第2个按钮(取消)
        btn2: function (index, layero) {
            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("import-graph-div");
        },
        // 对应右上角关闭按钮
        cancel: function (index, layero) {
            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("import-graph-div");
        },
        /*
            问题:第1次点击上传按钮，上传正常，关闭后。第2次点击上传，上传无效
    
            原因1: <input type='file'>的that.elemFile.on('change', xxx)第16098行执行失败,change事件未添加上去
                    根据搜索的资料，将原因归结于jquery动态绑定事件的问题
            参考1: https://blog.csdn.net/microandsmall/article/details/60326487
                    https://blog.csdn.net/helloqqdd/article/details/70877687
            解决1: that.elemFile.on('change', function()
                    修改为  
                    that.elemFile.parent().on('change', that.elemFile.get(0), function()
            结果1: 未能解决问题！
    
            原因2: 上传组件是动态render出来的,layui自动添加<input type='file' class='layui-upload-file'>标签,
                    这个标签存在一个缺陷(html自带的,与layui、jquery无关)，第2次点击无效(change事件不触发)!
            参考2: https://blog.csdn.net/sly94/article/details/53894274
                    https://blog.csdn.net/wc0077/article/details/42065193
                    https://blog.csdn.net/microandsmall/article/details/60326487
                    jquery中查看事件的方法:  $._data($('.layui-upload-file').get(0))
                    https://www.cnblogs.com/suizhikuo/p/4566063.html
                    JQuery修改div的id
                    https://blog.csdn.net/qq_29663071/article/details/50747476
            解决2: 动态修改上传组件的id
                    var new_id = 'upload_graph_'+vis.util.randomUUID()
                    $("#upload_graph").attr('id', new_id);
            结果2: 搞定^_^_^
            */
        success: function () {
            // 绑定esc键
            $(document).on("keydown", function (e) {
                if (e.keyCode == 27) {
                    layer.close(index);
                    closeDiv("import-graph-div");
                }
            });

            // 清空隐藏input的内容
            setValue("import-graph-div", "graph_file_url", "");

            // 修改上传组件的id(涉及到动态渲染,必须修改id!!!)
            var new_id = "upload_graph_" + new Date().getTime();
            $("#import-graph-div #upload_graph").attr("id", new_id);
            // 绑定数据上传组件
            renderUpload(
                "#" + new_id,
                "index/index/ajaxUploadGraph",
                "graph_file_url",
                "txt|dxf"
            );
        },
        end: function () { }
    });
    /* 渲染表单 */
    form.render();
}

function openGraph() {
    // 弹出对话框
    var index = layer.open({
        type: 1,
        title: "打开通风网络图",
        skin: "layui-layer-moon",
        scrollbar: false,
        area: ["360px", "210px"],
        shade: 0.618, // 不显示遮罩
        content: $("#open-graph-div"),
        btn: ["打开", "取消"],
        btnAlign: "c",
        // 对应第1个按钮
        yes: function (index, layero) {
            var graph_file_url = getValue("open-graph-div", "graph_file_url2222");
            if (graph_file_url == null || graph_file_url == "") {
                layer.alert("请指定一个通风网络图文件(json格式)!", {
                    skin: "layui-layer-moon"
                });
            } else {
                var param = {
                    graph_file_url: getValue("open-graph-div", "graph_file_url2222"),
                    need_layout: 0 // 是否需要进行布局计算
                };
                ajaxVngFromGraph(param, importNetworkCallBack);
                layer.close(index); //如果设定了yes回调，需进行手工关闭
                closeDiv("open-graph-div");
            }
        },
        // 对应第2个按钮(取消)
        btn2: function (index, layero) {
            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("open-graph-div");
        },
        // 对应右上角关闭按钮
        cancel: function (index, layero) {
            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("open-graph-div");
        },
        success: function () {
            // 绑定esc键
            $(document).on("keydown", function (e) {
                if (e.keyCode == 27) {
                    layer.close(index);
                    closeDiv("open-graph-div");
                }
            });
            // 清空隐藏input的内容
            setValue("open-graph-div", "graph_file_url2222", "");

            // 修改上传组件的id(涉及到动态渲染,必须修改id!!!)
            var new_id = "upload_graph_" + new Date().getTime();
            $("#open-graph-div #upload_graph").attr("id", new_id);
            // 绑定数据上传组件
            renderUpload(
                "#" + new_id,
                "index/index/ajaxUploadGraph",
                "graph_file_url2222",
                "json"
            );
        },
        end: function () { }
    });
    /* 渲染表单 */
    form.render();
}

/**
 * Html编码获取Html转义实体
 * JQuery Html Encoding、Decoding
 * 原理是利用JQuery自带的html()和text()函数可以转义Html字符
 * 虚拟一个Div通过赋值和取值来得到想要的Html编码或者解码
 * https://blog.csdn.net/wzygis/article/details/67636932
 * https://www.cnblogs.com/daysme/p/7100553.html
 */
function htmlEncode(value) {
    return $("<div/>")
        .text(value)
        .html();
}
//Html解码获取Html实体
function htmlDecode(value) {
    return $("<div/>")
        .html(value)
        .text();
}

// 图片与Base64编码之间的转换和下载
// https://blog.csdn.net/qq20004604/article/details/72824147
// https://blog.csdn.net/four_lemmo/article/details/70158844
/**
 * base64 转 blob 对象，文件上传
 * 转载自：http://blog.csdn.net/hsany330/article/details/52575459
 * @param dataURI
 * @returns {Blob}
 */
function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(",")[1]);
    var mimeString = dataURI
        .split(",")[0]
        .split(":")[1]
        .split(";")[0];
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: mimeString });
}

function downloadFile(content, filename, isBase64) {
    // 创建隐藏的可下载链接
    var eleLink = document.createElement("a");
    eleLink.download = filename;
    eleLink.style.display = "none";

    var blob = null;
    if (isBase64) {
        // eleLink.href = content;
        blob = dataURItoBlob(content);
    } else {
        // 字符内容转变成blob地址
        blob = new Blob([content]);
    }
    if (blob != null) {
        eleLink.href = URL.createObjectURL(blob);
        // console.log(eleLink);
        // 触发点击
        document.body.appendChild(eleLink);
        eleLink.click();
    }
    // 然后移除
    document.body.removeChild(eleLink);
}

// 生成svg图形
// file:///D:/develop/GitTest/GoJS/intro/makingSVG.html
// 取出dom对象的html字符串源码: svg.outerHTML
function exportSvg(_filename, _scale) {
    // 生成svg
    var svg = myDiagram.makeSvg({
        scale: _scale
    });
    // console.log(svg);
    var svg_file = _filename + ".svg";
    downloadFile(svg.outerHTML, svg_file, false);
}

function exportImg(_filename, _type, _scale) {
    var bound = myDiagram.documentBounds;
    console.log(bound);

    options = {
        scale: _scale,
        // 参考: https://gojs.net/latest/intro/makingImages.html
        // The default value for maxSize is go.Size(2000, 2000)
        maxSize: new go.Size(bound.width*_scale, bound.height*_scale)
    };
    // 设置颜色
    // png图形支持rgba格式,默认为透明; 而jpeg格式只支持rgb,不支持透明背景
    // options['background'] = "rgba(255, 255, 255, 0)";
    if (_type == "jpeg") {
        options["type"] = "image/jpeg";
        options["background"] = "AntiqueWhite"; // 不设置该项,默认为透明背景
    }

    // 生成jpg
    var img = myDiagram.makeImage(options);
    // console.log(img);
    var img_file = _filename + "." + _type;
    // img.src采用Base64编码表示图片
    downloadFile(img.src, img_file, true);
}

function exportJson(_filename, _type) {
    // 使用内置的toJson方法导出模型数据
    var model_json = myDiagram.model.toJson();
    var graph = JSON.parse(model_json);
    // 附加全局配置数据
    graph["graph_config"] = graph_config;

    // https://www.cnblogs.com/ys-wuhan/p/6599063.html
    var json_file = _filename + "." + _type;
    downloadFile(JSON.stringify(graph), json_file, false);
}

function exportDxf(_filename, _type) {
    // 构建网络图几何信息数据
    var graph = buildGraphDatas(true);
    if (graph == null) {
        layer.alert("生成通风网络数据失败!", { skin: "layui-layer-moon" });
        return;
    }
    // console.log(graph);

    // https://www.cnblogs.com/ys-wuhan/p/6599063.html
    var dxf_file = _filename + "." + _type;
    $.ajax({
        url: "index/index/ajaxForDxf", //这里指向的就不再是页面了，而是一个方法。
        data: { graph: JSON.stringify(graph) },
        type: "post",
        dataType: "json",
        success: function (data) {
            downloadFile(data["dxf"], dxf_file, false);
            // console.log(data);
        },
        error: function () {
            layer.alert("导出CAD图形失败!", { skin: "layui-layer-moon" });
        }
    });
}

// 生成png图片
// file:///D:/develop/GitTest/GoJS/intro/makingImages.html
function exportGraph() {
    var index = layer.open({
        type: 1,
        title: "导出通风网络图",
        skin: "layui-layer-moon",
        scrollbar: false,
        area: ["360px", "350px"],
        shade: 0.618, // 不显示遮罩
        content: $("#export-graph-div"),
        btn: ["导出", "取消"],
        btnAlign: "c",
        // 对应第1个按钮
        yes: function (index, layero) {
            var graph_name = getValue("export-graph-div", "graph_name");
            graph_name = graph_name.replace(/^\s+|\s+$/g, "");
            if (graph_name == "") {
                layer.alert("请指定导出图形名称!", { skin: "layui-layer-moon" });
            } else {
                if (myDiagram.model.linkDataArray.length == 0) {
                    layer.alert("分支个数为0,无数据可以导出!", {
                        skin: "layui-layer-moon"
                    });
                    return;
                }
                var img_scale = parseFloat(getValue("export-graph-div", "img_scale"));
                var img_type = getValue("export-graph-div", "img_type");
                if (img_type == "svg") {
                    exportSvg(graph_name, img_scale);
                } else if (img_type == "png" || img_type == "jpeg") {
                    exportImg(graph_name, img_type, img_scale);
                } else if (img_type == "dxf") {
                    exportDxf(graph_name, img_type);
                }
                // else if (img_type == 'json') {
                //     exportJson(graph_name, img_type);
                // }

                // graph_config.graph_name = getValue("export-graph-div", "graph_name");
                graph_config.img_scale = getValue("export-graph-div", "img_scale");
                graph_config.img_type = getValue("export-graph-div", "img_type");
                layer.close(index); //如果设定了yes回调，需进行手工关闭
                closeDiv("export-graph-div");
            }
        },
        // 对应第2个按钮(取消)
        btn2: function (index, layero) {
            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("export-graph-div");
        },
        // 对应右上角关闭按钮
        cancel: function (index, layero) {
            layer.close(index); //如果设定了yes回调，需进行手工关闭
            closeDiv("export-graph-div");
        },
        success: function () {
            // 绑定esc键
            $(document).on("keydown", function (e) {
                if (e.keyCode == 27) {
                    layer.close(index);
                    closeDiv("export-graph-div");
                }
            });
            // 读取当前布局参数
            setValue("export-graph-div", "graph_name", graph_config.graph_name);
            setValue("export-graph-div", "img_scale", graph_config.img_scale);
            setValue("export-graph-div", "img_type", graph_config.img_type);
        },
        end: function () { }
    });
}
