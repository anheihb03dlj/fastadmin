<?php

use Thrift\Exception\TException;
use ThriftRpc\RpcClient;

function test_rpc()
{
    try {
		$service_client = new RpcClient('vno', HOST, PORT2);
		$client = $service_client->getClient();
        $ret = $client->TestVng();
        echo('测试结果:'.$ret);
	} catch (TException $tx) {
        echo('TException: '.$tx->getMessage()."\n");
    }
}
?>