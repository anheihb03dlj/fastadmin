通风网络图绘制软件,采用B/S技术

前端: 
    1、界面
        模仿fastadmin后台(https://www.fastadmin.net/)
        模仿AntV g6 editor(https://antv.alipay.com/assets/dist/3.0.0/g6/1.x/editor/base.html)
    2、拓扑图可视化(network)
        gojs(商业库, https://gojs.net/latest/index.html)
    3、DOM操纵
        jquery+ajax
后端：
    1、thinkphp+thrift
    前端利用ajax发送数据（json格式），经过TP5的路由转交给php处理，php利用rpc技术向python服务器请求进行布局计算
    计算完成后，将布局的结果（节点坐标和分支形状）返回到前端（ajax）

布局：
    1、python+graphviz实现网络图布局计算
    2、利用thrift进行rpc通信(python作为rpc服务器，php客户端)
