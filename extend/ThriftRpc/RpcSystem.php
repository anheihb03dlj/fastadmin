<?php

namespace ThriftRpc;

/*
* http://www.cainiaods.com/article/view/9
        https://blog.csdn.net/kc87654321/article/details/7226719
        http://helight.info/2014-02-13/thrift%E4%B9%8Bphp%E5%AE%A2%E6%88%B7%E7%AB%AF%E4%BD%BF%E7%94%A8/
*/
require_once EXTEND_PATH.'Thrift/ClassLoader/ThriftClassLoader.php';

use Thrift\ClassLoader\ThriftClassLoader;

class RpcSystem
{
    public static $THRIFT_ROOT = LIB_PATH;
    public static $GEN_DIR = LIB_PATH;
    // $GEN_DIR = realpath(dirname(__FILE__)).'/gen-php';
    static function init()
    {
        $loader = new ThriftClassLoader();
        $loader->registerNamespace('Thrift', self::$THRIFT_ROOT);
        $loader->registerDefinition('vno', self::$GEN_DIR);
        $loader->register();        
    }
}
?>