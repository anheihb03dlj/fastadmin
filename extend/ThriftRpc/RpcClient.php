<?php
 
namespace ThriftRpc;

use Thrift\Protocol\TBinaryProtocol;
use Thrift\Transport\TSocket;
// use Thrift\Transport\THttpClient;
use Thrift\Transport\TBufferedTransport;
use Thrift\Exception\TException;
 
use vno\VnoServiceClient;

 define('HOST', 'localhost');
 define('PORT1', 9100);
 define('PORT2', 9090);
 
 function create_client($service_name, $protocol)
 {
     if($service_name == 'vno')
     {
         return new VnoServiceClient($protocol);
     }
     else
     {
         return NULL;
     }
 }

class RpcClient {
 
    public $client;
    private $transport;
    private $protocol;
    private $service_name;
    private $server_ip;
    private $server_port;
    private $socket;
 
    function __construct($service_name, $server_ip=HOST, $server_port=PORT2) {
        $this->service_name = $service_name;
        $this->server_ip = $server_ip;
        $this->server_port = $server_port;
    }
 
    function __destruct() {
        $this->transport->close();
    }
 
    public function getClient() {
        $this->socket = new TSocket($this->server_ip, $this->server_port);
        $this->transport = new TBufferedTransport($this->socket, 1024, 1024);
        $this->protocol = new TBinaryProtocol($this->transport);
        $this->client = create_client($this->service_name, $this->protocol);
        if($this->client == NULL)
        {
            return NULL;
        }
        else
        {
            $this->transport->open();
            return $this->client;
        }        
    }
}

?>